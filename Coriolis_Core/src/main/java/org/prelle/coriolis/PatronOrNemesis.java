/**
 * 
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.BasePluginData;

/**
 * @author prelle
 *
 */
@Root(name="patron")
public class PatronOrNemesis extends BasePluginData {
	
	//-------------------------------------------------------------------
	public PatronOrNemesis() {
	}

	//-------------------------------------------------------------------
	public String getName() {
		try {
			return i18n.getString("patronOrNemesis."+getId()+".name");
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "patronOrNemesis."+getId()+".name";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "patronOrNemesis."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return null;
	}

	//-------------------------------------------------------------------
	public String getOrganization() {
		try {
			return i18n.getString("patronOrNemesis."+getId()+".orga");
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "patronOrNemesis."+getId()+".orga";
	}

	//-------------------------------------------------------------------
	public String getDescription() {
		try {
			return i18n.getString("patronOrNemesis."+getId()+".desc");
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "patronOrNemesis."+getId()+".desc";
	}

}
