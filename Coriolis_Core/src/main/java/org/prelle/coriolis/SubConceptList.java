/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="subconcepts")
@ElementList(entry="subconcept",type=SubConcept.class,inline=true)
public class SubConceptList extends ArrayList<SubConcept> {

}
