/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.MissingResourceException;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.YearZeroModule;

/**
 * @author prelle
 *
 */
@Root(name="groupconcept")
public class GroupConcept extends YearZeroModule {
	
	@ElementList(inline=false,type=PatronOrNemesis.class,entry="patron")
	private ArrayList<PatronOrNemesis> patrons;

	//-------------------------------------------------------------------
	public GroupConcept() {
		patrons = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("groupconcept."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "groupconcept."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "groupconcept."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "groupconcept."+getId()+".desc";
	}

	//-------------------------------------------------------------------
	public ArrayList<PatronOrNemesis> getPatronsAndNemesises() {
		return patrons;
	}

}
