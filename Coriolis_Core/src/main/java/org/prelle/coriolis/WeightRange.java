/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.List;

import org.prelle.coriolis.CoriolisItemTemplate.Weight;

/**
 * @author Stefan
 *
 */
public class WeightRange {

	private Weight min;
	private Weight max;

	//--------------------------------------------------------------------
	/**
	 */
	public WeightRange(Weight min, Weight max) {
		this.min = min;
		this.max = max;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the min
	 */
	public Weight getMin() {
		return min;
	}

	//--------------------------------------------------------------------
	/**
	 * @param min the min to set
	 */
	public void setMin(Weight min) {
		this.min = min;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public Weight getMax() {
		return max;
	}

	//--------------------------------------------------------------------
	/**
	 * @param max the max to set
	 */
	public void setMax(Weight max) {
		this.max = max;
	}

	//--------------------------------------------------------------------
	public boolean isFlexibel() {
		return (max!=null && min!=max);
	}

	//--------------------------------------------------------------------
	public List<Weight> getWeights() {
		List<Weight> ret = new ArrayList<>();
		for (Weight tmp : Weight.values()) {
			if (tmp.ordinal()>=min.ordinal() && tmp.ordinal()<=max.ordinal())
				ret.add(tmp);
		}
		return ret;
	}
}
