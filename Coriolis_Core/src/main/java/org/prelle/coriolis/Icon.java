/**
 * 
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.YearZeroModule;

/**
 * @author prelle
 *
 */
@Root(name="icon")
public class Icon extends YearZeroModule {

	//-------------------------------------------------------------------
	public Icon() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("icon."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "icon."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "icon."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "icon."+getId()+".desc";
	}

}
