/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="groupconcepts")
@ElementList(entry="groupconcept",type=GroupConcept.class,inline=true)
public class GroupConceptList extends ArrayList<GroupConcept> {

}
