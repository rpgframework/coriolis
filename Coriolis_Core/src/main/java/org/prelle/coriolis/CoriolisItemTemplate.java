/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.coriolis.persist.TechTierConverter;
import org.prelle.coriolis.persist.WeightRangeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.yearzeroengine.ItemTemplate;

/**
 * @author prelle
 *
 */
public class CoriolisItemTemplate extends ItemTemplate {

	public enum Weight {
		TINY,
		LIGHT,
		NORMAL,
		HEAVY
	}

	@Attribute
	@AttribConvert(WeightRangeConverter.class)
	private WeightRange weight;
	@Attribute
	@AttribConvert(TechTierConverter.class)
	private List<TechTier> tier;
	@Attribute
	private boolean restricted;
	@Attribute
	private ItemType type;
	@Element(name="weapon")
	private WeaponData weaponData;
	@Element(name="armor")
	private ArmorData armorData;

	//-------------------------------------------------------------------
	public CoriolisItemTemplate() {
		type = ItemType.EVERYDAY;
		weight = new WeightRange(Weight.NORMAL, null);
		tier = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public CoriolisItemTemplate(String id, TechTier tier, Weight weight) {
		this();
		this.weight.setMin(weight);
		this.id = id;
		this.tier.add(tier);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the weight
	 */
	public WeightRange getWeight() {
		if (weight==null)
			return new WeightRange(Weight.NORMAL, null);
		return weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(WeightRange weight) {
		this.weight = weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tier
	 */
	public List<TechTier> getTier() {
		if (tier==null)
			return new ArrayList<>();
		return new ArrayList<>(tier);
	}

	//-------------------------------------------------------------------
	/**
	 * @param tier the tier to set
	 */
	public void setTier(TechTier... tier) {
		this.tier = Arrays.asList(tier);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the restricted
	 */
	public boolean isRestricted() {
		return restricted;
	}

	//-------------------------------------------------------------------
	/**
	 * @param restricted the restricted to set
	 */
	public void setRestricted(boolean restricted) {
		this.restricted = restricted;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ItemType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the weaponData
	 */
	public WeaponData getWeaponData() {
		return weaponData;
	}

	//--------------------------------------------------------------------
	/**
	 * @param weaponData the weaponData to set
	 */
	public void setWeaponData(WeaponData weaponData) {
		this.weaponData = weaponData;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the armorData
	 */
	public ArmorData getArmorData() {
		return armorData;
	}

	//--------------------------------------------------------------------
	/**
	 * @param armorData the armorData to set
	 */
	public void setArmorData(ArmorData armorData) {
		this.armorData = armorData;
	}

}
