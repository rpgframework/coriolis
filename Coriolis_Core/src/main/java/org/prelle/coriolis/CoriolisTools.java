/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.HistoryElementImpl;
import org.prelle.yearzeroengine.RewardImpl;
import org.prelle.yearzeroengine.modifications.CurrencyModification;
import org.prelle.yearzeroengine.modifications.ItemModification;
import org.prelle.yearzeroengine.modifications.SkillModification;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.Datable;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;
import de.rpgframework.products.ProductServiceLoader;

/**
 * @author Stefan
 *
 */
public class CoriolisTools {

	private final static PropertyResourceBundle RES = CoriolisCore.getI18nResources();

	protected static Logger logger = CoriolisConstants.LOGGER;

	//--------------------------------------------------------------------
	public static String toString(Modification mod) {
		try {
			if (mod instanceof TalentModification) {
				TalentModification tMod = (TalentModification)mod;
				if (tMod.getTalent()!=null)
					return tMod.getTalent().getName();
				else {
					try {
						return RES.getString("talentmodchoice."+tMod.getType().name().toLowerCase());
					} catch (MissingResourceException e) {
						logger.warn("Missing key '"+e.getKey()+"' in "+RES.getBaseBundleName());
						return "talentmodchoice."+tMod.getType().name().toLowerCase();
					}
				}
			}
			if (mod instanceof ItemModification) {
				ItemModification iMod = (ItemModification)mod;
				if (iMod.getI18nKey()!=null && iMod.getSource()!=null)
					return ((BasePluginData)iMod.getSource()).getResourceBundle().getString(iMod.getI18nKey());
				return iMod.getItem().getName();
			}
			if (mod instanceof SkillModification) {
				SkillModification iMod = (SkillModification)mod;
				return iMod.getModifiedItem().getName()+" "+iMod.getValue();
			}
			if (mod instanceof CurrencyModification) {
				CurrencyModification iMod = (CurrencyModification)mod;
				return iMod.getValue()+" "+RES.getString("label.birr");
			}
			logger.warn("TODO: toString("+mod.getClass()+" = "+mod+" )");
			return mod.getClass().getSimpleName();
		} catch (Exception e) {
			return mod.getClass().getSimpleName()+"=null";
		}
	}

	//-------------------------------------------------------------------
	private static void sort(List<Datable> rewardsAndMods) {
		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
			public int compare(Datable o1, Datable o2) {
				Long time1 = 0L;
				Long time2 = 0L;
				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();

				int cmp = time1.compareTo(time2);
				if (cmp==0) {
					if (o1 instanceof RewardImpl && o2 instanceof Modification) return -1;
					if (o1 instanceof Modification && o2 instanceof RewardImpl) return  1;
				}
				return cmp;
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @param aggregate Aggregate history elements with same adventure
	 */
	public static List<HistoryElement> convertToHistoryElementList(CoriolisCharacter charac, boolean aggregate) {
		// Initial reward
		logger.warn("Sort "+charac.getRewards().size()+" rewards  and "+charac.getHistory().size()+" mods");

		/*
		 * Build a merged list of rewards and modifications and sort it by time
		 */
		List<Datable> rewardsAndMods = new ArrayList<Datable>();
		rewardsAndMods.addAll(charac.getRewards());
		for (Modification mod : charac.getHistory()) {
			rewardsAndMods.add(mod.clone());
		}
//		rewardsAndMods.addAll(charac.getHistory());
		sort(rewardsAndMods);

		/*
		 * Now build a list of HistoryElements. Start a new H
		 */
		List<HistoryElement> ret = new ArrayList<HistoryElement>();
		HistoryElementImpl current = null;
		ProductService sessServ = ProductServiceLoader.getInstance();

		for (Datable item : rewardsAndMods) {
			if (item instanceof RewardImpl) {
				RewardImpl reward = (RewardImpl)item;
				Adventure adv = null;
				if (reward.getId()!=null && sessServ!=null) {
					adv = sessServ.getAdventure(RoleplayingSystem.CORIOLIS, reward.getId());
					if (adv==null) {
						logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
					}
				}
				// If is same adventure as current, keep same history element
				if (!aggregate || !(adv!=null && current!=null && adv.getId().equals(current.getAdventureID())) ) {
					current = new HistoryElementImpl();
					current.setName(reward.getTitle());
					if (adv!=null) {
						current.setName(adv.getTitle());
						current.setAdventure(adv);
					}
					ret.add(current);
				}
				current.addGained(reward);
			} else if (item instanceof ValueModification<?>) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
				} else {
					Modification lastMod = (current.getSpent().isEmpty())?null:current.getSpent().get(current.getSpent().size()-1);
					if (lastMod!=null && lastMod.getClass()==item.getClass()) {
						if (item instanceof SkillModification) {
//							logger.debug("Combine "+lastMod+" with "+item);
							// Aggregate same skill
							SkillModification lastSMod = (SkillModification)lastMod;
							SkillModification newtMod = (SkillModification)item;
							if (lastSMod.getSkill()==newtMod.getSkill()) {
								// Same skill
								lastSMod.setValue(newtMod.getValue());
								lastSMod.setExpCost(lastSMod.getExpCost() + newtMod.getExpCost());
							} else {
								// Different skill
								current.addSpent((ValueModification<?>) item);
							}
						} else {
							current.addSpent((ValueModification<?>) item);
						}
					} else {
						current.addSpent((ValueModification<?>) item);
					}
				}
			} else if (item instanceof CurrencyModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					ret.add(current);
				} else {
				}
			} else if (item instanceof TalentModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					current.addSpent((Modification) item);
					ret.add(current);
				} else {
					current.addSpent((Modification) item);
				}
			} else {
				logger.error("Don't know how to "+item.getClass());
			}
		}


		logger.warn("  return "+ret.size()+" elements");
		return ret;
	}

	//-------------------------------------------------------------------
	public static void reward(CoriolisCharacter charac, RewardImpl reward) {
		logger.info("Add reward "+reward+" to "+charac);
		charac.addReward(reward);

		charac.setExpFree(charac.getExpFree() + reward.getExperiencePoints());
		for (Modification mod : reward.getModifications()) {
			if (mod.getDate()==null) {
				mod.setDate(reward.getDate());
				logger.info("Fix modification date to "+reward.getDate());
			}
			if (mod instanceof CurrencyModification) {
				CurrencyModification nMod = (CurrencyModification)mod;
				charac.setBirr(charac.getBirr() + nMod.getValue());
				// Add to history
				charac.addToHistory(mod);

			} else {
				logger.error("Unsupported modification: "+mod.getClass());
			}
		}
	}

	//-------------------------------------------------------------------
	public static void removeFromHistory(CoriolisCharacter model, Modification search) {
		List<Modification> hist = model.getHistory();
		Collections.reverse(hist);
		for (Modification tmp : hist) {
			if (tmp.equals(search)) {
				logger.info("history before: "+model.getHistory());
				model.removeFromHistory(tmp);
				logger.info("Removed from character history: "+tmp);
				logger.info("history now: "+model.getHistory());
				return;
			} else
				logger.info("Different: "+tmp+"  vs.  "+search);
		}
		logger.error("Could not find history to remove");
	}

}
