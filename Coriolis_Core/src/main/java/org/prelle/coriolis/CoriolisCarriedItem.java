/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.List;

import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.WeaponData.Range;
import org.prelle.simplepersist.Attribute;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.modifications.SkillModification;

/**
 * @author Stefan
 *
 */
public class CoriolisCarriedItem extends CarriedItem<CoriolisItemTemplate> {

	@Attribute
	private TechTier tier;
	@Attribute
	private Weight weight;
	@Attribute
	private int cost;
	@Attribute(name="loc")
	private ItemLocation location;

	//--------------------------------------------------------------------
	public CoriolisCarriedItem() {
		location = ItemLocation.BODY;
	}

	//--------------------------------------------------------------------
	public CoriolisCarriedItem(CoriolisItemTemplate item) {
		super(item);
		location = ItemLocation.BODY;
		switch (item.getTier().size()) {
		case 0:
			throw new IllegalArgumentException("No tech tier for "+item.getId());
		case 1:
//			tier = item.getTier().get(0);
			break;
		default:
			throw new IllegalArgumentException("item '"+item.getId()+"' has multiple tech tiers - use <init>(Template, Tier)");
		}
		count = 0;
	}

	//--------------------------------------------------------------------
	public CoriolisCarriedItem(CoriolisItemTemplate item, Selection[] select) {
		super(item);
		location = ItemLocation.BODY;
		Skill skill = null;
		for (Selection sel : select) {
			switch (sel.option) {
			case COST: cost = (Integer)sel.value; break;
			case TIER: this.tier = (TechTier)sel.value; break;
			case WEIGHT: this.weight = (Weight)sel.value; break;
			case COUNT: this.count = (Integer)sel.value; break;
			case SKILL: skill = (Skill)sel.value; break;
			case VAL: 
				if (skill==null)
					CoriolisConstants.LOGGER.error("Don't know how to process "+sel.option+" for "+item.getId()+" when skill is not set");
				else {
					SkillModification mod = new SkillModification(skill, (Integer)sel.value);
					modifications.add(mod);
				}
				break;
			case NAME: this.name = (String)sel.value; break;
			}
		}
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (ref!=null)
			return "(Item '"+ref.getId()+"', tier="+tier+", weight="+weight+")";
		return "(Item is null, tier="+tier+", weight="+weight+")";
	}

	//--------------------------------------------------------------------
	public List<FeatureReference> getFeatures() {
		if (ref==null)
			return new ArrayList<FeatureReference>();
		switch (ref.getType()) {
		case ARMOR:
			if (ref.getArmorData()==null)
				return new ArrayList<FeatureReference>();
			return ref.getArmorData().getFeatures();
		case WEAPON_RANGE:
		case WEAPON_MELEE:
			if (ref.getWeaponData()==null)
				return new ArrayList<FeatureReference>();
			return ref.getWeaponData().getFeatures();
		default:
			return new ArrayList<FeatureReference>();
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.CarriedItem#getBonus()
	 */
	@Override
	public int getBonus() {
		return ref.getBonus();
	}

	//--------------------------------------------------------------------
	public TechTier getTechTier() {
		return tier;
	}

	//--------------------------------------------------------------------
	public int getInitiative() {
		return (ref.getWeaponData()!=null)?ref.getWeaponData().getInitiative():0;
	}

	//--------------------------------------------------------------------
	public int getDamage() {
		return (ref.getWeaponData()!=null)?ref.getWeaponData().getDamage():0;
	}

	//--------------------------------------------------------------------
	public int getCrit() {
		return (ref.getWeaponData()!=null)?ref.getWeaponData().getCrit():0;
	}

	//--------------------------------------------------------------------
	public int getBlastPower() {
		return (ref.getWeaponData()!=null)?ref.getWeaponData().getBlastPower():0;
	}

	//--------------------------------------------------------------------
	public Range getRange() {
		return (ref.getWeaponData()!=null)?ref.getWeaponData().getRange():null;
	}

	//--------------------------------------------------------------------
	public int getArmorRating() {
		return (ref.getArmorData()!=null)?ref.getArmorData().getRating():0;
	}

	//--------------------------------------------------------------------
	public Weight getWeight() {
		if (weight!=null)
			return weight;
		if (ref.getWeight().isFlexibel())
			return Weight.NORMAL;
		else
			return ref.getWeight().getMin();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		if (cost==0 && ref.getCost()!=null) {
			return ref.getCost().getMin();
		}
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the location
	 */
	public ItemLocation getLocation() {
		return location;
	}

	//--------------------------------------------------------------------
	/**
	 * @param location the location to set
	 */
	public void setLocation(ItemLocation location) {
		this.location = location;
	}

}
