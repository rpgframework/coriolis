/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="humantypes")
@ElementList(entry="humantype",type=HumanType.class,inline=true)
public class HumanTypeList extends ArrayList<HumanType> {

}
