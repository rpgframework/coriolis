package org.prelle.coriolis;

public enum TechTier { 
	P,
	O,
	A,
	F
	;

	//-------------------------------------------------------------------
	public String getName() {
		return CoriolisCore.getI18nResources().getString("techtier."+this.name().toLowerCase());
	}
	//-------------------------------------------------------------------
	public String getShortName() {
		return CoriolisCore.getI18nResources().getString("techtier."+this.name().toLowerCase()+".short");
	}

}