/**
 * 
 */
package org.prelle.coriolis;

import org.prelle.yearzeroengine.SkillCategory;

/**
 * @author prelle
 *
 */
public enum CoriolisSkillCategory implements SkillCategory {

	GENERAL,
	ADVANCED,
	;

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.Attribute#getShortName()
	 */
	@Override
	public String getShortName() {
		return CoriolisCore.getI18nResources().getString("attribute."+this.name().toLowerCase()+".short");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.Attribute#getName()
	 */
	@Override
    public String getName() {
        return CoriolisCore.getI18nResources().getString("attribute."+this.name().toLowerCase());
    }
	
	
}
