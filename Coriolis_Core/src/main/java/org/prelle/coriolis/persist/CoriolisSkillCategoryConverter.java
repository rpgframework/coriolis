/**
 * 
 */
package org.prelle.coriolis.persist;

import java.util.NoSuchElementException;

import org.prelle.coriolis.CoriolisSkillCategory;
import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({YearZeroEngineCore.KEY_RULES})
public class CoriolisSkillCategoryConverter implements StringValueConverter<CoriolisSkillCategory> {

	private YearZeroEngineRuleset ruleset;
	
	//-------------------------------------------------------------------
	public CoriolisSkillCategoryConverter(RoleplayingSystem rules) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngineCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(CoriolisSkillCategory value) throws Exception {
		return value.name();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CoriolisSkillCategory read(String idref) throws Exception {
		try {
			return CoriolisSkillCategory.valueOf(idref);
		} catch (NoSuchElementException e) {
			throw new SerializationException("Unknown CoriolisSkillCategory ID '"+idref);
		}
	}

}
