/**
 *
 */
package org.prelle.coriolis.persist;

import java.util.StringTokenizer;

import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.WeightRange;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author Stefan
 *
 */
public class WeightRangeConverter implements StringValueConverter<WeightRange> {

	//--------------------------------------------------------------------
	/**
	 */
	public WeightRangeConverter() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(WeightRange value) throws Exception {
		if (value.isFlexibel()) {
			return value.getMin()+"-"+value.getMax();
		}
		return String.valueOf(value.getMin());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public WeightRange read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v,"-");
		Weight min = Weight.valueOf(tok.nextToken());
		Weight max = null;
		if (tok.hasMoreTokens())
			max = Weight.valueOf(tok.nextToken());
		return new WeightRange(min,max);
	}

}
