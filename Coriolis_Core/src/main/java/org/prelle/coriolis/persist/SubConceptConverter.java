/**
 * 
 */
package org.prelle.coriolis.persist;

import org.prelle.coriolis.SubConcept;
import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.yearzeroengine.YearZeroEngineCore;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({YearZeroEngineCore.KEY_RULES})
public class SubConceptConverter implements StringValueConverter<SubConcept> {

	private YearZeroEngineRuleset ruleset;
	
	//-------------------------------------------------------------------
	public SubConceptConverter(RoleplayingSystem rules) {
		ruleset = YearZeroEngineCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in YearZeroEngineCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(SubConcept value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public SubConcept read(String idref) throws Exception {
		SubConcept skill = ruleset.getByType(SubConcept.class, idref);
		if (skill==null)
			throw new SerializationException("Unknown subconcept ID '"+idref);

		return skill;
	}

}
