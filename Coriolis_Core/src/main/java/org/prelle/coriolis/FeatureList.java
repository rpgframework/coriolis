/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="features")
@ElementList(entry="feature",type=Feature.class,inline=true)
public class FeatureList extends ArrayList<Feature> {

}
