/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="icons")
@ElementList(entry="icon",type=Icon.class,inline=true)
public class IconList extends ArrayList<Icon> {

}
