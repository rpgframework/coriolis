/**
 * 
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.YearZeroModule;

/**
 * @author prelle
 *
 */
@Root(name="upbringing")
public class Upbringing extends YearZeroModule {
	
	@Attribute
	private boolean humanites;

	//-------------------------------------------------------------------
	public Upbringing() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("upbringing."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "upbringing."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "upbringing."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "upbringing."+getId()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the humanites
	 */
	public boolean hasHumanites() {
		return humanites;
	}

}
