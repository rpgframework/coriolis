/**
 * 
 */
package org.prelle.coriolis;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.character.DecodeEncodeException;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class CoriolisRules implements RulePlugin<CoriolisCharacter>, CommandBusListener {
	
	private final static Logger logger = LogManager.getLogger("coriolis");
	
	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();
	
	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.PERSISTENCE);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CORE";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Coriolis Core Rules";
	}
	
	//-------------------------------------------------------------------
	/**
	 */
	public CoriolisRules() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.CORIOLIS;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return FEATURES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case ENCODE:
			if (values[0]!=RoleplayingSystem.CORIOLIS) return false;
			if (values.length<2) return false;
			return (values[1] instanceof CoriolisCharacter);
		case DECODE:
			if (values[0]!=RoleplayingSystem.CORIOLIS) return false;
			if (values.length<2) return false;
			return (values[1] instanceof byte[]);
		default:
			return false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		logger.info("handleCommand("+type+", "+Arrays.toString(values)+")");
		switch (type) {
		case ENCODE:
			CoriolisCharacter model = (CoriolisCharacter)values[1];
			byte[] raw;
			try {
				raw = CoriolisCore.save(model);
				return new CommandResult(type, raw);
			} catch (DecodeEncodeException e) {
				if (e.getCause()!=null)
					return new CommandResult(type, false, e.getCause().toString());
				return new CommandResult(type, false, e.toString());
			}
		case DECODE:
			raw = (byte[])values[1];
			try {
				model = CoriolisCore.load(raw);
				logger.debug("Unmarshal done");
				return new CommandResult(type, model);
			} catch (DecodeEncodeException e) {
				return new CommandResult(type, false, e.toString());
			} catch (Exception e) {
				return new CommandResult(type, false, e.toString());
			}
		default:
			return new CommandResult(type, false, "Not supported");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		logger.debug("Add configuration to "+addBelow);
		ConfigContainer configRoot = addBelow.createContainer("Coriolis");
		configRoot.setResourceBundle(CoriolisCore.getI18nResources());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener arg0) {
		CoriolisCore.initialize(this);
		
		CommandBus.registerBusCommandListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return ClassLoader.getSystemResourceAsStream(CoriolisConstants.PREFIX+"/i18n/coriolis/core.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.ENGLISH.getLanguage(), Locale.GERMAN.getLanguage());
	}

}
