/**
 *
 */
package org.prelle.coriolis;

/**
 * @author prelle
 *
 */
public enum Origin {

	FIRSTCOME,
	ZENITHIAN
	;

	//-------------------------------------------------------------------
	public String getName() {
		return CoriolisCore.getI18nResources().getString("origin."+this.name().toLowerCase());
	}

	public String getHelpText() {
		return CoriolisCore.getI18nHelpResources().getString("origin."+this.name().toLowerCase()+".desc");
	}

}
