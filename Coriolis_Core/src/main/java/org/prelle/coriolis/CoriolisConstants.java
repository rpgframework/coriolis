/**
 * 
 */
package org.prelle.coriolis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Stefan Prelle
 *
 */
public interface CoriolisConstants {

	public static Logger LOGGER = LogManager.getLogger("coriolis");
	public static String PREFIX = "org/prelle/coriolis";

}
