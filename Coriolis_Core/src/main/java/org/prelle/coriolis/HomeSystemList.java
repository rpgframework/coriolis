/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="homesystems")
@ElementList(entry="homesystem",type=HomeSystem.class,inline=true)
public class HomeSystemList extends ArrayList<HomeSystem> {

}
