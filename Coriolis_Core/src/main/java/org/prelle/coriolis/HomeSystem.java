/**
 *
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.YearZeroModule;

/**
 * @author prelle
 *
 */
@Root(name="icon")
public class HomeSystem extends YearZeroModule {

	//-------------------------------------------------------------------
	public HomeSystem() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getId();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("homesystem."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
		}
		return "homesystem."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "homesystem."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "homesystem."+getId()+".desc";
	}

}
