package org.prelle.coriolis;

public class Selection {
	public SelectOption option;
	public Object value;

	public Selection() {}

	public String toString() {
		return option+"="+value;
	}

	public Selection(SelectOption opt, Object val) {
		this.option = opt;
		this.value  = val;
	}
}