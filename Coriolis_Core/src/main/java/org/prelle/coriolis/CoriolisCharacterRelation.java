/**
 * 
 */
package org.prelle.coriolis;

import org.prelle.simplepersist.Attribute;
import org.prelle.yearzeroengine.CharacterRelation;

/**
 * @author prelle
 *
 */
public class CoriolisCharacterRelation extends CharacterRelation {
	
	@Attribute
	private boolean buddy;

	//-------------------------------------------------------------------
	public CoriolisCharacterRelation() {
	}

	//-------------------------------------------------------------------
	public CoriolisCharacterRelation(String to, String kind) {
		super(to, kind);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the buddy
	 */
	public boolean isBuddy() {
		return buddy;
	}

	//-------------------------------------------------------------------
	/**
	 * @param buddy the buddy to set
	 */
	public void setBuddy(boolean buddy) {
		this.buddy = buddy;
	}

}
