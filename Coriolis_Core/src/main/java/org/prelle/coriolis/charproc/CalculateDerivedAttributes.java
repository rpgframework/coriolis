/**
 *
 */
package org.prelle.coriolis.charproc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisConstants;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CalculateDerivedAttributes implements CharacterProcessor {

	protected static Logger logger = CoriolisConstants.LOGGER;

	//--------------------------------------------------------------------
	public CalculateDerivedAttributes() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> unprocessed) {
		AttributeValue str = model.getAttribute(AttributeCoriolis.STRENGTH);
		AttributeValue agi = model.getAttribute(AttributeCoriolis.AGILITY);
		AttributeValue emp = model.getAttribute(AttributeCoriolis.EMPATHY);
		AttributeValue wit = model.getAttribute(AttributeCoriolis.WITS);

		((CoriolisCharacter)model).initAttributes(AttributeCoriolis.derivedValues());

		// HP = STR + AGI
		AttributeValue hit = model.getAttribute(AttributeCoriolis.HIT_POINTS);
		hit.setPoints(str.getPoints() + agi.getPoints());
		logger.info("Set hit points to "+hit.getPoints());

		// MP = EMP + WIT
		AttributeValue min = model.getAttribute(AttributeCoriolis.MIND_POINTS);
		min.setPoints(emp.getPoints() + wit.getPoints());
		logger.info("Set mind points to "+min.getPoints());

//		// Mental damage
//		AttributeValue mdam = model.getAttribute(AttributeCoriolis.MENTAL_DAMAGE);
//		AttributeModification amod = new AttributeModification(AttributeCoriolis.MIND_POINTS, -mdam.getPoints());
//		min.addModification(amod);
//		logger.info("After respecting Mind damage MP is "+min.getModifiedValue());
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<ToDoElement>();
	}

}
