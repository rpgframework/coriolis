/**
 *
 */
package org.prelle.coriolis;

import org.prelle.coriolis.persist.FeatureConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class FeatureReference {

	@Attribute
	@AttribConvert(FeatureConverter.class)
	private Feature ref;
	@Attribute
	private int level;

	//--------------------------------------------------------------------
	public FeatureReference() {
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Feature getType() {
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Feature type) {
		this.ref = type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}
