/**
 *
 */
package org.prelle.coriolis;

import org.prelle.yearzeroengine.Attribute;

/**
 * @author prelle
 *
 */
public enum AttributeCoriolis implements Attribute {

	AGILITY,
	EMPATHY,
	STRENGTH,
	WITS,

	REPUTATION,
	HIT_POINTS,
	MIND_POINTS,
	RADIATION,
	MENTAL_DAMAGE

	;

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.Attribute#getShortName()
	 */
	@Override
	public String getShortName() {
		return CoriolisCore.getI18nResources().getString("attribute."+this.name().toLowerCase()+".short");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.Attribute#getName()
	 */
	@Override
    public String getName() {
        return CoriolisCore.getI18nResources().getString("attribute."+this.name().toLowerCase());
    }

	//-------------------------------------------------------------------
	public static Attribute[] primaryValues() {
		return new Attribute[]{STRENGTH,AGILITY,WITS,EMPATHY};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValues() {
		return new Attribute[]{REPUTATION, RADIATION, MENTAL_DAMAGE};
	}

	//-------------------------------------------------------------------
	public static Attribute[] derivedValues() {
		return new Attribute[]{HIT_POINTS, MIND_POINTS};
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		for (Attribute key : primaryValues())
			if (this==key) return true;
		return false;
	}

}
