package org.prelle.coriolis;
import java.util.MissingResourceException;

import org.prelle.yearzeroengine.YearZeroModule;

/**
 *
 */

/**
 * @author Stefan
 *
 */
public class Feature extends YearZeroModule {

	//--------------------------------------------------------------------
	public Feature() {
		// TODO Auto-generated constructor stub
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("feature."+getId());
		} catch (MissingResourceException e) {
			String key = e.getKey();
			if (!reportedKeys.contains(key)) {
				reportedKeys.add(key);
				logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(e.getKey()+"=");
			}
		}
		return "feature."+getId();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "feature."+getId()+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "feature."+getId()+".desc";
	}

}
