/**
 * 
 */
package org.prelle.coriolis;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="items")
@ElementList(entry="item",type=CoriolisItemTemplate.class,inline=true)
public class CoriolisItemTemplateList extends ArrayList<CoriolisItemTemplate> {

}
