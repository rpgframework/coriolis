package org.prelle.coriolis;

public enum ItemType {
	EVERYDAY,
	MEDICURGICAL,
	TOOLS,
	SURVIVAL,
	VEHICLES,
	RECON,
	COMBAT,
	WEAPON_MELEE,
	WEAPON_RANGE,
	ARMOR,
	;


	//-------------------------------------------------------------------
	public String getName() {
		return CoriolisCore.getI18nResources().getString("itemtype."+this.name().toLowerCase());
	}

}