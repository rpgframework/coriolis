package org.prelle.coriolis;
/**
 *
 */


import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
@ElementList(entry="item",type=CoriolisCarriedItem.class)
public class CoriolisCarriedItemList extends ArrayList<CoriolisCarriedItem> {

	private static final long serialVersionUID = 1L;


}
