/**
 * 
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.persist.CharacterClassConverter;

/**
 * @author prelle
 *
 */
@Root(name="subconcept")
public class SubConcept extends YearZeroModule {
	
	@Attribute(required=true)
	@AttribConvert(CharacterClassConverter.class)
	private CharacterClass concept;

	//-------------------------------------------------------------------
	public SubConcept() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getId()+" (of "+concept+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("subconcept."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "subconcept."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "subconcept."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "subconcept."+getId()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the concept
	 */
	public CharacterClass getConcept() {
		return concept;
	}

}
