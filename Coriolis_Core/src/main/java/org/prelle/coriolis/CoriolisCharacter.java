/**
 *
 */
package org.prelle.coriolis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.coriolis.persist.GroupConceptConverter;
import org.prelle.coriolis.persist.HomeSystemConverter;
import org.prelle.coriolis.persist.HumanTypeConverter;
import org.prelle.coriolis.persist.IconConverter;
import org.prelle.coriolis.persist.SubConceptConverter;
import org.prelle.coriolis.persist.UpbringingConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.modifications.TalentModification;
import org.prelle.yearzeroengine.persist.TalentConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="coriolischar")
public class CoriolisCharacter extends YearZeroEngineCharacter {

	@Attribute(name="grpcncpt")
	@AttribConvert(GroupConceptConverter.class)
	private GroupConcept groupConcept;
	@Attribute(name="grptal")
	@AttribConvert(TalentConverter.class)
	private Talent groupTalent;
	@Attribute
	private Origin origin;
	@Attribute(name="upbring")
	@AttribConvert(UpbringingConverter.class)
	private Upbringing upbringing;
	@Attribute(name="human")
	@AttribConvert(HumanTypeConverter.class)
	private HumanType humanType;
	@Attribute(name="homesystem")
	@AttribConvert(HomeSystemConverter.class)
	private HomeSystem homeSystem;
	@Attribute(name="subconcept")
	@AttribConvert(SubConceptConverter.class)
	private SubConcept subconcept;
	@Attribute(name="icon")
	@AttribConvert(IconConverter.class)
	private Icon icon;
	@Element(name="carries")
	protected CoriolisCarriedItemList carries;
	@Attribute(name="birr")
	protected int birr;

	//-------------------------------------------------------------------
	public CoriolisCharacter() {
		carries    = new CoriolisCarriedItemList();
		attributes.addSecondary(new AttributeValue(AttributeCoriolis.HIT_POINTS, 0));
		attributes.addSecondary(new AttributeValue(AttributeCoriolis.MIND_POINTS, 0));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroEngineCharacter#initAttributes(org.prelle.yearzeroengine.Attribute[])
	 */
	@Override
	public void initAttributes(org.prelle.yearzeroengine.Attribute[] values) {
		for (org.prelle.yearzeroengine.Attribute attr : values) {
			switch ( (AttributeCoriolis)attr ) {
			case HIT_POINTS:
			case MIND_POINTS:
				attributes.addSecondary(new AttributeValue(attr, 0));
				break;
			default:
				attributes.add(new AttributeValue(attr, 0));
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the upbringing
	 */
	public Upbringing getUpbringing() {
		return upbringing;
	}

	//-------------------------------------------------------------------
	/**
	 * @param upbringing the upbringing to set
	 */
	public void setUpbringing(Upbringing upbringing) {
		this.upbringing = upbringing;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the subconcept
	 */
	public SubConcept getSubconcept() {
		return subconcept;
	}

	//-------------------------------------------------------------------
	/**
	 * @param subconcept the subconcept to set
	 */
	public void setSubconcept(SubConcept subconcept) {
		this.subconcept = subconcept;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	//-------------------------------------------------------------------
	/**
	 * @param icon the icon to set
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the humanType
	 */
	public HumanType getHumanType() {
		return humanType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param humanType the humanType to set
	 */
	public void setHumanType(HumanType humanType) {
		this.humanType = humanType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the origin
	 */
	public Origin getOrigin() {
		return origin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(Origin origin) {
		this.origin = origin;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the homeSystem
	 */
	public HomeSystem getHomeSystem() {
		return homeSystem;
	}

	//--------------------------------------------------------------------
	/**
	 * @param homeSystem the homeSystem to set
	 */
	public void setHomeSystem(HomeSystem homeSystem) {
		this.homeSystem = homeSystem;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the groupConcept
	 */
	public GroupConcept getGroupConcept() {
		return groupConcept;
	}

	//--------------------------------------------------------------------
	/**
	 * @param groupConcept the groupConcept to set
	 */
	public void setGroupConcept(GroupConcept groupConcept) {
		this.groupConcept = groupConcept;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the groupTalent
	 */
	public Talent getGroupTalent() {
		return groupTalent;
	}

	//--------------------------------------------------------------------
	/**
	 * @param groupTalent the groupTalent to set
	 */
	public void setGroupTalent(Talent groupTalent) {
		this.groupTalent = groupTalent;
	}

	//-------------------------------------------------------------------
	public void clearItems() {
		carries.clear();
	}

	//-------------------------------------------------------------------
	public CoriolisCarriedItem getItem(UUID uuid) {
		for (CoriolisCarriedItem item : carries) {
			if (uuid.equals(item.getUniqueId()))
				return item;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addItem(CoriolisCarriedItem data) {
		if (!carries.contains(data))
			carries.add(data);
	}

	//-------------------------------------------------------------------
	public void removeItem(CoriolisCarriedItem itemToRemove) {
		carries.remove(itemToRemove);
	}

	//--------------------------------------------------------------------
	public List<CoriolisCarriedItem> getItems() {
		List<CoriolisCarriedItem> items = new ArrayList<CoriolisCarriedItem>();
		items.addAll(carries);

		Collections.sort(items);

		return items;
	}

	//--------------------------------------------------------------------
	public List<CoriolisCarriedItem> getItems(ItemType itemType) {
		List<CoriolisCarriedItem> items = new ArrayList<CoriolisCarriedItem>();
		items.addAll(carries.stream().filter(t -> ((CoriolisItemTemplate)t.getItem()).getType()==itemType).collect(Collectors.toList()));

		Collections.sort(items);

		return items;
	}

	//--------------------------------------------------------------------
	public boolean hasItem(CoriolisItemTemplate key) {
		for (CoriolisCarriedItem item : carries) {
			if (item.getItem()==key)
				return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the birr
	 */
	public int getBirr() {
		return birr;
	}

	//--------------------------------------------------------------------
	/**
	 * @param birr the birr to set
	 */
	public void setBirr(int birr) {
		this.birr = birr;
	}

	//-------------------------------------------------------------------
	@Override
	public List<TalentValue> getTalents() {
		// Take saved talents
		List<TalentValue> ret = super.getTalents();
		// Add Icon talent
		if (icon!=null) {
			for (Modification tmp : icon.getModifications()) {
				ret.add(new TalentValue( ((TalentModification)tmp).getTalent(), 0));
			}
		}
		// Sort alphabetically
		Collections.sort(ret);
		return ret;
	}

}
