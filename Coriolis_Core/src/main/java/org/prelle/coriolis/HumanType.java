/**
 * 
 */
package org.prelle.coriolis;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;
import org.prelle.yearzeroengine.YearZeroModule;

/**
 * @author prelle
 *
 */
@Root(name="humantype")
public class HumanType extends YearZeroModule {
	
	@Attribute
	private boolean humanite;

	//-------------------------------------------------------------------
	public HumanType() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.YearZeroModule#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("humantype."+getId());
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return "humantype."+getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "humantype."+getId()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "humantype."+getId()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the humanite
	 */
	public boolean isHumanite() {
		return humanite;
	}

}
