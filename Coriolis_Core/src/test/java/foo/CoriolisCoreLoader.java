/**
 * 
 */
package foo;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class CoriolisCoreLoader {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CoriolisCore.initialize(new DummyRulePlugin<CoriolisCharacter>(RoleplayingSystem.CORIOLIS, "CORE"));
		
//		for (CharacterConcept c : ShadowrunCore.getCharacterConcepts())
//			System.out.println("+ "+c);
	}

}
