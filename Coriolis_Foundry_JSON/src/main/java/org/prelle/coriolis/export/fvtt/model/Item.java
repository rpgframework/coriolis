package org.prelle.coriolis.export.fvtt.model;

/**
 * @author prelle
 *
 */
public class Item<T> {
	
	private String name;
	private String type;
	public String img;
	private T data;

	//-------------------------------------------------------------------
	public Item(String name, String type, T data) {
		this.name = name;
		this.type = type;
		this.data = data;
	}

}
