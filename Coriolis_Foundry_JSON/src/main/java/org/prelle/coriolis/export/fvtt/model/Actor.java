package org.prelle.coriolis.export.fvtt.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author prelle
 *
 */
public class Actor {
	
	public String name;
	public String type;
	public Lifeform data;
	public List<Item> items;
	private List<Object> effects;

	//-------------------------------------------------------------------
	public Actor(String name, String type, Lifeform data) {
		this.name = name;
		this.type = type;
		this.data = data;
		items = new ArrayList<>();
		effects = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void setItems(List<Item> value) {
		items = value;
	}

	//-------------------------------------------------------------------
	public void addItems(Item value) {
		items.add(value);
	}

}
