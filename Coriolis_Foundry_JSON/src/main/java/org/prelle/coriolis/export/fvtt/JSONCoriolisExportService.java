package org.prelle.coriolis.export.fvtt;

import java.util.List;

import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.HumanType;
import org.prelle.coriolis.ItemType;
import org.prelle.coriolis.TechTier;
import org.prelle.coriolis.export.fvtt.model.Actor;
import org.prelle.coriolis.export.fvtt.model.FVTTArmor;
import org.prelle.coriolis.export.fvtt.model.FVTTGear;
import org.prelle.coriolis.export.fvtt.model.FVTTRelationship;
import org.prelle.coriolis.export.fvtt.model.FVTTSkill;
import org.prelle.coriolis.export.fvtt.model.FVTTTalent;
import org.prelle.coriolis.export.fvtt.model.FVTTWeapon;
import org.prelle.coriolis.export.fvtt.model.Item;
import org.prelle.coriolis.export.fvtt.model.JSONCharacter;
import org.prelle.yearzeroengine.CharacterRelation;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.Talent.Type;
import org.prelle.yearzeroengine.TalentValue;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONCoriolisExportService {

    public String exportCharacter(CoriolisCharacter character) {
        Gson gson = new GsonBuilder()
        		.disableHtmlEscaping()
        		.setPrettyPrinting()
        		.create();
        return gson.toJson(getJSONCharacter(character));
    }

    //-------------------------------------------------------------------
    private JSONCharacter getJSONCharacter(CoriolisCharacter character) {
        JSONCharacter jsonCharacter = new JSONCharacter(character.getName());
        setGeneralInfo(jsonCharacter, character);
        setAttributes(jsonCharacter, character);
        setSkills(jsonCharacter, character);
        setBio(jsonCharacter, character);
        setRelationships(jsonCharacter, character);
        addTalents(jsonCharacter, character);
        addMeleeWeapons(jsonCharacter, character);
        addRangedWeapons(jsonCharacter, character);
        addArmor(jsonCharacter, character);
        addOtherGear(jsonCharacter, character);
        return jsonCharacter;
    }

    private String getPageString(int page, String productNameShort) {
        String pageBook;
        if (page == 0) {
            pageBook = " ";
        } else {
            pageBook = String.format("%s %s", productNameShort, page);
        }
        return pageBook;
    }

    //-------------------------------------------------------------------
    private void setSkills(JSONCharacter jsonCharacter, CoriolisCharacter character) {
    	for (Skill skill : CoriolisCore.getRuleset().getSkills()) {
    		FVTTSkill sVal = new FVTTSkill();
    		sVal.category  = skill.getCategory().name().toLowerCase();
    		sVal.attribute = skill.getAttribute().name().toLowerCase();
    		sVal.value     = character.getSkillValue(skill).getModifiedValue();
    		
    		String name = skill.getId().toLowerCase();
    		switch (name) {
    		case "mystic_powers": name="mysticpowers"; break;
    		case "ranged_combat": name="rangedcombat"; break;
    		case "melee_combat" : name="meleecombat"; break;
    		case "data_djinn" : name="datadjinn"; break;
    		}
    		jsonCharacter.data.skills.put(name, sVal);
    	}
    }

    //-------------------------------------------------------------------
    private void setRelationships(JSONCharacter actor, CoriolisCharacter model) {
    	for (CharacterRelation rel : model.getRelations()) {
    		FVTTRelationship toAdd = new FVTTRelationship();
    		toAdd.buddy = rel.isBuddy();
    		toAdd.name = rel.getTo()+" "+rel.getKind();
    		actor.data.relationships.put(rel.getTo(), toAdd);
    	}
    }

    //-------------------------------------------------------------------
    private void setAttributes(JSONCharacter jsonCharacter, CoriolisCharacter character) {
    	jsonCharacter.data.attributes.strength.value = character.getAttribute(AttributeCoriolis.STRENGTH).getModifiedValue();
    	jsonCharacter.data.attributes.strength.max = 5;
    	jsonCharacter.data.attributes.agility.value = character.getAttribute(AttributeCoriolis.AGILITY).getModifiedValue();
    	jsonCharacter.data.attributes.agility.max = 5;
    	jsonCharacter.data.attributes.wits.value = character.getAttribute(AttributeCoriolis.WITS).getModifiedValue();
    	jsonCharacter.data.attributes.wits.max = 5;
    	jsonCharacter.data.attributes.empathy.value = character.getAttribute(AttributeCoriolis.EMPATHY).getModifiedValue();
    	jsonCharacter.data.attributes.empathy.max = 5;
    	
    	jsonCharacter.data.hitPoints.value = character.getAttribute(AttributeCoriolis.HIT_POINTS).getModifiedValue();
    	jsonCharacter.data.mindPoints.value = character.getAttribute(AttributeCoriolis.MIND_POINTS).getModifiedValue();
    	jsonCharacter.data.reputation.value = character.getAttribute(AttributeCoriolis.REPUTATION).getModifiedValue();
    	jsonCharacter.data.reputation.max = 10;
    	jsonCharacter.data.radiation.value = character.getAttribute(AttributeCoriolis.RADIATION).getModifiedValue();
    	jsonCharacter.data.radiation.max = 10;
    }

   //-------------------------------------------------------------------
   private void setGeneralInfo(JSONCharacter jsonCharacter, CoriolisCharacter character) {
        jsonCharacter.name = character.getName();
    	jsonCharacter.data.experience.value = character.getExpInvested();
    	jsonCharacter.data.experience.max = character.getExpInvested();
    	jsonCharacter.data.birr = character.getBirr();
    	jsonCharacter.data.keyArt = "systems/yzecoriolis/css/images/unknown_player.png";
   }

   //-------------------------------------------------------------------
   private void setBio(JSONCharacter json, CoriolisCharacter model) {
	   json.data.bio.origin = model.getOrigin().getName()+"/"+model.getHomeSystem().getName();
	   json.data.bio.upbringing = model.getUpbringing().getName();
	   json.data.bio.concept    = model.getCharacterClass().getName()+"/"+model.getSubconcept().getName();
	   json.data.bio.groupConcept = model.getGroupConcept().getName();
	   json.data.bio.personalProblem = model.getProblem();
	   json.data.bio.appearance.clothing = model.getClothing();
	   json.data.bio.appearance.face = model.getFace();
	   json.data.bio.humanite = model.getHumanType().isHumanite();
	   if (model.getIcon()!=null) {
		   json.data.bio.icon = model.getIcon().getName();
	   }
   }

	//-------------------------------------------------------------------
	private void addTalents(Actor actor, CoriolisCharacter model) {
		for (TalentValue val : model.getTalents()) {
			FVTTTalent fVal = new FVTTTalent();
			// Definition fields
			fVal.genesisID = val.getTalent().getId();
			switch (val.getTalent().getType()) {
			case BIONIC: fVal.category  = "bionicsculpt"; break;
			case MYSTICAL: fVal.category  = "mysticalpowers"; break;
			default:
				fVal.category  = val.getTalent().getType().name().toLowerCase();
			}
			fVal.description = "<p>"+val.getTalent().getHelpText()+"</p>";

			Item<FVTTTalent> item = new Item<FVTTTalent>(val.getTalent().getName(), "talent", fVal);
			item.img = "systems/yzecoriolis/css/icons/talent-icon.svg";
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addMeleeWeapons(Actor actor, CoriolisCharacter model) {
		for (CoriolisCarriedItem val : model.getItems(ItemType.WEAPON_MELEE)) {
			FVTTWeapon fVal = new FVTTWeapon();
			// Definition fields
			fVal.genesisID = val.getItem().getId();
			switch (val.getWeight()) {
			case LIGHT: fVal.weight = "L";
			case HEAVY: fVal.weight = "H";
			case NORMAL: fVal.weight = "N";
			case TINY: fVal.weight = "T";
			}
			if (val.getTechTier()!=null)
				fVal.techTier = val.getTechTier().name();
			else
				fVal.techTier  = TechTier.O.name();
			fVal.quantity = (val.getCount()>0)?val.getCount():1;
			fVal.cost     = val.getCost();
			if (val.getItem().getHelpText()!=null) {
				fVal.description = "<p>"+val.getItem().getHelpText()+"</p>";
			}
			fVal.restricted = val.getItem().isRestricted();
			fVal.equipped = val.getLocation()==ItemLocation.BODY;
			fVal.bonus    = val.getBonus();
			fVal.crit.numericValue = val.getCrit();
			fVal.damage = val.getDamage();
			fVal.initiative = val.getInitiative();
			fVal.melee = true;
			fVal.equipped = val.getLocation()==ItemLocation.BODY;

			Item<FVTTWeapon> item = new Item<FVTTWeapon>(val.getName(), "weapon", fVal);
			item.img = "systems/yzecoriolis/css/icons/weapons-icon.svg";
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addRangedWeapons(Actor actor, CoriolisCharacter model) {
		for (CoriolisCarriedItem val : model.getItems(ItemType.WEAPON_RANGE)) {
			FVTTWeapon fVal = new FVTTWeapon();
			// Definition fields
			fVal.genesisID = val.getItem().getId();
			switch (val.getWeight()) {
			case LIGHT: fVal.weight = "L";
			case HEAVY: fVal.weight = "H";
			case NORMAL: fVal.weight = "N";
			case TINY: fVal.weight = "T";
			}
			if (val.getTechTier()!=null)
				fVal.techTier = val.getTechTier().name();
			else
				fVal.techTier  = TechTier.O.name();
			fVal.quantity = (val.getCount()>0)?val.getCount():1;
			fVal.cost     = val.getCost();
			if (val.getItem().getHelpText()!=null) {
				fVal.description = "<p>"+val.getItem().getHelpText()+"</p>";
			}
			fVal.restricted = val.getItem().isRestricted();
			fVal.equipped = val.getLocation()==ItemLocation.BODY;
			fVal.bonus    = val.getBonus();
			fVal.crit.numericValue = val.getCrit();
			fVal.damage = val.getDamage();
			fVal.initiative = val.getInitiative();
			fVal.melee = false;
			fVal.equipped = val.getLocation()==ItemLocation.BODY;

			Item<FVTTWeapon> item = new Item<FVTTWeapon>(val.getName(), "weapon", fVal);
			item.img = "systems/yzecoriolis/css/icons/weapons-icon.svg";
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addArmor(Actor actor, CoriolisCharacter model) {
		for (CoriolisCarriedItem val : model.getItems(ItemType.ARMOR)) {
			FVTTArmor fVal = new FVTTArmor();
			// Definition fields
			fVal.genesisID = val.getItem().getId();
			switch (val.getWeight()) {
			case LIGHT: fVal.weight = "L";
			case HEAVY: fVal.weight = "H";
			case NORMAL: fVal.weight = "N";
			case TINY: fVal.weight = "T";
			}
			if (val.getTechTier()!=null)
				fVal.techTier = val.getTechTier().name();
			else
				fVal.techTier  = TechTier.O.name();
			fVal.quantity = (val.getCount()>0)?val.getCount():1;
			fVal.cost     = val.getCost();
			if (val.getItem().getHelpText()!=null) {
				fVal.description = "<p>"+val.getItem().getHelpText()+"</p>";
			}
			fVal.restricted = val.getItem().isRestricted();
			fVal.equipped = val.getLocation()==ItemLocation.BODY;
			fVal.bonus    = val.getBonus();
			fVal.armorRating = val.getArmorRating();

			Item<FVTTArmor> item = new Item<FVTTArmor>(val.getName(), "armor", fVal);
			item.img = "systems/yzecoriolis/css/icons/weapons-icon.svg";
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addOtherGear(Actor actor, CoriolisCharacter model) {
		for (CoriolisCarriedItem val : model.getItems()) {
			if (List.of(ItemType.WEAPON_RANGE, ItemType.WEAPON_MELEE, ItemType.ARMOR).contains(val.getItem().getType()))
				continue;
			FVTTGear fVal = new FVTTGear();
			// Definition fields
			fVal.genesisID = val.getItem().getId();
			switch (val.getWeight()) {
			case LIGHT: fVal.weight = "L";
			case HEAVY: fVal.weight = "H";
			case NORMAL: fVal.weight = "N";
			case TINY: fVal.weight = "T";
			}
			if (val.getTechTier()!=null)
				fVal.techTier = val.getTechTier().name();
			else
				fVal.techTier  = TechTier.O.name();
			fVal.quantity = val.getCount();
			fVal.cost     = val.getCost();
			if (val.getItem().getHelpText()!=null) {
				fVal.description = "<p>"+val.getItem().getHelpText()+"</p>";
			}
			fVal.restricted = val.getItem().isRestricted();
			fVal.equipped = val.getLocation()==ItemLocation.BODY;

			Item<FVTTGear> item = new Item<FVTTGear>(val.getName(), "gear", fVal);
			item.img = "systems/yzecoriolis/css/icons/gear-icon.svg";
			actor.addItems(item);
		}
	}

}
