package org.prelle.coriolis.export.fvtt.model;

/**
 * @author prelle
 *
 */
public class PrimaryAttributes {
	
	public static class AttributeValue {
		public int value;
		public int min;
		public int max;
	}

	public AttributeValue strength;
	public AttributeValue agility;
	public AttributeValue wits;
	public AttributeValue empathy;
	
	//-------------------------------------------------------------------
	/**
	 */
	public PrimaryAttributes() {
		strength = new AttributeValue();
		agility  = new AttributeValue();
		wits     = new AttributeValue();
		empathy  = new AttributeValue();
	}

}
