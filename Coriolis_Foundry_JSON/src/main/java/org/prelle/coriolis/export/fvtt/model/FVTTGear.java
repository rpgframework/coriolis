package org.prelle.coriolis.export.fvtt.model;

/**
 * @author prelle
 *
 */
public class FVTTGear {

	public String genesisID;
	public String description;
	public String weight;
	public String techTier;
	public int    cost;
	public int    quantity =1;
	public boolean restricted;
	public boolean equipped;
	public int bonus;
	
	//-------------------------------------------------------------------
	public FVTTGear() {
		// TODO Auto-generated constructor stub
	}

}
