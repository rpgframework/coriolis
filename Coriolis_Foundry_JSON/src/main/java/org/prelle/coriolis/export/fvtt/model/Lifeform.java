package org.prelle.coriolis.export.fvtt.model;

import java.util.LinkedHashMap;

import org.prelle.coriolis.export.fvtt.model.PrimaryAttributes.AttributeValue;

/**
 * @author prelle
 *
 */
public class Lifeform {
	
	public static class Edge {
		public int value;
		public int max;
	}
	
	public AttributeValue hitPoints;
	public AttributeValue mindPoints;
	public AttributeValue experience;
	public AttributeValue radiation;
	public AttributeValue reputation;
	public PrimaryAttributes attributes;
	
	public LinkedHashMap<String, FVTTSkill> skills;
	public String notes;
	public FVTTBio bio;
	public int birr;
	public String keyArt;
	public LinkedHashMap<String, FVTTRelationship> relationships;

	//-------------------------------------------------------------------
	/**
	 */
	public Lifeform() {
		hitPoints = new AttributeValue();
		mindPoints = new AttributeValue();
		experience = new AttributeValue();
		radiation = new AttributeValue();
		reputation = new AttributeValue();
		attributes = new PrimaryAttributes();
		bio = new FVTTBio();
		skills = new LinkedHashMap<>();
		relationships = new LinkedHashMap<>();
	}

}
