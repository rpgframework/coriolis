package org.prelle.coriolis.export.fvtt.model;

public class FVTTBio {
	
	public static class Appearance {
		public String face;
		public String clothing;
	}

	public String origin;
	public String upbringing;
	public boolean humanite;
	public String concept;
	public String icon;
	public String groupConcept;
	public String personalProblem;
	public Appearance appearance = new Appearance();

}
