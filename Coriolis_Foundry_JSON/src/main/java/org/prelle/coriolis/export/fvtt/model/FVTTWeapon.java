package org.prelle.coriolis.export.fvtt.model;

/**
 * @author prelle
 *
 */
public class FVTTWeapon extends FVTTGear {
	
	public static class Crit {
		public int numericValue;
		public String customValue;
	}

	public Crit crit = new Crit();
	
	public int damage;
	public String damageText;
	public int initiative;
	public String range;
	
	public boolean melee;
	
	//-------------------------------------------------------------------
	public FVTTWeapon() {
		// TODO Auto-generated constructor stub
	}

}
