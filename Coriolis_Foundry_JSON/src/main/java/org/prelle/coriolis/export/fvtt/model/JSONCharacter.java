package org.prelle.coriolis.export.fvtt.model;

import de.rpgframework.core.RoleplayingSystem;

import java.util.List;

public class JSONCharacter extends Actor {
	
    private String system = RoleplayingSystem.CORIOLIS.name();
    private String version = "0.0.2";

    public JSONCharacter(String name) {
    	super(name, "character", new Lifeform());
    }
    
}
