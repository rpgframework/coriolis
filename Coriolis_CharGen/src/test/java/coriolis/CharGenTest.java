package coriolis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.chargen.CoriolisCharacterClassGenerator;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.modifications.KeyAttributeModification;

import de.rpgframework.character.DecodeEncodeException;

/**
 * @author prelle
 *
 */
public class CharGenTest {

	private CoriolisCharacter model;
	private Serializer serializer;

	private CoriolisCharacterGenerator ctrl;

	//-------------------------------------------------------------------
	static {
		CoriolisCore.initialize(new DummyRulePlugin<>());
//		System.exit(0);

	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ctrl = new CoriolisCharacterGenerator();
		model  = ctrl.getCharacter();
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(-8, ctrl.getAttributeController().getPointsToSpend());
	}

	//-------------------------------------------------------------------
	@Test
	public void testUpringing() {
		assertNotNull("Cannot find upbringing by ID",CoriolisCore.getUpbringing("privileged"));
		ctrl.getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("privileged"));
		assertEquals(5, ctrl.getAttributeController().getPointsToSpend());
	}

	//-------------------------------------------------------------------
	@Test
	public void testConcept() {
		ctrl.getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("privileged"));
		CoriolisCharacterClassGenerator clsCtrl = (CoriolisCharacterClassGenerator)ctrl.getCharacterClassController();

		assertNotNull(clsCtrl.getAvailableClasses());
		assertFalse(clsCtrl.getAvailableClasses().isEmpty());
		assertTrue(clsCtrl.getAvailableClasses().contains(CoriolisCore.getCharacterClass("artist")));
		clsCtrl.setSelected(CoriolisCore.getCharacterClass("artist"));

		assertFalse(clsCtrl.getDecisions().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testAttributes() {
		ctrl.getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("privileged"));
		AttributeValue empathy = model.getAttribute(AttributeCoriolis.EMPATHY);
		assertEquals(2, empathy.getPoints());
		assertTrue(ctrl.getAttributeController().canBeIncreased(empathy));
		assertTrue(ctrl.getAttributeController().increase(empathy));
		assertEquals(3, model.getAttribute(AttributeCoriolis.EMPATHY).getPoints());
		assertTrue(ctrl.getAttributeController().canBeIncreased(empathy));
		assertTrue(ctrl.getAttributeController().increase(empathy));
		assertEquals(4, model.getAttribute(AttributeCoriolis.EMPATHY).getPoints());
		assertFalse("Raising to 5 only for key attribute",ctrl.getAttributeController().canBeIncreased(empathy));

		KeyAttributeModification mod = new KeyAttributeModification(AttributeCoriolis.EMPATHY);
		((CoriolisCharacterGenerator)ctrl).addUnitTestModification(mod);
		((CoriolisCharacterGenerator)ctrl).runProcessors();
		assertEquals(4, model.getAttribute(AttributeCoriolis.EMPATHY).getPoints());
		assertTrue("Raising to 5 should be possible for key attribute",ctrl.getAttributeController().canBeIncreased(empathy));
	}

	//-------------------------------------------------------------------
	@Test
	public void testSave() throws DecodeEncodeException, UnsupportedEncodingException {
		testAttributes();

		CoriolisCharacter pc = ctrl.getCharacter();
		byte[] raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		String dump = new String(raw, "UTF-8");
		System.out.println(dump);
	}

}
