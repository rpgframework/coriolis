package coriolis;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

public class SkillGenTest implements GenerationEventListener {

	class TestPlugin implements RulePlugin<CoriolisCharacter> {
		public boolean willProcessCommand(Object src, CommandType type, Object... values) {
			return false;
		}
		public CommandResult handleCommand(Object src, CommandType type, Object... values) {
			return null;
		}
		public String getID() {return "CORE";}
		public String getReadableName() {return "CORE";}
		public RoleplayingSystem getRules() {return RoleplayingSystem.SPACE1889;}
		public Collection<RulePluginFeatures> getSupportedFeatures() {
			return null;
		}
		public List<ConfigOption<?>> getConfiguration() {
			return null;
		}
		public void init(RulePluginProgessListener callback) {}
		public InputStream getAboutHTML() {return null;}
		public Collection<String> getRequiredPlugins() {return new ArrayList<>();}
		public void attachConfigurationTree(ConfigContainer addBelow) { }
		public List<String> getLanguages() {
			return Arrays.asList(Locale.GERMAN.getLanguage());
		}
	}

	private static YearZeroEngineRuleset ruleset;
	private static RulePlugin<CoriolisCharacter> plugin = new SkillGenTest().new TestPlugin();
	private static Skill SCIENCE;
	private static Skill BIOLOGY;
	private static Skill PHYSICS;
	private static Skill LARCENY;
	
	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() {
		CoriolisCore.initialize(new DummyRulePlugin<>());
	}
	
//	private SkillGenerator skillGen;
	private CoriolisCharacter model;
	private List<GenerationEvent> events;

	//--------------------------------------------------------------------
	public SkillGenTest() {
		events   = new ArrayList<GenerationEvent>();
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.coriolis.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		events.add(event);
	}

	//--------------------------------------------------------------------
	private boolean eventWasDelivered(GenerationEventType type) {
		for (GenerationEvent ev : events) {
			if (ev.getType()==type)
				return true;
		}
		return false;
	}
	
	//--------------------------------------------------------------------
	@Before
	public void setUp() throws Exception {
		model = new CoriolisCharacter();
//		skillGen = new SkillGenerator(template, ruleset, model);
		events.clear();
	}

	//--------------------------------------------------------------------
	@Test
	public void testInitial() {
//		assertTrue(skillGen.getAvailableSkills().contains(BIOLOGY));
//		assertTrue(skillGen.getAvailableSkills().contains(PHYSICS));
//		assertFalse(skillGen.getAvailableSkills().contains(SCIENCE));		
//		assertTrue(skillGen.getAvailableSkills().contains(LARCENY));
	}

	//--------------------------------------------------------------------
	@Test
	public void testIncGroupWithoutTalent() {
		System.out.println("---testIncGroupWithoutTalent---------------------");
		/*
		 * Increasing a skillgroup without having the skillmaster
		 * talent should be impossible
		 */
//		assertFalse(skillGen.canBeIncreased(SCIENCE));
//		assertFalse(skillGen.increase(SCIENCE));
		
	}
	
}
