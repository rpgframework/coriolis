/**
 *
 */
package coriolis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.SelectOption;
import org.prelle.coriolis.Selection;
import org.prelle.coriolis.TechTier;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.chargen.CoriolisGearGenerator;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.CostRange;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.modifications.CurrencyModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;
import org.prelle.yearzeroengine.modifications.ItemModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class GearGeneratorTest {

	private CoriolisCharacterGenerator parent;
	private CoriolisGearGenerator gear;
	private CoriolisCharacter model;

	//-------------------------------------------------------------------
	static {
		CoriolisCore.initialize(new DummyRulePlugin<>());
//		System.exit(0);

	}

	//--------------------------------------------------------------------
	@Before
	public void setUp() {
		parent = new CoriolisCharacterGenerator();
		model = parent.getCharacter();
		gear = new CoriolisGearGenerator(parent);
	}

	//--------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(0, gear.getEncumbrancePointsLeft(), 0);
	}

	//--------------------------------------------------------------------
	@Test
	public void testAcceptPoints() {
		List<Modification> previous = new ArrayList<>();
		previous.add(new FreePointsModification(Type.ENCUMBRANCE, 6));
		gear.process(model, previous);
		assertEquals(6, gear.getEncumbrancePointsLeft(), 0);
	}

	//--------------------------------------------------------------------
	@Test
	public void testPointCalculation() {
		System.out.println("testPointCalculation");
		CoriolisItemTemplate template0 = new CoriolisItemTemplate("t0", TechTier.O, Weight.TINY);
		CoriolisItemTemplate template1 = new CoriolisItemTemplate("t1", TechTier.O, Weight.LIGHT);
		CoriolisItemTemplate template2 = new CoriolisItemTemplate("t2", TechTier.O, Weight.NORMAL);
		CoriolisItemTemplate template3 = new CoriolisItemTemplate("t3", TechTier.O, Weight.HEAVY);

		List<Modification> previous = new ArrayList<>();
		previous.add(new FreePointsModification(Type.ENCUMBRANCE, 6));
		previous.add(new ItemModification(template0));
		previous.add(new ItemModification(template1));
		previous.add(new ItemModification(template2));
		previous.add(new ItemModification(template3));
		gear.process(model, previous);
		assertEquals("Encumbrance not calculated", 2.5, gear.getEncumbrancePointsLeft(), 0);
	}

	//--------------------------------------------------------------------
	@Test
	public void testPaying() {
		System.out.println("testPaying");

		CoriolisItemTemplate template0 = new CoriolisItemTemplate("t0", TechTier.O, Weight.TINY);
		CoriolisItemTemplate template1 = new CoriolisItemTemplate("t1", TechTier.O, Weight.LIGHT);
		template0.setCost(new CostRange(200, 0));
		template1.setCost(new CostRange(500, 800));

		List<Modification> previous = new ArrayList<>();
		previous.add(new FreePointsModification(Type.ENCUMBRANCE, 6));
		previous.add(new CurrencyModification(10000));
		parent.addUnitTestModification(new FreePointsModification(Type.ENCUMBRANCE, 6));
		parent.addUnitTestModification(new CurrencyModification(10000));


		gear.process(model, previous);

		CarriedItem<CoriolisItemTemplate> item0 = gear.select(template0, ItemLocation.BODY);
		assertNotNull(item0);
		assertEquals("Selecting did not pay fixed costs", 9800, model.getBirr());
		assertTrue("Item not selected", parent.getCharacter().getItems().contains(item0));

		CarriedItem<CoriolisItemTemplate> item1 = gear.select(template1, ItemLocation.BODY, new Selection(SelectOption.COST, 700));
		assertNotNull(item1);
		assertEquals("Selecting did not correctly pay variable costs", 9100, model.getBirr());
		assertTrue("Item not selected", parent.getCharacter().getItems().contains(item1));

		// Sell
		assertTrue("Cannot sell bought item", gear.canBeDeselected(item0));
		assertTrue("Cannot sell bought item", gear.canBeDeselected(item1));
		assertEquals(9100, model.getBirr());
		System.out.println("Sell");
		gear.deselect(item0);
		assertFalse("Item not deselected", parent.getCharacter().getItems().contains(item0));
		assertTrue("More items removed than instructred", parent.getCharacter().getItems().contains(item1));
		assertEquals("Deselecting did not return fixed costs", 9300, model.getBirr());
		gear.deselect(item1);
		assertEquals("Deselecting did not return fixed costs", 10000, model.getBirr());
	}

}
