package coriolis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.Origin;
import org.prelle.coriolis.charctrl.CoriolisGroupController;
import org.prelle.coriolis.chargen.CoriolisCharacterClassGenerator;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.RewardImpl;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.modifications.ModificationChoice;
import org.prelle.yearzeroengine.modifications.SkillModification;
import org.prelle.yearzeroengine.modifications.TalentModification;

import de.rpgframework.character.DecodeEncodeException;

/**
 * @author prelle
 *
 */
public class ExampleCharacterTest {

	private CoriolisCharacter model;
	private Serializer serializer;

	private CoriolisCharacterGenerator ctrl;

	//-------------------------------------------------------------------
	static {
	}

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() {
		//		PropertyConfigurator.configure(getClass().getResourceAsStream("log4j.properties"));
		CoriolisCore.initialize(new DummyRulePlugin<>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ctrl = new CoriolisCharacterGenerator();
		ctrl.start();
		model  = ctrl.getCharacter();
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testInara() throws IOException, URISyntaxException {
		// Define the group
		ctrl.getGroupController().setPCNames(Arrays.asList("Malcolm","Wash","Zoe","Jayne","Kaylee","Simon","River"));
		assertNotNull(CoriolisCore.getGroupConcept("free_traders"));
		((CoriolisGroupController)ctrl.getGroupController()).setGroupConcept(CoriolisCore.getGroupConcept("free_traders"));
		((CoriolisGroupController)ctrl.getGroupController()).setGroupTalent(ctrl.getRuleset().getByType(Talent.class, "a_friend_in_every_port"));

		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setOrigin(Origin.ZENITHIAN);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setHomeSystem(((CoriolisCharacterGenerator)ctrl).getUpbringingController().getRandomHomeSystem());
		assertEquals(0, ((CoriolisCharacter)ctrl.getCharacter()).getBirr(), 0);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("stationary"));
		assertEquals(1000, ((CoriolisCharacter)ctrl.getCharacter()).getBirr(), 0);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setHumanType(CoriolisCore.getHumanType("human"));
		assertEquals(10, ctrl.getSkillController().getPointsToSpend());

		CoriolisCharacterClassGenerator clsCtrl = (CoriolisCharacterClassGenerator)ctrl.getCharacterClassController();
		clsCtrl.setSelected(CoriolisCore.getCharacterClass("artist"));
		for (int i=0; i<clsCtrl.getDecisions().size(); i++) {
			System.err.println("Before: Decision "+i+" = "+clsCtrl.getDecisions().get(i));
		}
		// Decision 1
		ModificationChoice choice = (ModificationChoice)clsCtrl.getDecisions().get(0).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(0), choice.getOptions()[2]);
		Talent seductive = CoriolisCore.getRuleset().getByType(Talent.class, "seductive");
		assertNotNull(seductive);
		assertNotNull("Talent from choice not selected", model.getTalentValue(seductive));
		// Decision 5
		choice = (ModificationChoice)clsCtrl.getDecisions().get(4).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(4), choice.getOptions()[0]);
		Talent beautiful = CoriolisCore.getRuleset().getByType(Talent.class, "beautiful");
		assertNotNull(beautiful);
		assertNotNull("Talent from choice not selected", model.getTalentValue(beautiful));

		clsCtrl.setSelectedSubConcept(CoriolisCore.getSubConcept("courtesan"));

		// Attributes
		AttributeValue empathy = model.getAttribute(AttributeCoriolis.EMPATHY);
		AttributeValue agility = model.getAttribute(AttributeCoriolis.AGILITY);
		AttributeValue wits    = model.getAttribute(AttributeCoriolis.WITS);
		assertTrue(ctrl.getAttributeController().canBeIncreased(empathy));
		assertTrue(ctrl.getAttributeController().increase(empathy));
		assertTrue(ctrl.getAttributeController().increase(empathy));
		assertTrue(ctrl.getAttributeController().increase(empathy));
		assertEquals(5, model.getAttribute(AttributeCoriolis.EMPATHY).getPoints());
		assertTrue(ctrl.getAttributeController().increase(agility));
		assertTrue(ctrl.getAttributeController().increase(agility));
		assertEquals(4, model.getAttribute(AttributeCoriolis.AGILITY).getPoints());
		assertTrue(ctrl.getAttributeController().increase(wits));
		assertEquals(3, model.getAttribute(AttributeCoriolis.WITS).getPoints());
		assertEquals(0, ctrl.getAttributeController().getPointsToSpend());

		// Skills
		assertEquals(10, ctrl.getSkillController().getPointsToSpend());
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("manipulation"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("manipulation"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("manipulation"))));
		assertFalse(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("manipulation"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("infiltration"))));
		assertFalse(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("infiltration"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("culture"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("culture"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("culture"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("dexterity"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("observation"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("medicurgy"))));
		assertEquals(0, ctrl.getSkillController().getPointsToSpend());

		// Items
		assertEquals("Encumbrance points not calculated correctly", 4.0f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		// Decision 2
		choice = (ModificationChoice)clsCtrl.getDecisions().get(1).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(1), choice.getOptions()[0]);
		CoriolisItemTemplate handFan = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "hand_fan");
		assertNotNull(handFan);
		assertTrue("Object from choice not selected", model.hasItem(handFan));
		assertEquals("Encumbrance points not calculated correctly", 3.5f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		// Decision 3
		choice = (ModificationChoice)clsCtrl.getDecisions().get(2).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(2), choice.getOptions()[0]);
		CoriolisItemTemplate opor = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "opor");
		assertNotNull(opor);
		assertTrue("Object from choice not selected", model.hasItem(opor));
		assertEquals("Tiny object encumbrance is 0", 3.5f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		// Decision 4
		choice = (ModificationChoice)clsCtrl.getDecisions().get(3).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(3), choice.getOptions()[0]);
		CoriolisItemTemplate clothing = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "clothing");
		assertNotNull(clothing);
		assertTrue("Object from choice not selected", model.hasItem(clothing));

		System.out.println("ToDos: "+ctrl.getToDos());
		assertEquals(0, ctrl.getToDos().size());

		// Add some equipment
		CoriolisItemTemplate item = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "communicator_personal");
		ctrl.getCharacter().addItem(new CoriolisCarriedItem(item));

		CoriolisCharacter pc = ctrl.getCharacter();
		pc.setName("Inara Serra");
		ctrl.stop();

		// Add exp#
		pc.addReward(new RewardImpl(6, "The dying ship"));
		pc.addToHistory(new SkillModification(CoriolisCore.getRuleset().getSkill("manipulation"), 3));

		// Dump without image
		byte[] raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		String dump = new String(raw, "UTF-8");
		System.out.println(dump);

		// Write with image
		pc.setImage(Files.readAllBytes(Paths.get(getClass().getResource("Inara.png").toURI())));
		raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		dump = new String(raw, "UTF-8");
		PrintWriter out = new PrintWriter(new File("target/"+pc.getName()+".xml"));
		out.println(dump);
		out.close();
	}

	//-------------------------------------------------------------------
	@Test
	public void testRiver() throws IOException, URISyntaxException {
		// Define the group
		ctrl.getGroupController().setPCNames(Arrays.asList("Malcolm","Wash","Zoe","Jayne","Kaylee","Simon","Inara"));
		assertNotNull(CoriolisCore.getGroupConcept("free_traders"));
		((CoriolisGroupController)ctrl.getGroupController()).setGroupConcept(CoriolisCore.getGroupConcept("free_traders"));
		((CoriolisGroupController)ctrl.getGroupController()).setGroupTalent(ctrl.getRuleset().getByType(Talent.class, "a_friend_in_every_port"));

		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setOrigin(Origin.ZENITHIAN);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setHomeSystem(((CoriolisCharacterGenerator)ctrl).getUpbringingController().getRandomHomeSystem());
		assertEquals(0, ((CoriolisCharacter)ctrl.getCharacter()).getBirr(), 0);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("plebeian"));
		assertEquals(500, ((CoriolisCharacter)ctrl.getCharacter()).getBirr(), 0);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setHumanType(CoriolisCore.getHumanType("human"));
		assertEquals(8, ctrl.getSkillController().getPointsToSpend());

		CoriolisCharacterClassGenerator clsCtrl = (CoriolisCharacterClassGenerator)ctrl.getCharacterClassController();
		clsCtrl.setSelected(CoriolisCore.getCharacterClass("fugitive"));
		for (int i=0; i<clsCtrl.getDecisions().size(); i++) {
			System.err.println("Before: Decision "+i+" = "+clsCtrl.getDecisions().get(i));
		}
		// Decision 1
		ModificationChoice choice = (ModificationChoice)clsCtrl.getDecisions().get(0).getChoice();
		clsCtrl.decide(clsCtrl.getDecisions().get(0), new TalentModification(ctrl.getRuleset().getByType(Talent.class, "premonition")));
		//		Talent seductive = CoriolisCore.getRuleset().getByType(Talent.class, "seductive");
		//		assertNotNull(seductive);
		//		assertNotNull("Talent from choice not selected", model.getTalentValue(seductive));
		//		// Decision 5
		//		choice = (ModificationChoice)clsCtrl.getDecisions().get(4).getChoice();
		//		clsCtrl.decide(clsCtrl.getDecisions().get(4), choice.getOptions()[0]);
		//		Talent beautiful = CoriolisCore.getRuleset().getByType(Talent.class, "beautiful");
		//		assertNotNull(beautiful);
		//		assertNotNull("Talent from choice not selected", model.getTalentValue(beautiful));

		clsCtrl.setSelectedSubConcept(CoriolisCore.getSubConcept("mystic"));

		// Attributes
		AttributeValue empathy = model.getAttribute(AttributeCoriolis.EMPATHY);
		AttributeValue agility = model.getAttribute(AttributeCoriolis.AGILITY);
		AttributeValue wits    = model.getAttribute(AttributeCoriolis.WITS);
		AttributeValue strength= model.getAttribute(AttributeCoriolis.STRENGTH);
		assertTrue(ctrl.getAttributeController().increase(empathy)); // 3
		assertTrue(ctrl.getAttributeController().increase(empathy)); // 4
		assertTrue(ctrl.getAttributeController().increase(empathy)); // 5
		assertEquals(5, model.getAttribute(AttributeCoriolis.EMPATHY).getPoints());
		assertTrue(ctrl.getAttributeController().increase(agility)); // 3
		assertTrue(ctrl.getAttributeController().increase(agility));  // 4
		assertEquals(4, model.getAttribute(AttributeCoriolis.AGILITY).getPoints());
		//		assertTrue(ctrl.getAttributeController().increase(strength));
		//		assertEquals(3, model.getAttribute(AttributeCoriolis.STRENGTH).getPoints());
		assertTrue(ctrl.getAttributeController().increase(wits));
		assertTrue(ctrl.getAttributeController().increase(wits));
		assertEquals(4, model.getAttribute(AttributeCoriolis.WITS).getPoints());
		assertEquals(0, ctrl.getAttributeController().getPointsToSpend());

		// Skills
		assertEquals(8, ctrl.getSkillController().getPointsToSpend());
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("mystic_powers"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("mystic_powers"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("mystic_powers"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("dexterity"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("dexterity"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("melee_combat"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("ranged_combat"))));
		assertTrue(ctrl.getSkillController().increase(model.getSkillValue(CoriolisCore.getRuleset().getSkill("pilot"))));
		assertEquals(0, ctrl.getSkillController().getPointsToSpend());

		// Mystic Powers >0 - select another talent
		assertFalse(ctrl.getTalentController().canBeSelected(ctrl.getRuleset().getByType(Talent.class, "regenerate")));
		assertFalse(ctrl.getTalentController().canBeSelected(ctrl.getRuleset().getByType(Talent.class, "mind_reader")));
//		ctrl.getTalentController().select(ctrl.getRuleset().getByType(Talent.class, "mind_reader"));

		// Items
		assertEquals("Encumbrance points not calculated correctly", 4.0f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		//		// Decision 2
		//		choice = (ModificationChoice)clsCtrl.getDecisions().get(1).getChoice();
		//		clsCtrl.decide(clsCtrl.getDecisions().get(1), choice.getOptions()[0]);
		//		CoriolisItemTemplate handFan = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "hand_fan");
		//		assertNotNull(handFan);
		//		assertTrue("Object from choice not selected", model.hasItem(handFan));
		//		assertEquals("Encumbrance points not calculated correctly", 3.5f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		//		// Decision 3
		//		choice = (ModificationChoice)clsCtrl.getDecisions().get(2).getChoice();
		//		clsCtrl.decide(clsCtrl.getDecisions().get(2), choice.getOptions()[0]);
		//		CoriolisItemTemplate opor = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "opor");
		//		assertNotNull(opor);
		//		assertTrue("Object from choice not selected", model.hasItem(opor));
		//		assertEquals("Tiny object encumbrance is 0", 3.5f, ctrl.getGearController().getEncumbrancePointsLeft(), 0);
		//		// Decision 4
		//		choice = (ModificationChoice)clsCtrl.getDecisions().get(3).getChoice();
		//		clsCtrl.decide(clsCtrl.getDecisions().get(3), choice.getOptions()[0]);
		//		CoriolisItemTemplate clothing = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "clothing");
		//		assertNotNull(clothing);
		//		assertTrue("Object from choice not selected", model.hasItem(clothing));

		System.out.println("ToDos: "+ctrl.getToDos());
		assertEquals(0, ctrl.getToDos().size());

		//		// Add some equipment
		//		CoriolisItemTemplate item = CoriolisCore.getRuleset().getByType(CoriolisItemTemplate.class, "communicator_personal");
		//		ctrl.getCharacter().addItem(new CoriolisCarriedItem(item));

		CoriolisCharacter pc = ctrl.getCharacter();
		pc.setName("River Tam");
		ctrl.stop();

		// Dump without image
		byte[] raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		String dump = new String(raw, "UTF-8");
		System.out.println(dump);

		// Write with image
		pc.setImage(Files.readAllBytes(Paths.get(getClass().getResource("River.png").toURI())));
		raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		dump = new String(raw, "UTF-8");
		PrintWriter out = new PrintWriter(new File("target/"+pc.getName()+".xml"));
		out.println(dump);
		out.close();
	}

	//-------------------------------------------------------------------
	@Test
	public void testHumanite() throws DecodeEncodeException, UnsupportedEncodingException {

		// Define the group
		ctrl.getGroupController().setPCNames(Arrays.asList("Other"));
		((CoriolisGroupController)ctrl.getGroupController()).setGroupConcept(CoriolisCore.getGroupConcept("free_traders"));

		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setOrigin(Origin.ZENITHIAN);
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setUpbringing(CoriolisCore.getUpbringing("plebeian"));
		System.out.println("Attr = "+model.getAttribute(AttributeCoriolis.REPUTATION));
		assertEquals(2, model.getAttribute(AttributeCoriolis.REPUTATION).getModifiedValue());
		((CoriolisCharacterGenerator)ctrl).getUpbringingController().setHumanType(CoriolisCore.getHumanType("sirb"));
		assertEquals(1, model.getAttribute(AttributeCoriolis.REPUTATION).getModifiedValue());
		assertEquals(8, ctrl.getSkillController().getPointsToSpend());

		CoriolisCharacterClassGenerator clsCtrl = (CoriolisCharacterClassGenerator)ctrl.getCharacterClassController();
		clsCtrl.setSelected(CoriolisCore.getCharacterClass("negotiator"));
		clsCtrl.setSelectedSubConcept(CoriolisCore.getSubConcept("peddler"));

		System.out.println("ToDos: "+ctrl.getToDos());
		//		assertEquals(0, ctrl.getToDos().size());
		ctrl.stop();
		CoriolisCharacter pc = ctrl.getCharacter();
		byte[] raw = CoriolisCore.save(pc);
		assertNotNull(raw);
		String dump = new String(raw, "UTF-8");
		System.out.println(dump);
	}

}
