/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.GroupConcept;
import org.prelle.coriolis.charctrl.CoriolisGroupController;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.chargen.GroupGenerator;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CoriolisGroupGenerator extends GroupGenerator<CoriolisCharacter> implements CoriolisGroupController, CharacterProcessor {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private final static PropertyResourceBundle RES = CoriolisCharGenConstants.RES;

	//-------------------------------------------------------------------
	/**
	 */
	public CoriolisGroupGenerator(CoriolisCharacterGenerator parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisGroupController#setGroupConcept(org.prelle.coriolis.GroupConcept)
	 */
	@Override
	public void setGroupConcept(GroupConcept value) {
		parent.getCharacter().setGroupConcept(value);

		logger.info("Set group concept to "+value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisGroupController#getGroupConcept()
	 */
	@Override
	public GroupConcept getGroupConcept() {
		return parent.getCharacter().getGroupConcept();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		try {
			unprocessed.addAll(super.process(model, previous));

			if (parent.getCharacter().getGroupConcept()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.group.concept_not_set") ));
			} else {
				unprocessed.addAll(parent.getCharacter().getGroupConcept().getModifications());
			}

			if (parent.getCharacter().getGroupTalent()==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, RES.getString("todo.group.talent_not_set") ));
			} else {
				unprocessed.addAll(parent.getCharacter().getGroupTalent().getModifications());
			}
		} finally {
			logger.trace("STOP : process");

		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisGroupController#setGroupTalent(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public void setGroupTalent(Talent value) {
		parent.getCharacter().setGroupTalent(value);

		logger.info("Set group talent to "+value);
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisGroupController#getGroupTalent()
	 */
	@Override
	public Talent getGroupTalent() {
		return parent.getCharacter().getGroupTalent();
	}

}
