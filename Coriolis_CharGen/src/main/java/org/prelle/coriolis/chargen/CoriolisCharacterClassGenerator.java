/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCharacterRelation;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.SubConcept;
import org.prelle.coriolis.charctrl.CoriolisCharacterClassController;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.CharacterRelation;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.chargen.CharacterClassGenerator;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.modifications.RecommendationModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CoriolisCharacterClassGenerator extends CharacterClassGenerator<CoriolisCharacter> implements CoriolisCharacterClassController {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;

	private SubConcept selectedSub;

	private List<SubConcept> recommended;

	//-------------------------------------------------------------------
	public CoriolisCharacterClassGenerator(CoriolisCharacterGenerator parent) {
		super(parent, CoriolisCore.getRuleset(), parent.geti18nResources());
		recommended = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisCharacterClassController#setAllowUnusualConcepts(boolean)
	 */
	@Override
	public void setAllowUnusualConcepts(boolean permissive) {

	}

	//-------------------------------------------------------------------
	@Override
	public List<SubConcept> getAvailableSubConcepts() {
		List<SubConcept> ret = new ArrayList<>();
		for (SubConcept tmp : ruleset.getListByType(SubConcept.class)) {
			logger.info("check "+tmp+"/"+tmp.getConcept()+" matches "+getSelected()+" = "+(tmp.getConcept()==getSelected()));
			if (tmp.getConcept()==getSelected())
				ret.add(tmp);
		}
		Collections.sort(ret);
		logger.info("ret "+ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisCharacterClassController#isRecommended(org.prelle.yearzeroengine.CharacterClass)
	 */
	@Override
	public boolean isRecommended(CharacterClass value) {
		for (SubConcept tmp : recommended) {
			if (tmp.getConcept()==value)
				return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisCharacterClassController#isRecommended(org.prelle.coriolis.SubConcept)
	 */
	@Override
	public boolean isRecommended(SubConcept value) {
		return recommended.contains(value);

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisCharacterClassController#getSelectedSubConcept()
	 */
	@Override
	public SubConcept getSelectedSubConcept() {
		return selectedSub;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisCharacterClassController#setSelectedSubConcept(org.prelle.coriolis.SubConcept)
	 */
	@Override
	public void setSelectedSubConcept(SubConcept value) {
		if (value!=null && value.getConcept()!=parent.getCharacter().getCharacterClass())
			throw new IllegalArgumentException("Trying to select unavailable subconcept: "+value+" - valid are: "+getAvailableSubConcepts());
		selectedSub = value;

		logger.info("Select subconcept is "+value);
		parent.getCharacter().setSubconcept(value);

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.CharacterClassController#rollRandomRelations(org.prelle.yearzeroengine.YearZeroEngineCharacter)
	 */
	@Override
	public void rollRandomRelations(YearZeroEngineCharacter model) {
		if (model.getCharacterClass()==null) {
			logger.warn("Cannot randomize relations - character class not selected yet");
			return;
		}

		logger.debug("rollRandomRelations");
		List<CharacterRelation> toSet = new ArrayList<>();
		Random random = new Random();
		List<String> options = new ArrayList<>();
		try {
			for (String name : parent.getGroupController().getPCNames()) {
				if (options.isEmpty()) {
					options = model.getCharacterClass().getFluffOptions("relatePC");
				}
				if (options.isEmpty()) {
					logger.warn("No relations defined for concept "+model.getCharacterClass().getId());
					System.exit(0);
					return;
				}
				// Roll from remaining options
				int roll = random.nextInt(options.size());
				String rolled = options.get(roll);
				options.remove(rolled);
				toSet.add( new CoriolisCharacterRelation(name, rolled) );
			}
		} catch (Exception e) {
			logger.error("Error randomizing relations",e);
		}

		if (toSet.isEmpty())
			return;

		model.setRelations(toSet);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));

		// Find one relation to define as a buddy
		if (model.getRelations().size()>0) {
			CoriolisCharacterRelation buddyRel = (CoriolisCharacterRelation) model.getRelations().get(random.nextInt(model.getRelations().size()));
			buddyRel.setBuddy(true);
		}
	}

	//-------------------------------------------------------------------
	public void rollPersonalProblem(CoriolisCharacter model) {
		if (model.getCharacterClass()==null) {
			logger.warn("Cannot randomize problem - character class not selected yet");
			return;
		}

		logger.debug("rollPersonalProblem");
		Random random = new Random();
		List<String> options = model.getCharacterClass().getFluffOptions("problem");
		if (options.isEmpty())
			return;
		model.setProblem(options.get(random.nextInt(options.size())));

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		try {
			// Check for recommendations
			recommended.clear();
			for (Modification mod : previous) {
				if (mod instanceof RecommendationModification && ((RecommendationModification)mod).getType().equals("subconcept")) {
					String fullID = ((RecommendationModification)mod).getReferencedId();
//					String conceptID = fullID.substring(0, fullID.indexOf("/"));
					String subconceptID = fullID.substring(fullID.indexOf("/")+1, fullID.length());
					SubConcept subconcept = CoriolisCore.getSubConcept(subconceptID);
					if (subconcept!=null) {
						logger.debug("  recommended subconcept "+subconceptID);
						recommended.add(subconcept);
					} else
						logger.error("Unknown subconcept recommended: "+subconceptID+"  (fully "+fullID+") from "+mod.getSource());
				} else
					unprocessed.add(mod);
			}

			// Call general YearZeroEngine processing
			unprocessed = super.process(model, unprocessed);
			if (selectedSub==null) {
				toDos.add(new ToDoElement(Severity.STOPPER, i18n.getString("todo.subconcept_not_selected")));
			} else {
				logger.debug("Select subconcept: "+selectedSub.getId());
				unprocessed.addAll(selectedSub.getModifications());
			}

			/*
			 * Personal problems
			 */
			if ( ((CoriolisCharacter)model).getProblem()==null) {
				rollPersonalProblem((CoriolisCharacter) model);
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

}
