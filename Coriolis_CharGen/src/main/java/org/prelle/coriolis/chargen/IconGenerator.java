/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.Icon;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class IconGenerator implements CharacterProcessor {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private final static Random RANDOM = new Random();

	private CoriolisCharacterGenerator parent;
	protected List<ToDoElement> toDos;

	//-------------------------------------------------------------------
	public IconGenerator(CoriolisCharacterGenerator parent) {
		this.parent = parent;
		toDos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void rollIcon() {
		rollIcon(parent.getCharacter());
		logger.info("Random icon is "+parent.getCharacter().getIcon());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ICON_CHANGED, parent.getCharacter().getIcon()));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	private void rollIcon(CoriolisCharacter model) {
		int roll = (RANDOM.nextInt(6)+1)*10 + (RANDOM.nextInt(6)+1);
		if (roll>=11 && roll<=14) {
			model.setIcon(CoriolisCore.getIcon("lady_of_tears"));
		} else if (roll>=15 && roll<=22) {
			model.setIcon(CoriolisCore.getIcon("dancer"));
		} else if (roll>=23 && roll<=26) {
			model.setIcon(CoriolisCore.getIcon("gambler"));
		} else if (roll>=31 && roll<=34) {
			model.setIcon(CoriolisCore.getIcon("merchant"));
		} else if (roll>=35 && roll<=42) {
			model.setIcon(CoriolisCore.getIcon("deckhand"));
		} else if (roll>=43 && roll<=46) {
			model.setIcon(CoriolisCore.getIcon("traveler"));
		} else if (roll>=51 && roll<=54) {
			model.setIcon(CoriolisCore.getIcon("messenger"));
		} else if (roll>=55 && roll<=62) {
			model.setIcon(CoriolisCore.getIcon("judge"));
		} else if (roll>=63 && roll<=66) {
			model.setIcon(CoriolisCore.getIcon("faceless_one"));
		}
	}

	//-------------------------------------------------------------------
	public void setIcon(Icon value) {
		logger.info("Set icon to "+value);
		parent.getCharacter().setIcon(value);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ICON_CHANGED, parent.getCharacter().getIcon()));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter genModel, List<Modification> previous) {
		logger.trace("START: process");
		CoriolisCharacter model = (CoriolisCharacter)genModel;
		List<Modification> unprocessed = new ArrayList<>(previous);
		toDos.clear();
		try {
			if (model.getIcon()==null) {
				rollIcon(model);
			}

			/*
			 * Instead of applying the talent modification here, rework
			 * getTalents() on the CoriolisCharacter in a way, that it injects
			 * the talent from the chosen icon
			 */
//			logger.debug("Add mods = "+model.getIcon().getModifications());
//			unprocessed.addAll(model.getIcon().getModifications());
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
