/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.Selection;
import org.prelle.coriolis.charctrl.CoriolisGearController;
import org.prelle.coriolis.charlvl.CoriolisGearLeveller;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemTemplate;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.modifications.CurrencyModification;
import org.prelle.yearzeroengine.modifications.ItemModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CoriolisGearGenerator extends CoriolisGearLeveller implements
CoriolisGearController {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;

	private List<CoriolisCarriedItem> freeItems;

	//--------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public CoriolisGearGenerator(CharacterController<CoriolisCharacter> parent) {
		super(parent);
		freeItems = new ArrayList<CoriolisCarriedItem>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#canBeDeselected(org.prelle.yearzeroengine.CarriedItem)
	 */
	@Override
	public <T extends ItemTemplate> boolean canBeDeselected(CarriedItem<T> item) {
		if (freeItems.contains(item))
			return false;
		return super.canBeDeselected(item);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#deselect(org.prelle.yearzeroengine.CarriedItem)
	 */
	@Override
	public <T extends ItemTemplate> void deselect(CarriedItem<T> value) {
		super.deselect(value);



		((CoriolisCharacterGenerator)parent).runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		logger.trace("START: process");
		CoriolisCharacter cmodel = (CoriolisCharacter)model;
		List<Modification> unprocessed = new ArrayList<>();

		for (CoriolisCarriedItem free : freeItems) {
			cmodel.removeItem(free);
		}
		freeItems.clear();
		logger.debug("* After removing free items, character has: "+parent.getCharacter().getItems());
		cmodel.setBirr(0);
		try {
			for (Modification tmp : previous) {
				if (tmp instanceof ItemModification) {
					ItemModification mod = (ItemModification)tmp;
					CoriolisItemTemplate item = (CoriolisItemTemplate)mod.getItem();
					logger.info("ItemModification = "+mod);
					logger.info("ItemModification.getOptions() = "+mod.getOptions());
					Selection[] options = CoriolisGearController.parseAsSelectionString(mod.getOptions());
					CoriolisCarriedItem cItem = (options.length==0)?
							new CoriolisCarriedItem(item):
							new CoriolisCarriedItem(item, options)
					;
					if (mod.getI18nKey()!=null) {
//						logger.debug("i18n: Source = "+tmp.getSource());
						if (tmp.getSource()!=null && tmp.getSource() instanceof BasePluginData) {
							try {
								String translated = ((BasePluginData)tmp.getSource()).getResourceBundle().getString(mod.getI18nKey());
								logger.debug("Set item name to translated "+translated);
								cItem.setName(translated);
							} catch (MissingResourceException mre) {
								logger.error("Missing "+mre.getKey()+" in "+((BasePluginData)tmp.getSource()).getResourceBundle().getBaseBundleName());
							}
						} else
							logger.error("ItemModification with i18nKey, but no modification source found");
					} else if (mod.getName()!=null) {
						logger.debug("Set item name to fixed "+mod.getName());
						cItem.setName(mod.getName());
					}
					logger.debug("  Add item by modification: "+cItem);
					cmodel.addItem(cItem);
					freeItems.add(cItem);
				} else if (tmp instanceof CurrencyModification) {
					logger.debug("  Add "+((CurrencyModification)tmp).getValue()+" from "+tmp.getSource());
					cmodel.setBirr(cmodel.getBirr() +
							((CurrencyModification)tmp).getValue());
				} else {
					logger.trace("  keep processing "+tmp);
					unprocessed.add(tmp);
				}
			}

			/*
			 * Pay remaining items
			 */
			for (CoriolisCarriedItem carried : parent.getCharacter().getItems()) {
				if (!freeItems.contains(carried)) {
					logger.debug("* pay "+carried.getCost()+" for "+carried);
					cmodel.setBirr( cmodel.getBirr() - carried.getCost() );
				}
			}
		} finally {
			logger.trace("STOP : process");
		}
		// Calculate encumbrance
		return super.process(model, unprocessed);
	}

}
