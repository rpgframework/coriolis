/**
 * 
 */
package org.prelle.coriolis.chargen;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Stefan Prelle
 *
 */
public interface CoriolisCharGenConstants {

	public final static Logger LOGGGER = LogManager.getLogger("coriolis.chargen");
	public final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/coriolis/chargen/i18n/chargen");

}
