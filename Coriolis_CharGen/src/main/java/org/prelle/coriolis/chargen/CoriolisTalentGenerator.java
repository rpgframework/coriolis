/**
 *
 */
package org.prelle.coriolis.chargen;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.Talent.Type;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.charctrl.TalentController;
import org.prelle.yearzeroengine.chargen.CommonCharacterGenerator;
import org.prelle.yearzeroengine.chargen.TalentGenerator;

/**
 * @author Stefan
 *
 */
public class CoriolisTalentGenerator extends TalentGenerator<CoriolisCharacter> implements
		TalentController {


	//--------------------------------------------------------------------
	public CoriolisTalentGenerator(
			CommonCharacterGenerator<CoriolisCharacter> parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeIncreased(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean canBeSelected(Talent key) {
		CoriolisCharacter model = parent.getCharacter();
		// Only mystic talents are free selectable
		if (key.getType()!=Type.MYSTICAL)
			return false;
		// If the skill level is at least 1
		int mystLevel = model.getSkillValue(parent.getRuleset().getSkill("mystic_powers")).getPoints();
		if (mystLevel<=0)
			return false;
		// And only if the subconcept is "fugitive/mystic"
		if (model.getSubconcept()==null || !model.getSubconcept().getId().equals("mystic"))
			return false;
		// and there are free points left
		return free>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.TalentController#canBeDecreased(org.prelle.yearzeroengine.Talent)
	 */
	@Override
	public boolean canBeDeselected(TalentValue val) {
		return val.getTalent().getType()==Type.MYSTICAL;
	}

}
