/**
 *
 */
package org.prelle.coriolis.chargen;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.SkillValue;
import org.prelle.yearzeroengine.chargen.AttributeGenerator;
import org.prelle.yearzeroengine.chargen.CommonCharacterGenerator;
import org.prelle.yearzeroengine.chargen.SkillGenerator;
import org.prelle.yearzeroengine.chargen.TalentGenerator;
import org.prelle.yearzeroengine.charproc.ApplyTalentModifications;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public class CoriolisCharacterGenerator extends CommonCharacterGenerator<CoriolisCharacter> implements CoriolisCharacterController {

	private final static Logger logger = CoriolisCharGenConstants.LOGGGER;
	private final static PropertyResourceBundle i18NResources = CoriolisCharGenConstants.RES;

	private BackgroundGenerator background;
	private IconGenerator icons;

	//-------------------------------------------------------------------
	public CoriolisCharacterGenerator() {
		super(CoriolisCore.getRuleset(), i18NResources);
		background = new BackgroundGenerator(this);
		attributes = new CoriolisAttributeGenerator(this);
		charclasses= new CoriolisCharacterClassGenerator(this);
		group      = new CoriolisGroupGenerator(this);
		icons      = new IconGenerator(this);
		skills     = new CoriolisSkillGenerator(this);
		talents    = new CoriolisTalentGenerator(this);
		gear       = new CoriolisGearGenerator(this);

		processChain.add( (CharacterProcessor) group );
		// 6. Choose your background
		processChain.add( new BackgroundGenerator(this));
		// 7. Choose a concept
		processChain.add( (CharacterProcessor) charclasses );
		// 11. Distribute your attribute points
		processChain.add( (CharacterProcessor) attributes);
		// 13. Distribute your skill levels
		processChain.add( (CharacterProcessor) skills);
		// 15. Randomly determine your icon and Icon talent
		processChain.add( (CharacterProcessor) icons);
		processChain.add( (CharacterProcessor) talents);
		processChain.add( (CharacterProcessor) gear);
		processChain.add( (CharacterProcessor) fluff);
		processChain.add( ((AttributeGenerator<?>)attributes).getCostCalculator() );
		processChain.add( ((SkillGenerator<?>)skills).getCostCalculator() );
		processChain.add( ((TalentGenerator<?>)talents).getCostCalculator() );

		processChain.add(new ApplyTalentModifications());

		start();
	}

	//-------------------------------------------------------------------
	public PropertyResourceBundle geti18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	/**
	 */
	@Override
	public void start() {
		model = new CoriolisCharacter();
		model.setRuleset(CoriolisCore.getRuleset());
		model.initAttributes(AttributeCoriolis.values());
		// Set attribute starting values
		for (Attribute key : AttributeCoriolis.primaryValues())
			model.getAttribute(key).setPoints(2);
		AttributeValue val = model.getAttribute(AttributeCoriolis.REPUTATION);
		val.setPoints( val.getModifier() );

		runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.CommonCharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		logger.info("Stop character creation");

		// Write reputation as fixed points
		AttributeValue val = model.getAttribute(AttributeCoriolis.REPUTATION);
		val.setPoints( val.getModifier() );
		val.setStart(val.getPoints());
		val.clearModifications();

//		// Set start values for primary attributes
//		for (Attribute key : AttributeCoriolis.primaryValues()) {
//			val = model.getAttribute(key);
//			val.setStart(val.getPoints());
//			val.clearModifications();
//		}

		// Set start values for skills
		for (SkillValue key : model.getSkills()) {
			key.setStart(key.getPoints());
		}
	}

	//-------------------------------------------------------------------
	public BackgroundGenerator getUpbringingController() {
		return background;
	}

	//-------------------------------------------------------------------
	public IconGenerator getIconController() {
		return icons;
	}

}
