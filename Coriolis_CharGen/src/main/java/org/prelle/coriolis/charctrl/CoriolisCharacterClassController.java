/**
 *
 */
package org.prelle.coriolis.charctrl;

import java.util.List;

import org.prelle.coriolis.SubConcept;
import org.prelle.yearzeroengine.CharacterClass;
import org.prelle.yearzeroengine.charctrl.CharacterClassController;

/**
 * @author prelle
 *
 */
public interface CoriolisCharacterClassController extends CharacterClassController {

	//-------------------------------------------------------------------
	/**
	 * Allow selection of character concepts that are not recommended
	 * by the group concept
	 */
	public void setAllowUnusualConcepts(boolean permissive);

	//-------------------------------------------------------------------
	public List<SubConcept> getAvailableSubConcepts();

	//-------------------------------------------------------------------
	public boolean isRecommended(CharacterClass value);

	//-------------------------------------------------------------------
	public boolean isRecommended(SubConcept value);

	//-------------------------------------------------------------------
	public SubConcept getSelectedSubConcept();

	//-------------------------------------------------------------------
	public void setSelectedSubConcept(SubConcept value);

}
