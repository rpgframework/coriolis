/**
 *
 */
package org.prelle.coriolis.charctrl;

import org.prelle.coriolis.GroupConcept;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.charctrl.GroupController;

/**
 * @author prelle
 *
 */
public interface CoriolisGroupController extends GroupController {

	//-------------------------------------------------------------------
	public void setGroupConcept(GroupConcept value);

	//-------------------------------------------------------------------
	public GroupConcept getGroupConcept();

	//-------------------------------------------------------------------
	public void setGroupTalent(Talent n);

	//-------------------------------------------------------------------
	public Talent getGroupTalent();

}
