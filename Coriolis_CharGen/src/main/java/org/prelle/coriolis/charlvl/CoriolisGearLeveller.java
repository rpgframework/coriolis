/**
 *
 */
package org.prelle.coriolis.charlvl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.Selection;
import org.prelle.coriolis.charctrl.CoriolisGearController;
import org.prelle.coriolis.chargen.CoriolisCharGenConstants;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.ItemTemplate;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;
import org.prelle.yearzeroengine.charproc.CharacterProcessor;
import org.prelle.yearzeroengine.modifications.FreePointsModification;
import org.prelle.yearzeroengine.modifications.FreePointsModification.Type;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CoriolisGearLeveller implements CoriolisGearController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("coriolis.charlvl");
	private final static PropertyResourceBundle RES = CoriolisCharGenConstants.RES;

	protected CharacterController<CoriolisCharacter> parent;

	private float encumbrancePointsLeft;
	private List<ToDoElement> toDos;

	//--------------------------------------------------------------------
	/**
	 */
	public CoriolisGearLeveller(CharacterController<CoriolisCharacter> parent) {
		this.parent = parent;
		toDos = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#canBeSelected(org.prelle.yearzeroengine.ItemTemplate)
	 */
	@Override
	public <T extends ItemTemplate> boolean canBeSelected(T item) {
		int minCost = item.getCost().getMin();
		return parent.getCharacter().getBirr()>=minCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#canBeDeselected(org.prelle.yearzeroengine.CarriedItem)
	 */
	@Override
	public <T extends ItemTemplate> boolean canBeDeselected(CarriedItem<T> item) {
		return true;
	}

	//--------------------------------------------------------------------
	private void addAndPay(CoriolisCarriedItem item) {
		parent.getCharacter().addItem(item);
		logger.info("Equipped "+item);

		/* Pay */
		logger.info("Pay "+item.getCost()+" birr");
		parent.getCharacter().setBirr(parent.getCharacter().getBirr()-item.getCost());

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, parent.getCharacter()));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#select(org.prelle.yearzeroengine.ItemTemplate)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends ItemTemplate> CarriedItem<T> select(T value, ItemLocation loc) {
		if (!canBeSelected(value)) {
			logger.warn("Trying to select "+value+" which is not selectable");
			return null;
		}
		logger.info("select "+value+" for location "+loc);

		CoriolisCarriedItem item = new CoriolisCarriedItem((CoriolisItemTemplate) value);
		item.setLocation(loc);
		addAndPay(item);
		return (CarriedItem<T>) item;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.charctrl.CoriolisGearController#select(org.prelle.coriolis.CoriolisItemTemplate, org.prelle.coriolis.Selection[])
	 */
	@Override
	public CoriolisCarriedItem select(CoriolisItemTemplate value, ItemLocation loc, Selection... choices) {
		if (!canBeSelected(value)) {
			logger.warn("Trying to select an item which cannot be selected: "+value);
		}
		logger.info("select "+value+" for location "+loc+" and choices: "+Arrays.asList(choices));
		
		/* Create item */
		CoriolisCarriedItem item = new CoriolisCarriedItem((CoriolisItemTemplate) value, choices);
		item.setLocation(loc);
		addAndPay(item);

		parent.runProcessors();
		return item;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#deselect(org.prelle.yearzeroengine.CarriedItem)
	 */
	@Override
	public <T extends ItemTemplate> void deselect(CarriedItem<T> value) {
		if (!canBeDeselected(value)) {
			logger.warn("Trying to deselect an item which cannot be deselected: "+value);
			return;
		}

		logger.info("Deselect/Remove "+value);
		parent.getCharacter().removeItem((CoriolisCarriedItem) value);

		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	protected float getEncumbrance(CoriolisCarriedItem item) {
		switch (item.getWeight()) {
		case TINY  : return 0;
		case LIGHT : return 0.5f;
		case NORMAL: return 1;
		case HEAVY : return 2;
		}
		return 0f;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charctrl.GearController#getEncumbrancePointsLeft()
	 */
	@Override
	public float getEncumbrancePointsLeft() {
		//		/*
		//		 * Pool is 2 x STRENGTH
		//		 * TINY=0, LIGHT=0.5, NORMAL=1, HEAVY=2
		//		 */
		//		float pool = parent.getCharacter().getAttribute(AttributeCoriolis.STRENGTH).getModifiedValue()*2;
		//		for (CoriolisCarriedItem item : parent.getCharacter().getItems()) {
		//			pool -= getEncumbrance(item);
		//		}
		//		return pool;
		return encumbrancePointsLeft;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#process(org.prelle.yearzeroengine.YearZeroEngineCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(YearZeroEngineCharacter model,
			List<Modification> previous) {
		logger.trace("START: process");

		toDos.clear();
		encumbrancePointsLeft = 0f;
		List<Modification> unprocessed = new ArrayList<>();

		try {
			// Filter modifications to process
			for (Modification mod : previous) {
				if (mod instanceof FreePointsModification && ((FreePointsModification)mod).getType()==Type.ENCUMBRANCE) {
					encumbrancePointsLeft += ((FreePointsModification)mod).getCount();
					logger.debug("  Set encumbrance points to "+encumbrancePointsLeft);
				} else
					unprocessed.add(mod);
			}

			/*
			 * Calculate encumbrance of items
			 */
			for (CoriolisCarriedItem item : parent.getCharacter().getItems()) {
				if (item.getLocation()==ItemLocation.BODY) {
					encumbrancePointsLeft -= getEncumbrance(item);
					logger.debug("  "+item+" weighs "+getEncumbrance(item));
				}
			}
			logger.info("Calculated encumbrance left: "+encumbrancePointsLeft);
			// Warn if necessary
			if (encumbrancePointsLeft<0) {
				toDos.add(new ToDoElement(Severity.WARNING, RES.getString("todo.encumbrance")));
			}
		} finally {
			logger.trace("STOP : process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.charproc.CharacterProcessor#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return toDos;
	}

}
