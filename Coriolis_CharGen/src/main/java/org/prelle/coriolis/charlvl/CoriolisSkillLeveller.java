/**
 *
 */
package org.prelle.coriolis.charlvl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisTools;
import org.prelle.yearzeroengine.SkillValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charlvl.SkillLeveller;
import org.prelle.yearzeroengine.modifications.KeySkillModification;
import org.prelle.yearzeroengine.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CoriolisSkillLeveller extends SkillLeveller<CoriolisCharacter> {

	private final static Logger logger = LogManager.getLogger("coriolis.charlvl");

	//--------------------------------------------------------------------
	public CoriolisSkillLeveller(CoriolisCharacterLeveller parent) {
		super(parent);
		
		logger.warn("mods = "+parent.getCharacter().getSubconcept().getModifications());
		for (Modification tmp : parent.getCharacter().getSubconcept().getModifications()) {
			if (tmp instanceof KeySkillModification) {
				KeySkillModification mod = (KeySkillModification)tmp;
				conceptSkills.add(mod.getSkill());
				System.err.println("<init> Add "+mod.getSkill()+" as concept skill");
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean increase(SkillValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+val.getModifyable()+" to "+val.getPoints());
		// Add to history
		parent.getCharacter().addToHistory(new SkillModification(val.getModifyable(), val.getPoints(), 5));

		// Pay Exp
		parent.getCharacter().setExpFree(parent.getCharacter().getExpFree() -5);
		parent.getCharacter().setExpInvested(parent.getCharacter().getExpInvested() +5);

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean decrease(SkillValue val) {
		if (!canBeDecreased(val)) {
			logger.warn("Trying to increase attribute "+val+" which cannot be increased");
			return false;
		}

		// Remove to history
		CoriolisTools.removeFromHistory(parent.getCharacter(), new SkillModification(val.getModifyable(), val.getPoints(), 5));
		// Apply
		val.setPoints(val.getPoints()-1);
		logger.info("Decrease "+val.getModifyable()+" to "+val.getPoints());

		// Grant Exp
		parent.getCharacter().setExpFree(parent.getCharacter().getExpFree() +5);
		parent.getCharacter().setExpInvested(parent.getCharacter().getExpInvested() -5);

		parent.runProcessors();
		return true;
	}

}
