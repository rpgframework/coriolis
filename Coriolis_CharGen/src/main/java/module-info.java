/**
 * @author Stefan Prelle
 *
 */
module coriolis.chargen {
	exports org.prelle.coriolis.chargen;
	exports org.prelle.coriolis.charlvl;
	exports org.prelle.coriolis.charctrl;

	requires transitive coriolis.core;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires simple.persist;
	requires transitive yearzeroengine.chargen;
	requires transitive yearzeroengine.core;
}