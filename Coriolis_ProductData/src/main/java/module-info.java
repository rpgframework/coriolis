module rpgframework.products.coriolis {
	exports de.rpgframework.products.coriolis;
	opens de.rpgframework.products.coriolis;

	provides de.rpgframework.products.ProductDataPlugin with de.rpgframework.products.coriolis.ProductDataCoriolis;

	requires de.rpgframework.core;
	requires de.rpgframework.products;
	requires org.apache.logging.log4j;
	
}