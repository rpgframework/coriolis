alert.cancel_creation.title=Cancel character creation?
alert.cancel_creation.message=Going back to mainscreen will cancel character creation.\nAny progress is lost.!\n\nReally cancel?

alert.save_character.title=Save changes
alert.save_character.message=Do you want to save the character?

alert.reloading.char=Error while reloading character
appearance.filechooser.title=Select a (square) character image
appearance.btn.delete_image=Remove\nimage

button.delete.tooltip=Delete selection
button.add.tooltip=Add new item
button.random=Random

command.primary.delete=Delete
command.primary.print=Print

dialog.reward.title=Add a reward

error.saving_character.title=Error saving the character
error.saving_character.message=The following internal error occurred while trying to save the character:

itemselector.name.prompt=Type here to narrow options
talentselector.name.prompt=Type here to narrow options

label.adventure=Adventure
label.birr=Birr
label.birr.short=Birr
label.character=Character
label.clothing=Clothing
label.concept=Concept
label.ep.free=Free
label.exp=Experience
label.experience.short=EP
label.expTotal=Experience total
label.expUsed=Used
label.face=Face
label.gender=Gender
label.group_concept=Group Concept
label.group_concept.explain=Your group can consist of all kinds of individuals \u2013 the group concept only outlines your most basic reason for sticking together in the Third Horizon.
label.group_talent=Group Talent
label.group_talent.explain=When creating your group, pick one of the talents connected to your group concept. Everyone in the group can use this talent.
label.humantype=Human/ite
label.homesystem=Home system
label.icon=Icon
label.load.short=Loadp.
label.name=Name
label.or=or
label.origin=Origin
label.otherchars.explain=If you already know them, enter the names of your fellow characters or at least their players. You can change this later.
label.pc=PC
label.personal_problem=Personal Problem
label.points=Points
label.points.short=Pt.
label.portrait=Portrait
label.title=Title
label.upbringing=Upbringing
label.when=When

navItem.develop=Experience
navItem.fluff=Appearance
navItem.gear=Gear
navItem.overview=Overview
navItem.talents=Talents

relation.head.to=Character
relation.head.kind=Kind of relation
relation.head.buddy=Buddy

section.basedata=Base data
section.attributes=Attributes
section.health=Health
section.skills.general=General skills
section.skills.advanced=Advanced skills
skilltable.col.name=Name
skilltable.col.attribute=Attribute
skilltable.col.value=Value

wizard.appearance.title=Name & Appearance
wizard.appearance.intro=Now the trickiest part: Name and describe yourself.
wizard.appearance.button.load=Load image
wizard.appearance.button.clear=Clear image

wizard.attributes.title=Attributes
wizard.attributes.intro=Your character is defined by two physical and two mental attributes. One of these is your key attribute and is defined by your character concept and you can raise this higher than the others.

wizard.background.title=Background
wizard.background.intro=The first thing you must do is to decide on your PC\u2019s background. Where are you from? How did you grow up? Are you a normal human or a humanite? The answers will be the foundation on which to build your PC.
wizard.background.upbringing=Upbringing
wizard.background.upbringing.explain=Are you from a remote colony in the jungle that now covers a ruined metropolis from the first wave of colonization? Or did you grow up among the traveling nomads, going from station to station, living off odd jobs or selling handicrafts? Perhaps you were born into one of the old Zenithian bloodlines, brought up according to the old traditions?
wizard.background.origin=Origin
wizard.background.origin.explain=Origin means both which star system you come from, and whether you\u2019re of the Firstcome or a Zenithian.
wizard.background.colonization_wave=Colonization Wave
wizard.background.home_system=Home system
wizard.background.home_system.random=Random
wizard.background.humantype.head=Human or Humanite
wizard.background.humantype.explain=Not  everyone in the Horizon is a normal human, or a \u201Cpureblood\u201D as the older Hegemonists would say. Some groups of people are biosculpted. These \u201Chumanites\u201D, as they are called, are often found in primitive tribes on distant planets or abandoned space stations. Common folk view the humanites as sub-human and dirty, but they are usually superbly fitted to life in the environment they were designed for.
wizard.background.humantype=Human(ite)

wizard.cancelconfirm.header=Really cancel?
wizard.cancelconfirm.content=Do you really want to cancel the character creation and loose your progress?

wizard.concept.title=Character Concept & Attributes
wizard.concept.intro=The concept tells you what you do for a living. Each concept contains several sub-concepts to give your PC more detail. Your concept affects your attributes, which skills and talents you can choose from at the start of the game, your gear, your relationships and your personal problem
wizard.concept.concept=Concept
wizard.concept.subconcept=Subconcept
wizard.concept.decisions=Pick one:

wizard.gear.title=Starting gear
wizard.gear.templates.avprompt=You cannot select any more items
wizard.gear.templates.selprompt=Drag additional gear here
wizard.gear.concept.heading=Concept gear
wizard.gear.concept.intro=The following gear choices are available from your concept.
wizard.gear.extra.heading=Additional gear
wizard.gear.extra.intro=You can buy additional gear, as long as you have sufficient encumbrance points and Birr

wizard.groupconcept1.title=The Group
wizard.groupconcept1.otherchars.heading=Who is in your group?
wizard.groupconcept1.otherchars.prompt=Character- or player name
wizard.groupconcept1.intro=Before creating your PCs, you should decide together what kind of group your PCs make up.
wizard.groupconcept2.title=The Group (Talent)
wizard.groupconcept2.intro=When creating your group, pick one of the talents connected to your group concept. Everyone in the group can use this talent.

wizard.iconAndTalent.title=Icon & Talents
wizard.iconAndTalent.icon.heading=Icon
wizard.iconAndTalent.icon.intro=You are born under the sign of one of the Icons. This Icon will have a tangible impact on your life, and might just lend you supernatural powers. You won\u2019t get to choose your Icon \u2013 this is up to fate
wizard.iconAndTalent.talents.heading=Talents
wizard.iconAndTalent.talents.intro=Your previous choices granted you the following talents - you cannot change them.\nEventually you still get to choose a mystic talent.
wizard.iconAndTalent.talents.avprompt=There are no additional talents available.

wizard.relAndProb.title=Personal problem and relationships
wizard.relAndProb.problem.heading=Personal Problem
wizard.relAndProb.problem.intro=Roll a random personal problem or write your own.\n\
 Your personal problem is mainly a tool for the GM to create stories and events that feel personal to you, but it also affects how many Experience Points you receive after a gaming session (page 28).
wizard.relAndProb.relation.heading=Relationships to other PCs
wizard.relAndProb.relation.intro=When creating your PC, describe your relationship to each of the other PCs with a short sentence on your character sheet.\n\
 When you\u2019ve chosen your relationships to the other PCs, you should pick one of them as your buddy \u2013 your best friend in the group.

wizard.skills.title=Skills
wizard.skills.intro=Distribute your skill points.\nMeaning: 1 = Novice,     2 = Capable,     3 = Competent\n\
 During generation, only your concept skills can be raised above 1.
