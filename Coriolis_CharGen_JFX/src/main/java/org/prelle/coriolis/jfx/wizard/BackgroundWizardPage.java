/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.HomeSystem;
import org.prelle.coriolis.HumanType;
import org.prelle.coriolis.Origin;
import org.prelle.coriolis.Upbringing;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;




/**
 * @author Stefan
 *
 */
public class BackgroundWizardPage extends WizardPage {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private ChoiceBox<Origin> cbColWave;
	private ChoiceBox<HomeSystem> cbHomeSystem;
	private Button btnHomeSystem;
	private ChoiceBox<Upbringing> cbUpbringing;
	private ChoiceBox<HumanType> cbHumanType;
	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public BackgroundWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
		cbHumanType.getSelectionModel().select(0);

		setTitle(RES.getString("wizard.background.title"));
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbColWave = new ChoiceBox<>();
		cbColWave.getItems().addAll(Origin.values());
		cbColWave.setConverter(new StringConverter<Origin>() {
			public String toString(Origin value) { return value!=null?value.getName():"";}
			public Origin fromString(String arg0) { return null; }
		});

		cbHomeSystem = new ChoiceBox<>();
		cbHomeSystem.getItems().addAll(ctrl.getRuleset().getListByType(HomeSystem.class));
		cbHomeSystem.setConverter(new StringConverter<HomeSystem>() {
			public String toString(HomeSystem value) { return value!=null?value.getName():"";}
			public HomeSystem fromString(String arg0) { return null; }
		});
		btnHomeSystem = new Button(RES.getString("wizard.background.home_system.random"));

		cbUpbringing = new ChoiceBox<>();
		cbUpbringing.getItems().addAll(ctrl.getRuleset().getListByType(Upbringing.class));
		cbUpbringing.setConverter(new StringConverter<Upbringing>() {
			public String toString(Upbringing value) { return value!=null?value.getName():"";}
			public Upbringing fromString(String arg0) { return null; }
		});

		cbHumanType = new ChoiceBox<>();
		cbHumanType.getItems().addAll(ctrl.getRuleset().getListByType(HumanType.class));
		cbHumanType.setConverter(new StringConverter<HumanType>() {
			public String toString(HumanType value) { return value!=null?value.getName():"";}
			public HumanType fromString(String arg0) { return null; }
		});

		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbIntro = new Label(RES.getString("wizard.background.intro"));
		lbIntro.setWrapText(true);

		/*
		 * Origin
		 */
		Label hdOrigin = new Label(RES.getString("wizard.background.origin"));
		hdOrigin.getStyleClass().add("subtitle");
		Label lbOriginEx = new Label(RES.getString("wizard.background.origin.explain"));
		lbOriginEx.setWrapText(true);
		Label lbColWave = new Label(RES.getString("wizard.background.colonization_wave"));
		lbColWave.getStyleClass().add("base");
		Label lbHomeSystem = new Label(RES.getString("wizard.background.home_system"));
		lbHomeSystem.getStyleClass().add("base");

		GridPane bxOrigin = new GridPane();
		bxOrigin.add(lbColWave    , 0, 0);
		bxOrigin.add(cbColWave    , 1, 0);
		bxOrigin.add(lbHomeSystem , 0, 1);
		bxOrigin.add(cbHomeSystem , 1, 1);
		bxOrigin.add(btnHomeSystem, 2, 1);
		bxOrigin.setStyle("-fx-vgap: 1em; -fx-hgap: 0.5em");
		bxOrigin.setAlignment(Pos.CENTER_LEFT);

		/*
		 * Upbringing
		 */
		Label hdUpbringing = new Label(RES.getString("wizard.background.upbringing"));
		hdUpbringing.getStyleClass().add("subtitle");
		Label lbUpbringingEx = new Label(RES.getString("wizard.background.upbringing.explain"));
		lbUpbringingEx.setWrapText(true);
		Label lbUpbringing = new Label(RES.getString("wizard.background.upbringing"));
		lbUpbringing.getStyleClass().add("base");

		HBox bxUpbringing = new HBox(lbUpbringing, cbUpbringing);
		bxUpbringing.setStyle("-fx-spacing: 1em");
		bxUpbringing.setAlignment(Pos.CENTER_LEFT);


		/*
		 * Human type
		 */
		Label hdHumanType = new Label(RES.getString("wizard.background.humantype.head"));
		hdHumanType.getStyleClass().add("subtitle");
		Label lbHumanTypeEx = new Label(RES.getString("wizard.background.humantype.explain"));
		lbHumanTypeEx.setWrapText(true);
		Label lbHumanType = new Label(RES.getString("wizard.background.humantype"));
		lbHumanType.getStyleClass().add("base");

		HBox bxHumanType = new HBox(lbHumanType, cbHumanType);
		bxHumanType.setStyle("-fx-spacing: 1em");
		bxHumanType.setAlignment(Pos.CENTER_LEFT);


		VBox content = new VBox(5);
		content.getChildren().addAll(lbIntro);
		content.getChildren().addAll(hdOrigin, lbOriginEx, bxOrigin);
		content.getChildren().addAll(hdUpbringing, lbUpbringingEx, bxUpbringing);
		content.getChildren().addAll(hdHumanType, lbHumanTypeEx, bxHumanType);
		VBox.setMargin(hdOrigin, new Insets(20, 0, 10, 0));
		VBox.setMargin(hdUpbringing, new Insets(20, 0, 10, 0));
		VBox.setMargin(hdHumanType, new Insets(20, 0, 10, 0));


		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
//		allContent.setStyle("-fx-spacing: 2em");
//		content.setStyle("-fx-pref-width: 60em; -fx-min-width:35em");
//		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		content.setStyle("-fx-pref-width: 51em; -fx-min-width:24em");
		explain.setStyle("-fx-pref-width: 15em; -fx-min-width:15em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbColWave.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getUpbringingController().setOrigin(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(null);
				lbSelectDesc.setText(n.getHelpText());
			}
		});

		cbHomeSystem.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getUpbringingController().setHomeSystem(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
		});

		cbUpbringing.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getUpbringingController().setUpbringing(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
			// Update list of allowed human types
			List<HumanType> toSet = ctrl.getUpbringingController().getAvailableHumanTypes();
			cbHumanType.getItems().retainAll(toSet);
			for (HumanType tmp : toSet) {
				if (!cbHumanType.getItems().contains(tmp))
					cbHumanType.getItems().add(tmp);
			}
			// If previously selected human type is not available anymore, select first
			if (((CoriolisCharacter)ctrl.getCharacter()).getHumanType()!=null &&
					!cbHumanType.getItems().contains( ((CoriolisCharacter)ctrl.getCharacter()).getHumanType())) {
				cbHumanType.getSelectionModel().select(0);
			} else if (((CoriolisCharacter)ctrl.getCharacter()).getHumanType()==null) {
				cbHumanType.getSelectionModel().select(0);
			}
		});

		cbHumanType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getUpbringingController().setHumanType(n);
			if (n!=null) {
				lbSelectName.setText(n.getName());
				lbSelectRef.setText(n.getProductName()+" "+n.getPage());
				lbSelectDesc.setText(n.getHelpText());
			}
		});

		btnHomeSystem.setOnAction(ev -> {
			cbHomeSystem.getSelectionModel().select(ctrl.getUpbringingController().getRandomHomeSystem());
		});
	}

}
