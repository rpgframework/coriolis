/**
 * 
 */
package org.prelle.coriolis.jfx.dialogs;

import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.yearzeroengine.ItemLocation;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelector extends Control implements IListSelector<CoriolisItemTemplate>{
	
	private ItemLocation allowedTypes;
	private ObjectProperty<CoriolisItemTemplate> selected;

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemLocation allowed, CoriolisCharacterController control) {
		allowedTypes = allowed;
		selected = new SimpleObjectProperty<CoriolisItemTemplate>();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemLocation getAllowedTypes() {
		return allowedTypes;
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<CoriolisItemTemplate> selectedItemProperty() {
		return selected;
	}

	//-------------------------------------------------------------------
	public CoriolisItemTemplate getSelectedItem() {
		return selected.get();
	}

	//-------------------------------------------------------------------
	void setSelectedItem(CoriolisItemTemplate data) {
		selected.set(data);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<CoriolisItemTemplate> getSelectionModel() {
		return ((ItemTemplateSelectorSkin)getSkin()).getSelectionModel();
	}
	
}
