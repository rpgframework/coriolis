/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;

/**
 * @author Stefan
 *
 */
public class CoriolisWizard extends Wizard implements GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private CoriolisCharacterGenerator charGen;

	//--------------------------------------------------------------------
	public CoriolisWizard(CoriolisCharacterGenerator ctrl) {
//		setSkin(new CoriolisDialogSkin(this));
		this.charGen = ctrl;
		getPages().addAll(
				new GroupConceptWizardPage(this, ctrl),
//				new GroupTalentWizardPage(this, ctrl),
				new BackgroundWizardPage(this, ctrl),
				new ConceptWizardPage(this, ctrl),
//				new AttributeWizardPage(this, ctrl),
				new SkillsWizardPage(this, ctrl),
//				new IconAndTalentWizardPage(this, ctrl),
				new ProblemAndRelationshipWizardPage(this, ctrl),
				new GearWizardPage(this, ctrl),
				new AppearanceWizardPage(this, ctrl)
				);

		this.setConfirmCancelCallback( wizardParam -> {
			CloseType answer = getScreenManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					CoriolisCharGenJFXConstants.RES.getString("wizard.cancelconfirm.header"),
					CoriolisCharGenJFXConstants.RES.getString("wizard.cancelconfirm.content"));
			return answer==CloseType.OK || answer==CloseType.YES;
		});

		GenerationEventDispatcher.addListener(this);

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Wizard#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		for (ToDoElement todo : charGen.getToDos()) {
			if (todo.getSeverity()==Severity.STOPPER) {
				return false;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		refresh();
	}

}
