/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import java.util.PropertyResourceBundle;

import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.CharacterRelation;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class RelationshipsSection extends SingleSection {

	private final static String ICON_ADD = "\uE710";
	private final static String ICON_REMOVE = "\uE711";

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	private CharacterController<? extends YearZeroEngineCharacter> ctrl;

	private Label hdPC, hdKind, hdBuddy;
	private Button[] btPlusMinus;
	private TextField[] lbPC;
	private TextField[] tfText;
	private RadioButton[] cbBuddy;
	private ToggleGroup buddyGroup;
	private GridPane grid;

	//-------------------------------------------------------------------
	public RelationshipsSection( String title, CharacterController<? extends YearZeroEngineCharacter> ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		this.ctrl  = ctrl;

		initComponents();
		initLayout();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		buddyGroup = new ToggleGroup();
		grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em;");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		hdPC = new Label(UI.getString("relation.head.to")); hdPC.getStyleClass().add("base");
		hdKind = new Label(UI.getString("relation.head.kind")); hdKind.getStyleClass().add("base");
		hdBuddy = new Label(UI.getString("relation.head.buddy")); hdBuddy.getStyleClass().add("base");
		grid.add(hdPC   , 1, 0);
		grid.add(hdKind , 2, 0);
		grid.add(hdBuddy, 3, 0);
		
		setContent(grid);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		int COUNT = ctrl.getCharacter().getRelations().size()+1;
		if (btPlusMinus==null || COUNT!=btPlusMinus.length) {
			grid.getChildren().clear();
			grid.add(hdPC   , 1, 0);
			grid.add(hdKind , 2, 0);
			grid.add(hdBuddy, 3, 0);
			btPlusMinus = new Button[COUNT];
			lbPC   = new TextField[COUNT];
			tfText = new TextField[COUNT];
			cbBuddy = new RadioButton[COUNT];
			for (int i=0; i<COUNT; i++) {
				btPlusMinus[i] = new Button();
				btPlusMinus[i].setStyle("-fx-font-family: 'Segoe MDL2 Assets'");
				lbPC[i] = new TextField();
				lbPC[i].setPromptText(UI.getString("label.pc")+" "+(i+1)+":");
				tfText[i] = new TextField();
				tfText[i].setMaxWidth(Double.MAX_VALUE);
				cbBuddy[i] = new RadioButton();
				cbBuddy[i].setToggleGroup(buddyGroup);
			}
			for (int i=0; i<COUNT; i++) {
				grid.add(btPlusMinus[i], 0, i+1);
				grid.add(lbPC[i]       , 1, i+1);
				grid.add(tfText[i]     , 2, i+1);
				grid.add(cbBuddy[i]    , 3, i+1);
				GridPane.setValignment(lbPC[i], VPos.TOP);
				GridPane.setHgrow(tfText[i], Priority.ALWAYS);
			}
		}

		// Fill current data
		int i=0;
		for (CharacterRelation rel : ctrl.getCharacter().getRelations()) {
			btPlusMinus[i].setText(ICON_REMOVE);
			lbPC[i].setText(rel.getTo());
			tfText[i].setText(rel.getKind());
			cbBuddy[i].setSelected(rel.isBuddy());
			// Enable
			lbPC[i].setDisable(false);
			tfText[i].setDisable(false);
			cbBuddy[i].setDisable(false);
			// Interactivity
			btPlusMinus[i].setOnAction(ev -> ctrl.getFluffController().removeRelation(rel));
			lbPC[i].textProperty().addListener( (ov,o,n) -> rel.setTo(n));
			tfText[i].textProperty().addListener( (ov,o,n) -> rel.setKind(n));
			cbBuddy[i].selectedProperty().addListener( (ov,o,n) -> rel.setBuddy(n));
			i++;
		}
		// Last line
		btPlusMinus[i].setText(ICON_ADD);
		lbPC[i].setDisable(true);
		tfText[i].setDisable(true);
		cbBuddy[i].setDisable(true);
		btPlusMinus[i].setOnAction(ev -> ctrl.getFluffController().addRelation(new CharacterRelation("?", "?")));
	}

}
