/**
 * 
 */
package org.prelle.coriolis.jfx.dialogs;

import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.yearzeroengine.Talent;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;

/**
 * @author prelle
 *
 */
public class TalentSelector extends Control implements IListSelector<Talent>{
	
	private Talent.Type[] allowedTypes;
	private ObjectProperty<Talent> selected;

	//-------------------------------------------------------------------
	public TalentSelector(Talent.Type[] allowed, CoriolisCharacterController control) {
		allowedTypes = allowed;
		selected = new SimpleObjectProperty<Talent>();
		
		TalentSelectorSkin skin = new TalentSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public Talent.Type[] getAllowedTypes() {
		return allowedTypes;
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<Talent> selectedItemProperty() {
		return selected;
	}

	//-------------------------------------------------------------------
	public Talent getSelectedItem() {
		return selected.get();
	}

	//-------------------------------------------------------------------
	void setSelectedItem(Talent data) {
		selected.set(data);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<Talent> getSelectionModel() {
		return ((TalentSelectorSkin)getSkin()).getSelectionModel();
	}
	
}
