/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.NumericalValueField;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class HealthSection extends SingleSection {

	private NumericalValueField<Attribute, AttributeValue> nfRadiation;
	private NumericalValueField<Attribute, AttributeValue> nfMental;

	private AttributeController attrGen;
	private YearZeroEngineCharacter model;

	//-------------------------------------------------------------------
	public HealthSection(String title, CharacterController<? extends YearZeroEngineCharacter> gen, ScreenManagerProvider provider) {
		super(provider, title, null);
		if (gen==null)
			throw new NullPointerException();
		this.attrGen = gen.getAttributeController();
		model = gen.getCharacter();

		initCompontents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		nfRadiation = new NumericalValueField<Attribute, AttributeValue>(model.getAttribute(AttributeCoriolis.RADIATION), attrGen);
		nfMental = new NumericalValueField<Attribute, AttributeValue>(model.getAttribute(AttributeCoriolis.MENTAL_DAMAGE), attrGen);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdRadiation = new Label(AttributeCoriolis.RADIATION.getName());
		hdRadiation.getStyleClass().add("base");

		Label hdMental = new Label(AttributeCoriolis.MENTAL_DAMAGE.getName());
		hdMental.getStyleClass().add("base");

		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em; -fx-padding: 0.5em");
		grid.add(hdRadiation, 0, 0);
		grid.add(nfRadiation, 1, 0);

		grid.add(hdMental, 0, 1);
		grid.add(nfMental, 1, 1);
		
		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	public void refresh() {
		nfRadiation.refresh();
		nfMental.refresh();
	}

}
