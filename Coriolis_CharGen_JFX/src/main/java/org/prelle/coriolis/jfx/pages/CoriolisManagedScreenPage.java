package org.prelle.coriolis.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.ExpLine;
import org.prelle.rpgframework.jfx.CharacterDocumentView;

import de.rpgframework.character.CharacterHandle;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class CoriolisManagedScreenPage extends CharacterDocumentView {

	protected static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);
	
	protected static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	protected CoriolisCharacterController charGen;
	protected CharacterHandle handle;

	protected MenuItem cmdPrint;
	protected MenuItem cmdDelete;
	
	protected ExpLine expLine;

	//-------------------------------------------------------------------
	public CoriolisManagedScreenPage(CoriolisCharacterController charGen, CharacterHandle handle) {
		super();
		this.charGen = charGen;
		initPrivateComponents();
	}

	//-------------------------------------------------------------------
	private void initPrivateComponents() {
		/*
		 * Exp & Co.
		 */
		setPointsNameProperty(UI.getString("label.ep.free"));
		setPointsFree(charGen.getCharacter().getExpFree());
		expLine = new ExpLine();
		expLine.setData(charGen.getCharacter());
		
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		setHandle(handle);
		
		getCommandBar().setContent(expLine);
	}

	//-------------------------------------------------------------------
	protected void setHandle(CharacterHandle value) {
		if (this.handle==value)
			return;
		if (this.handle!=null) {
			getCommandBar().getPrimaryCommands().removeAll(cmdPrint, cmdDelete);			
		}
		
		this.handle = value;
		if (handle!=null) {
			getCommandBar().getPrimaryCommands().addAll(cmdPrint, cmdDelete);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		expLine.setData(charGen.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(charGen.getCharacter().getExpFree());
	}

}
