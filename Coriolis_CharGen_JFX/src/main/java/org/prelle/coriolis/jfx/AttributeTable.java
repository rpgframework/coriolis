/**
 *
 */
package org.prelle.coriolis.jfx;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class AttributeTable extends GridPane {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private Attribute[] attributes;

	private Map<Attribute, CheckBox[]> fields;
	private Map<Attribute, Button> decButtons;
	private Map<Attribute, Button> incButtons;

	private YearZeroEngineRuleset rules;
	private AttributeController attrGen;
	private YearZeroEngineCharacter model;
	private EventHandler<MouseEvent> onMouse;

	//-------------------------------------------------------------------
	public AttributeTable(CharacterController<? extends YearZeroEngineCharacter> gen, Attribute[] toShow) {
		if (gen==null)
			throw new NullPointerException();
		this.attributes = toShow;
		this.rules   = gen.getRuleset();
		this.attrGen = gen.getAttributeController();

		initCompontents();
		initLayout();
		initInteractivity();
		setData(gen.getCharacter());
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		setVgap(10);
//		getStyleClass().add("priority-table");

		fields = new HashMap<Attribute, CheckBox[]>();
		incButtons = new HashMap<Attribute, Button>();
		decButtons = new HashMap<Attribute, Button>();
		for (Attribute key : attributes) {
			CheckBox[] boxes = new CheckBox[5];
			for (int i=0; i<boxes.length; i++) {
				boxes[i] = new CheckBox();
				boxes[i].getStyleClass().add("intensity-check-box");
			}
			fields.put(key, boxes);
			logger.info("Put "+key+" = "+fields.get(key));
			Button dec = new Button("\uE0C6");
			Button inc = new Button("\uE0C5");
			inc.getStyleClass().add("mini-button");
			dec.getStyleClass().add("mini-button");
			decButtons.put(key, dec);
			incButtons.put(key, inc);
			dec.setUserData(key);
			inc.setUserData(key);
			dec.setOnAction(ev -> attrGen.decrease(model.getAttribute(key)));
			inc.setOnAction(ev -> attrGen.increase(model.getAttribute(key)));
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Create attribute labels
		 */
		Label[] lblAttrib = new Label[rules.getAttributes().size()];
		int i=0;
		for (Attribute attrib : attributes) {
			lblAttrib[i] = new Label(attrib.getName());
			lblAttrib[i].setUserData(attrib);
			lblAttrib[i].setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			// Geht nur in Java 8
//			lblAttrib[i].setPadding(new Insets(0,10,0,0));;
			i++;
		}


		i=0;
		for (Attribute attrib : attributes) {
			GridPane.setMargin(decButtons.get(attrib), new Insets(0,10,0,0));

			add(lblAttrib[i], 0,i);
			add(decButtons.get(attrib), 1,i);
			CheckBox[] boxes = fields.get(attrib);
			for (int x=0; x<boxes.length; x++) {
				add(boxes[x], 2+x, i);
//				// Extra space for last attribute
//				if ((x+1)==boxes.length)
//					GridPane.setMargin(boxes[x], new Insets(0,0,0,20));
			}
			add(incButtons.get(attrib), 3+boxes.length,i);

			i++;
		}
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Entry<Attribute, Button> entry : decButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.decrease(model.getAttribute(entry.getKey())));
		}
		for (Entry<Attribute, Button> entry : incButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.increase(model.getAttribute(entry.getKey())));
		}

		// Mouse over
		for (Attribute attrib : attributes) {
			decButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			incButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
		}

		for (Attribute attrib : attributes) {
//			logger.debug("Search1 "+attrib+" in "+fields.keySet()+" = "+fields.containsKey(attrib));
//			logger.debug("Search2 "+attrib+" in "+fields.keySet()+" = "+fields.get(attrib));
			for (CheckBox box : fields.get(attrib)) {
//				logger.debug("  check "+attrib);
				if (box==null) {
					logger.error("No boxes for attribute "+attrib+"   = present="+fields.keySet());
					continue;
				}
				box.setOnAction(event -> {
//				box.selectedProperty().addListener((ov,o,n) -> {
					int index = Arrays.asList(fields.get(attrib)).indexOf(box);
					setAttributeTo(attrib, index+1);
				});
			}
		}

	}

	//-------------------------------------------------------------------
	private void setAttributeTo(Attribute attrib, int val) {
		int current = model.getAttribute(attrib).getPoints();
		logger.debug("Set "+attrib+" to "+val);
		// Loop until requested value reached
		while (current!=val) {
			if (current>val) {
				// Must be decreased
				if (attrGen.canBeDecreased(model.getAttribute(attrib))) {
					attrGen.decrease(model.getAttribute(attrib));
				} else {
					logger.warn("Cannot decrease from "+current+" to "+val);
					refresh();
					break;
				}
			} else {
				// Must be increased
				if (attrGen.canBeIncreased(model.getAttribute(attrib))) {
					attrGen.increase(model.getAttribute(attrib));
				} else {
					logger.warn("Cannot increase from "+current+" to "+val);
					refresh();
					break;
				}
			}
			current = model.getAttribute(attrib).getPoints();
		}
		logger.debug("Done");
	}

	//-------------------------------------------------------------------
	public void setData(YearZeroEngineCharacter model) {
		this.model = model;

//		int i=0;
//		for (Attribute attrib : rules.getAttributes()) {
//			CheckBox[] boxes = fields.get(attrib);
//			int max = attrGen.getMaximum(attrib);
//			for (int x=0; x<boxes.length; x++) {
//				if (x<max) {
//					if (!getChildren().contains(boxes[x]))
//						add(boxes[x], 2+x, i);
//				} else {
//					getChildren().remove(boxes[x]);
//				}
//			}
//			i++;
//		}

		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		for (Attribute key : attributes) {
			AttributeValue val = model.getAttribute(key);
			set(key, val.getPoints());
			decButtons.get(key).setDisable(!attrGen.canBeDecreased(val));
			incButtons.get(key).setDisable(!attrGen.canBeIncreased(val));
			CheckBox[] boxes = fields.get(key);
			for (int i=0; i<boxes.length; i++) {
				boxes[i].setVisible( i<attrGen.getMaximum(key));
			}
		}
	}

	//-------------------------------------------------------------------
	public void set(Attribute key, int val) {
		CheckBox[] boxes = fields.get(key);
		for (int i=0; i<boxes.length; i++)
			boxes[i].setSelected(i<val);
	}

	//-------------------------------------------------------------------
	public void setOnAttributeSelected(EventHandler<MouseEvent> handler) {
		onMouse = handler;
	}

}
