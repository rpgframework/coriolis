/**
 *
 */
package org.prelle.coriolis.jfx.listcells;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.TalentController;

/**
 * @author Stefan
 *
 */
public class TalentValueCell implements BasePluginDataCell<TalentValue> {

	private CharacterController<?> parent;
	private TalentController control;

	private HBox layout;
	private Label lbName, lbAdditional;
	private Button btnDeselect;

	private TalentValue key;

	//--------------------------------------------------------------------
	public TalentValueCell(CharacterController<?> ctrl) {
		this.parent = ctrl;
		this.control = ctrl.getTalentController();

		lbName = new Label(); lbName.getStyleClass().add("base");
		lbAdditional = new Label();
		btnDeselect = new Button("\uE74D"); btnDeselect.setStyle("-fx-font-size: 200%");
		btnDeselect.getStyleClass().add("icon");

		VBox box = new VBox(5, lbName, lbAdditional);
		box.setAlignment(Pos.TOP_LEFT);
		layout = new HBox(10, box, btnDeselect);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(box, Priority.ALWAYS);
		layout.setStyle("-fx-background-color: -fx-lighter; -fx-border-color: -fx-darker; -fx-border-width: 1px;");

		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnDeselect.setOnAction(ev -> control.deselect(key));
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Labeled#getGraphic()
	 */
	@Override
	public Node getGraphic() {
		return layout;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.coriolis.jfx.BasePluginDataCell#updateWithItem(java.lang.Object)
	 */
	@Override
	public void updateWithItem(TalentValue item) {
		this.key = item;

		lbName.setText(item.getTalent().getName());
		lbAdditional.setText(item.getTalent().getProductNameShort()+" "+item.getTalent().getPage());
		if (control.canBeDeselected(item)) {
			btnDeselect.setVisible(true);
			btnDeselect.setManaged(true);
		} else {
			btnDeselect.setVisible(false);
			btnDeselect.setManaged(false);
		}
	}

}
