package org.prelle.coriolis.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.ExpLine;
import org.prelle.coriolis.jfx.sections.AppearanceSection;
import org.prelle.coriolis.jfx.sections.PortraitSection;
import org.prelle.coriolis.jfx.sections.RelationshipsSection;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class CoriolisFluffPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CoriolisFluffPage.class.getName());

	private CharacterHandle handle;
	private CoriolisCharacterController control;
	private ScreenManagerProvider provider;

	private ExpLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private AppearanceSection appear;
	private PortraitSection portrait;
	private RelationshipsSection relations;
	private Section line1;

	//-------------------------------------------------------------------
	public CoriolisFluffPage(CoriolisCharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("coriolis-fluff");
		this.control = control;
		this.handle  = handle;
		this.provider = provider;

		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new ExpLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		appear   = new AppearanceSection(UI.getString("section.appearance"), control, provider);
		portrait = new PortraitSection(UI.getString("section.portrait"), control, provider);

		line1 = new DoubleSection(appear, portrait);
		getSectionList().add(line1);
	}

	//-------------------------------------------------------------------
	private void initLine2() {
		relations = new RelationshipsSection(UI.getString("section.relationships"), control, provider);
		getSectionList().add(relations);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initLine1();
		initLine2();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getGearController().getEncumbrancePointsLeft());
		firstLine.setData(control.getCharacter());
		line1.getToDoList().setAll(control.getGearController().getToDos());

		appear.refresh();
		portrait.refresh();
		relations.refresh();
	}

}
