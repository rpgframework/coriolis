/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.Icon;
import org.prelle.coriolis.charctrl.CoriolisCharacterClassController;
import org.prelle.coriolis.chargen.CoriolisCharacterClassGenerator;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.RelationshipsGUIElement;
import org.prelle.coriolis.jfx.sections.RelationshipsSection;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.yearzeroengine.charctrl.FluffController;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;



/**
 * @author Stefan
 *
 */
public class ProblemAndRelationshipWizardPage extends WizardPage implements
		GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;
	private FluffController fluff;

	private Button btIcon;
	private ChoiceBox<Icon> cbIcon;

	private TextField tfProblem;
	private Button btProblem;
	private RelationshipsGUIElement tbRelation;
	private Button btRelation;

	//--------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public ProblemAndRelationshipWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;
		this.fluff = ctrl.getFluffController();

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.relAndProb.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbIcon  = new ChoiceBox<>();
		cbIcon.getItems().addAll(ctrl.getRuleset().getListByType(Icon.class));
		cbIcon.getSelectionModel().select(ctrl.getCharacter().getIcon());
		cbIcon.setConverter(new StringConverter<Icon>() {
			public String toString(Icon value) {return value!=null?value.getName():"";}
			public Icon fromString(String arg0) {return null;}
		});
		btIcon = new Button(RES.getString("button.random"));

		tfProblem  = new TextField();
		tfProblem.setPrefColumnCount(40);
		btProblem  = new Button(RES.getString("button.random"));
		tbRelation = new RelationshipsGUIElement(ctrl);
		btRelation = new Button(RES.getString("button.random"));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Icon
		 */
		Label hdIcon = new Label(RES.getString("wizard.iconAndTalent.icon.heading"));
		hdIcon.getStyleClass().add("subtitle");
		Label lbIntroIcon = new Label(RES.getString("wizard.iconAndTalent.icon.intro"));
		lbIntroIcon.setWrapText(true);

		HBox iconLine = new HBox();
		iconLine.getChildren().addAll(btIcon, cbIcon);
		iconLine.setStyle("-fx-spacing: 1em;");

		/*
		 * Personal problem
		 */
		Label hdProblem = new Label(RES.getString("wizard.relAndProb.problem.heading"));
		hdProblem.getStyleClass().add("subtitle");
		Label lbIntroProb = new Label(RES.getString("wizard.relAndProb.problem.intro"));
		lbIntroProb.setWrapText(true);

		HBox problemLine = new HBox();
		problemLine.getChildren().addAll(btProblem, tfProblem);
		problemLine.setStyle("-fx-spacing: 1em;");

		/*
		 * Relationships
		 */
		Label hdRelation = new Label(RES.getString("wizard.relAndProb.relation.heading"));
		hdRelation.getStyleClass().add("subtitle");
		Label lbIntroRel = new Label(RES.getString("wizard.relAndProb.relation.intro"));
		lbIntroRel.setWrapText(true);


		VBox allContent = new VBox();
		allContent.setStyle("-fx-spacing: 0.5em");
		allContent.getChildren().addAll(hdIcon, lbIntroIcon, iconLine);
		allContent.getChildren().addAll(hdProblem, lbIntroProb, problemLine);
		allContent.getChildren().addAll(hdRelation, lbIntroRel, btRelation, tbRelation);
		allContent.setStyle("-fx-pref-width: 45em; -fx-min-width:25em");
		VBox.setMargin(hdProblem , new Insets(20,0,0,0));
		VBox.setMargin(hdRelation, new Insets(20,0,0,0));
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		btIcon.setOnAction(ev -> ctrl.getIconController().rollIcon());
		cbIcon.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getIconController().setIcon(n);
//			if (n!=null) {
//				updateHelp(n);
//			}
		});
		btRelation.setOnAction(ev -> {
			logger.debug("Generate random relations");
			((CoriolisCharacterClassController)ctrl.getCharacterClassController()).rollRandomRelations(ctrl.getCharacter());
		});
		btProblem.setOnAction(ev -> {
			logger.debug("Generate random problem");
			((CoriolisCharacterClassGenerator)ctrl.getCharacterClassController()).rollPersonalProblem(ctrl.getCharacter());
		});
		tfProblem.textProperty().addListener( (ov,o,n) -> ctrl.getCharacter().setProblem(n));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			cbIcon.getSelectionModel().select(ctrl.getCharacter().getIcon());
			tfProblem.setText(ctrl.getCharacter().getProblem());
			tbRelation.refresh();
			break;
		}

	}

}
