/**
 *
 */
package org.prelle.coriolis.jfx;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.yearzeroengine.charctrl.CharacterController;
import org.prelle.yearzeroengine.charctrl.GearController;

/**
 * @author Stefan
 *
 */
public class CarriedItemListCell extends ListCell<CoriolisCarriedItem> {

	private GearController control;

	private HBox layout;
	private Label lbName, lbAdditional, lbSymbol;

	//--------------------------------------------------------------------
	public CarriedItemListCell(CharacterController<?> ctrl) {
		this.control = ctrl.getGearController();

		lbName = new Label(); lbName.getStyleClass().add("base");
		lbAdditional = new Label();
		lbSymbol = new Label(); lbSymbol.setStyle("-fx-font-size: 300%");
		lbSymbol.getStyleClass().add("icon");

		VBox box = new VBox(5, lbName, lbAdditional);
		box.setAlignment(Pos.TOP_LEFT);
		layout = new HBox(10, box, lbSymbol);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(box, Priority.ALWAYS);

		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CoriolisCarriedItem item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			lbName.setText(item.getName());
			lbAdditional.setText(item.getItem().getProductNameShort()+" "+item.getItem().getPage());
			if (control.canBeDeselected(item)) {
				lbSymbol.setText(null);
			} else {
				lbSymbol.setText("\uE72E");
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		CoriolisCarriedItem data = itemProperty().get();
//		logger.debug("drag started for "+data);
		if (data==null)
			return;

		Node source = (Node) event.getSource();
//		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);

        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "carried:"+data.getItem().getId()+"|"+data.getUniqueId();
        content.putString(id);
        db.setContent(content);

        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
    }

}
