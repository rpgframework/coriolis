package org.prelle.coriolis.jfx;

public enum ControllerDisplayMode {
	CREATION,
	LEVELLING
}