package org.prelle.coriolis.jfx;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.FormatStyle;
import java.util.Collections;
import java.util.Date;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.yearzeroengine.RewardImpl;
import org.prelle.yearzeroengine.modifications.CurrencyModification;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductServiceLoader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import javafx.util.converter.LocalTimeStringConverter;

public class RewardBox extends HBox {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private static PropertyResourceBundle res = CoriolisCharGenJFXConstants.RES;

	private Reward model;

	private TextField tfDescr;
	private TextField tfNuyen;
	private ChoiceBox<Adventure> cbAdventure;
	private TextField tfExp;
	private ImageView cover;
	private Spinner<LocalTime> timeSpinner;
	private DatePicker datePicker;

	private BooleanProperty enoughData;

	//-------------------------------------------------------------------
	public RewardBox() {
		super(20);
		initComponents();
		initLayout();
		initInterActivity();
	}

	//-------------------------------------------------------------------
	public RewardBox(Reward elem) {
		super(20);
		initComponents();
		setData(elem);
		initLayout();
		initInterActivity();
	}

	//--------------------------------------------------------------------
	public BooleanProperty enoughDataProperty() { return enoughData; }
	public boolean hasEnoughData() { return enoughData.get(); }

	//--------------------------------------------------------------------
	private void initComponents() {
		LocalDateTime now = LocalDateTime.now();

		enoughData = new SimpleBooleanProperty(true);
		datePicker = new DatePicker();
		datePicker.setValue(now.toLocalDate());
		timeSpinner = new Spinner<LocalTime>(new SpinnerValueFactory<LocalTime>(){
			{
	            setConverter(new LocalTimeStringConverter(FormatStyle.MEDIUM));
	        }
			@Override
			public void decrement(int steps) {
				 if (getValue() == null)
		                setValue(LocalTime.now());
		            else {
		                LocalTime time = (LocalTime) getValue();
		                setValue(time.minusMinutes(steps));
		            }
			}

			@Override
			public void increment(int steps) {
				if (this.getValue() == null)
	                setValue(LocalTime.now());
	            else {
	                LocalTime time = (LocalTime) getValue();
	                setValue(time.plusMinutes(steps));
	            }
			}});
		timeSpinner.setEditable(true);
		timeSpinner.getEditor().textProperty().addListener( (ov,o,n) -> {
			LocalTime newVal = timeSpinner.getValueFactory().getConverter().fromString(n);
			timeSpinner.getValueFactory().setValue(newVal);
		});
		timeSpinner.getValueFactory().setValue(now.toLocalTime());
		/*
		 * Form
		 */
		tfDescr = new TextField();
		tfNuyen = new TextField();
		cbAdventure = new ChoiceBox<>();
		cbAdventure.getItems().addAll(ProductServiceLoader.getInstance().getAdventures(RoleplayingSystem.CORIOLIS));
		cbAdventure.setConverter(new StringConverter<Adventure>() {
			public String toString(Adventure value) {return value!=null?value.getTitle():"";}
			public Adventure fromString(String string) {return null;}
		});
		Collections.sort(cbAdventure.getItems());
		tfExp = new TextField();
		tfExp.setMaxWidth(100);

		cover = new ImageView();
		cover.setFitWidth(200);
		cover.setFitHeight(300);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label title_l = new Label(res.getString("label.title"));
		Label adv_l = new Label(res.getString("label.adventure"));
		Label exp_l = new Label(res.getString("label.exp"));
		Label res_l = new Label(res.getString("label.birr"));
		Label dat_l = new Label(res.getString("label.when"));
		res_l.setMaxHeight(Double.MAX_VALUE);
		res_l.setAlignment(Pos.TOP_LEFT);

		HBox timeAndDate = new HBox(5);
		timeAndDate.getChildren().addAll(datePicker, timeSpinner);


		VBox form = new VBox(5);
		form.getChildren().addAll(title_l, tfDescr, adv_l, cbAdventure);
		if (model==null) {
			form.getChildren().addAll(exp_l, tfExp, res_l, tfNuyen);
		}
		form.getChildren().addAll(dat_l, timeAndDate);
		VBox.setMargin(adv_l, new Insets(10,0,0,0));
		VBox.setMargin(exp_l, new Insets(10,0,0,0));
		VBox.setMargin(res_l, new Insets(10,0,0,0));

		getChildren().addAll(cover, form);
	}

	//--------------------------------------------------------------------
	private void initInterActivity() {
		cbAdventure.getSelectionModel().selectedItemProperty().addListener((ov,o,n) -> {
			if (n!=null && (n.getCover()!=null)) {
				Image img = new Image(new ByteArrayInputStream(n.getCover()));
				cover.setImage(img);
			} else
				cover.setImage(null);
		});

		tfDescr.textProperty().addListener((ov,o,n) -> testEnoughData());
		tfExp.textProperty().addListener((ov,o,n) -> testEnoughData());
	}

	//--------------------------------------------------------------------
	public void setData(Reward value) {
		this.model = value;
		tfDescr.setText(value.getTitle());

		Instant dateTime = Instant.ofEpochMilli(value.getDate().getTime());
		LocalDateTime timeAndDate = LocalDateTime.ofInstant(dateTime, ZoneId.systemDefault());
		datePicker.setValue(timeAndDate.toLocalDate());
		timeSpinner.getValueFactory().setValue(timeAndDate.toLocalTime());

		// Collect EP
		tfExp.setText(String.valueOf(model.getExperiencePoints()));

		logger.debug("Search "+value.getId());
		if (value.getId()!=null) {
			for (Adventure adv : ProductServiceLoader.getInstance().getAdventures(RoleplayingSystem.CORIOLIS)) {
//				logger.debug("  Compare with "+adv.getId());
				if (value.getId().equals(adv.getId())) {
					cbAdventure.getSelectionModel().select(adv);
					break;
				}
			}
		} else
			cbAdventure.getSelectionModel().clearSelection();
	}

	//--------------------------------------------------------------------
	private void testEnoughData() {
		enoughData.set(testEnoughData_impl());
	}

	//--------------------------------------------------------------------
	private boolean testEnoughData_impl() {
		logger.debug("testEnoughData_impl");
		if (tfDescr.getText()==null || tfDescr.getText().length()<1) return false;
		if (tfExp.getText()==null || tfExp.getText().length()<1) return false;
		try {
			int foo = Integer.parseInt(tfExp.getText());
			if (foo==0)
				return false;
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	public RewardImpl getDataAsReward() {
		LocalDateTime dateAndTime = LocalDateTime.of(datePicker.getValue(), timeSpinner.getValue());
		ZonedDateTime zonedDateTime = dateAndTime.atZone(ZoneId.systemDefault());
		Instant instant = Instant.from(zonedDateTime);
		Date date = Date.from(instant);
		logger.info("Date is "+date);

		int exp = Integer.valueOf(tfExp.getText());
		RewardImpl ret = new RewardImpl(exp, tfDescr.getText());
		ret.setDate(date);
		if (cbAdventure.getValue()!=null)
			ret.setId(cbAdventure.getValue().getId());

		try {
			if (!tfNuyen.getText().isEmpty() && Integer.valueOf(tfNuyen.getText())>0) {
				CurrencyModification mod = (new CurrencyModification(Integer.valueOf(tfNuyen.getText())));
				mod.setDate(date);
				ret.addModification(mod);
			}
		} catch (Exception e) {
			logger.error("Error parsing Birr reward: "+tfNuyen.getText()+": "+e);
		}
		
		return ret;
	}
}