/**
 * 
 */
package org.prelle.coriolis.jfx;

import org.prelle.coriolis.CoriolisCharacter;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class ExpLine extends HBox {

	private Label lbExpTotal;
	private Label lbExpInvested;
	private Label lbBirr;

	//-------------------------------------------------------------------
	public ExpLine() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lbExpTotal    = new Label("?");
		lbExpInvested = new Label("?");
		lbBirr       = new Label("?");
		lbExpTotal.getStyleClass().add("base");
		lbExpInvested.getStyleClass().add("base");
		lbBirr.getStyleClass().add("base");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Label hdExpTotal    = new Label(CoriolisCharGenJFXConstants.RES.getString("label.expTotal")+": ");
		Label hdExpInvested = new Label(CoriolisCharGenJFXConstants.RES.getString("label.expUsed")+": ");
		Label hdBirr        = new Label(CoriolisCharGenJFXConstants.RES.getString("label.birr")+": ");
		
		this.getChildren().addAll(hdExpTotal, lbExpTotal, hdExpInvested, lbExpInvested, hdBirr, lbBirr);
		HBox.setMargin(hdExpInvested, new Insets(0,0,0,20));
		HBox.setMargin(hdBirr, new Insets(0,0,0,20));
		this.getStyleClass().add("character-document-view-firstline");
	}

	//-------------------------------------------------------------------
	public void setData(CoriolisCharacter model) {
		lbExpTotal.setText((model.getExpInvested()+model.getExpFree())+"");
		lbExpInvested.setText(model.getExpInvested()+"");
		lbBirr.setText(model.getBirr()+"");
	}

}
