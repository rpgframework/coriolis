/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.GroupConcept;
import org.prelle.coriolis.HomeSystem;
import org.prelle.coriolis.Origin;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.WindowMode;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.Talent.Type;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;

import de.rpgframework.character.CharacterHandle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class BasicDataSection extends SingleSection implements ResponsiveControl {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterController control;
	private CoriolisCharacter model;
	private CharacterHandle handle;

	private TextField tfName;
	//	private ComboBox<CharacterClass> cbConcept;
	//	private ComboBox<SubConcept> cbSubConcept;
	private Label lbConcept;
	private ChoiceBox<Origin> cbOrigin;
	private ChoiceBox<HomeSystem> cbHomeSystem;
	private Label lbHumanType;
	private Label lbUpbringing;
	private Label lbIcon;
	private TextField lbReputation;
	private ChoiceBox<GroupConcept> cbGroupConcept;
	private ChoiceBox<Talent> cbGroupTalent;
	private TextField tfPersonalProblem;

	private Label hdName;
	private Label hdConcept;
	private Label hdOrigin;
	private Label hdHomeSystem;
	private Label hdHumanType;
	private Label hdUpbringing;
	private Label hdGroupConcept;
	private Label hdGroupTalent;
	private Label hdPersonalProblem;
	private Label hdIcon;
	private Label hdReputation;

	private ObjectProperty<YearZeroModule> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public BasicDataSection(String title, CoriolisCharacterController ctrl, CharacterHandle handle, ScreenManagerProvider provider) {
		super(provider, title, null);
		control = ctrl;
		this.handle = handle;
		model = ctrl.getCharacter();

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		String c1 = (model.getCharacterClass()!=null)?model.getCharacterClass().getName():"?";
		String c2 = (model.getSubconcept()!=null)?model.getSubconcept().getName():"?";
		lbConcept = new Label(c1+"/"+c2);

		cbOrigin     = new ChoiceBox<Origin>();
		cbOrigin.getItems().addAll(Origin.values());
		cbOrigin.setConverter(new StringConverter<Origin>() {
			public String toString(Origin val) {return val!=null?val.getName():"";}
			public Origin fromString(String string) {return null;}
		});
		cbHomeSystem = new ChoiceBox<HomeSystem>();
		cbHomeSystem.getItems().addAll(control.getRuleset().getListByType(HomeSystem.class));
		cbHomeSystem.setConverter(new StringConverter<HomeSystem>() {
			public String toString(HomeSystem val) {return val!=null?val.getName():"";}
			public HomeSystem fromString(String string) {return null;}
		});
		lbHumanType  = new Label();
		lbUpbringing = new Label();
		lbIcon       = new Label();
		lbReputation = new TextField();
		lbReputation.setPrefColumnCount(2);
		lbReputation.setStyle("-fx-max-width: 2em");
		cbGroupConcept = new ChoiceBox<GroupConcept>();
		cbGroupConcept.getItems().addAll(control.getRuleset().getListByType(GroupConcept.class));
		cbGroupConcept.setConverter(new StringConverter<GroupConcept>() {
			public String toString(GroupConcept val) {return val!=null?val.getName():"";}
			public GroupConcept fromString(String string) {return null;}
		});
		cbGroupTalent  = new ChoiceBox<Talent>();
		cbGroupTalent.getItems().addAll(control.getRuleset().getListByType(Talent.class).stream().filter(t -> t.getType()==Type.GROUP).collect(Collectors.toList()));
		cbGroupTalent.setConverter(new StringConverter<Talent>() {
			public String toString(Talent val) {return val!=null?val.getName():"";}
			public Talent fromString(String string) {return null;}
		});

		tfPersonalProblem = new TextField();

		tfName.setMaxWidth(Double.MAX_VALUE);
		tfPersonalProblem.setMaxWidth(Double.MAX_VALUE);

		hdName = new Label(UI.getString("label.name"));
		hdName.getStyleClass().add("base");
		hdConcept = new Label(UI.getString("label.concept"));
		hdConcept.getStyleClass().add("base");
		hdGroupConcept = new Label(UI.getString("label.group_concept"));
		hdGroupConcept.getStyleClass().add("base");
		hdGroupTalent= new Label(UI.getString("label.group_talent"));
		hdGroupTalent.getStyleClass().add("base");
		hdPersonalProblem = new Label(UI.getString("label.personal_problem"));
		hdPersonalProblem.getStyleClass().add("base");
		hdOrigin = new Label(UI.getString("label.origin"));
		hdOrigin.getStyleClass().add("base");
		hdHomeSystem = new Label(UI.getString("label.homesystem"));
		hdHomeSystem.getStyleClass().add("base");
		hdHumanType  = new Label(UI.getString("label.humantype"));
		hdHumanType.getStyleClass().add("base");
		hdUpbringing = new Label(UI.getString("label.upbringing"));
		hdUpbringing.getStyleClass().add("base");
		hdIcon = new Label(UI.getString("label.icon"));
		hdIcon.getStyleClass().add("base");
		hdReputation = new Label(AttributeCoriolis.REPUTATION.getName());
		hdReputation.getStyleClass().add("base");

	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		//		HBox clName         = new HBox(5, lbName, tfName);
		//		HBox clConcept      = new HBox(5, lbConcept, tfConcept);
		//		HBox clGroupConcept = new HBox(5, lbGroupConcept, tfGroupConcept);
		//		HBox clBackground   = new HBox(5, lbBackground, tfBackground);
		//		HBox clIcon         = new HBox(5, lbIcon, tfIcon);
		//		HBox clReputation   = new HBox(5, lbReputation, spReputation);
		//		HBox clPersProblem  = new HBox(5, lbPersonalProblem, tfPersonalProblem);
		HBox.setHgrow(tfName, Priority.ALWAYS);
		HBox.setHgrow(lbConcept, Priority.ALWAYS);
		HBox.setHgrow(cbGroupConcept, Priority.ALWAYS);
		HBox.setHgrow(tfPersonalProblem, Priority.ALWAYS);

		GridPane grid = new GridPane();
		grid.add(hdName        , 0, 0);
		grid.add(tfName        , 1, 0);
		grid.add(hdConcept     , 2, 0);
		//		if (mode==ControllerDisplayMode.CREATION) {
		//			grid.add(new HBox(5, cbConcept, cbSubConcept), 3, 0);
		//		} else {
		grid.add(lbConcept, 3,0);
		//		}
		grid.add(hdOrigin      , 0, 1);
		grid.add(cbOrigin      , 1, 1);
		grid.add(hdHomeSystem  , 2, 1);
		grid.add(cbHomeSystem  , 3, 1);

		grid.add(hdHumanType   , 0, 2);
		grid.add(lbHumanType   , 1, 2);
		grid.add(hdUpbringing  , 2, 2);
		grid.add(lbUpbringing  , 3, 2);

		grid.add(hdIcon        , 0, 3);
		grid.add(lbIcon        , 1, 3);
		grid.add(hdReputation  , 2, 3);
		grid.add(lbReputation  , 3, 3);
		grid.add(hdGroupConcept, 0, 4);
		grid.add(cbGroupConcept, 1, 4);
		grid.add(hdGroupTalent , 2, 4);
		grid.add(cbGroupTalent , 3, 4);
		grid.add(hdPersonalProblem , 0, 5);
		grid.add(tfPersonalProblem , 1, 5, 3,1);
		grid.setStyle("-fx-background-color: lightgrey; -fx-vgap: 0.5em; -fx-padding: 0.3em; -fx-hgap: 0.5em");
		
		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initLayoutMinimal() {
		getChildren().clear();
		VBox grid = new VBox(5);
		grid.getChildren().addAll(hdName, tfName);
		//		if (mode==ControllerDisplayMode.CREATION) {
		//			grid.getChildren().addAll(hdConcept, new HBox(5, cbConcept, cbSubConcept));
		//		} else {
		grid.getChildren().addAll(hdConcept, lbConcept);
		//		}
		grid.getChildren().addAll(hdGroupConcept, cbGroupConcept);
		grid.getChildren().addAll(hdOrigin, cbOrigin);
		grid.getChildren().addAll(hdIcon, lbIcon);
		grid.getChildren().addAll(hdReputation, lbReputation);
		grid.getChildren().addAll(hdPersonalProblem, tfPersonalProblem);
		grid.setStyle("-fx-background-color: lightgrey; -fx-spacing: 0.5em; -fx-padding: 0.3em");
		getChildren().add(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				logger.error("Ignore renaming to null");
				return;
			}
			logger.info("rename character from "+control.getCharacter().getName()+" to "+n);
			control.getCharacter().setName(n);
			GenerationEventDispatcher.fireCharacterChange(model);
			});
		lbReputation.textProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				logger.error("Ignore renaming to null");
				return;
			}
			logger.info("change Reputation to "+n);
			AttributeValue val = control.getCharacter().getAttribute(AttributeCoriolis.REPUTATION);
			val.setPoints( Integer.parseInt(n) - val.getModifiedValue() );
			GenerationEventDispatcher.fireCharacterChange(model);
			});

		cbGroupConcept.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set(n);
			model.setGroupConcept(n);
		});
		cbGroupTalent.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set(n);
			model.setGroupTalent(n);
		});
		cbHomeSystem.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set(n);
			model.setHomeSystem(n);
		});
		cbOrigin.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			model.setOrigin(n);
		});

	}

	//-------------------------------------------------------------------
	/**
	 * @param value
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.debug("Set mode to "+value);
		switch (value) {
		case COMPACT:
		case EXPANDED:
			initLayoutNormal();
			break;
		case MINIMAL:
			initLayoutMinimal();
			break;
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		tfName.setText(model.getName());

		lbConcept.setText(
				((model.getCharacterClass()!=null)?model.getCharacterClass().getName():null)
				+" / "+
				((model.getSubconcept()!=null)?model.getSubconcept().getName():null)
				);

		cbOrigin.getSelectionModel().select(model.getOrigin());
		cbHomeSystem.getSelectionModel().select(model.getHomeSystem());
		lbHumanType.setText( (model.getHumanType()!=null)?model.getHumanType().getName():null);
		lbUpbringing.setText( (model.getUpbringing()!=null)?model.getUpbringing().getName():null);

		lbIcon.setText( ((model.getIcon()!=null)?model.getIcon().getName():null));
		lbReputation.setText(String.valueOf(model.getAttribute(AttributeCoriolis.REPUTATION).getModifiedValue()));

		cbGroupConcept.getSelectionModel().select(model.getGroupConcept());
		cbGroupTalent.getSelectionModel().select(model.getGroupTalent());
		tfPersonalProblem.setText(model.getProblem());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<YearZeroModule> showHelpForProperty() {
		return showHelpFor;
	}

}
