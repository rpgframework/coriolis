package org.prelle.coriolis.jfx.sections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCarriedItem;
import org.prelle.coriolis.CoriolisItemTemplate;
import org.prelle.coriolis.CoriolisItemTemplate.Weight;
import org.prelle.coriolis.SelectOption;
import org.prelle.coriolis.Selection;
import org.prelle.coriolis.TechTier;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.charctrl.CoriolisGearController;
import org.prelle.coriolis.jfx.CarriedItemListCell;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.SelectorWithHelp;
import org.prelle.coriolis.jfx.dialogs.ItemTemplateSelector;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.yearzeroengine.CarriedItem;
import org.prelle.yearzeroengine.ItemLocation;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;

/**
 * @author Stefan Prelle
 *
 */
public class GearSection extends GenericListSection<CoriolisCarriedItem> {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(GearSection.class.getName());

	private ItemLocation location;

	//-------------------------------------------------------------------
	public GearSection(String title, CoriolisCharacterController ctrl, ScreenManagerProvider provider, ItemLocation location) {
		super(title, ctrl, provider);
		this.location = location;
		initPlaceholder();
		initCellFactories();
		initLayout();
		list.setOnDragOver(ev -> dragOverSelected(ev));
		list.setOnDragDropped(ev -> dragDroppedSelected(ev));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(UI.getString("equipmentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	protected  void initCellFactories() {
		list.setCellFactory(lv -> new CarriedItemListCell(control));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		list.setStyle("-fx-min-width: 22em; -fx-pref-width: 29em; -fx-pref-height: 40em");
		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getItems().stream().filter(ci -> ci.getLocation()==location).collect(Collectors.toList()));
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.debug("opening gear selection dialog");
		
		ItemTemplateSelector selector = new ItemTemplateSelector(location, control);
		SelectorWithHelp<CoriolisItemTemplate> pane = new SelectorWithHelp<CoriolisItemTemplate>(selector);
		ManagedDialog dialog = new ManagedDialog(UI.getString("selectiondialog.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.debug("Closed with "+close);
		if (close==CloseType.OK) {
			CoriolisItemTemplate toMove = selector.getSelectedItem();
			logger.debug("Selected gear: "+toMove);
			if (toMove!=null) {
        		// Some items require some refinement choices 
        		Selection[] choices = askNecessaryChoices(toMove);
        		logger.debug("  with choices "+Arrays.asList(choices));
        		// Now select
         		CarriedItem<CoriolisItemTemplate> selected = ((CoriolisGearController)control.getGearController()).select(toMove, location, choices);
        		if (selected!=null) {
        			logger.info("Added item "+toMove);
        		} else {
        			logger.warn("Failed adding item: "+toMove);
        		}        			

//        		CarriedItem<ItemTemplate> item = control.getGearController().select(selected, location);
//				if (item==null) {
//					getManagerProvider().getScreenManager().showAlertAndCall(AlertType.ERROR, "header", "Cannot select "+selected);
//				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	protected void onDelete() {
		CoriolisCarriedItem selected = list.getSelectionModel().getSelectedItem();
		logger.debug("CarriedItem to deselect: "+selected);
		control.getGearController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	private Selection[] askNecessaryChoices(CoriolisItemTemplate toMove) {
		List<Selection> choices_l = new ArrayList<>();
		GridPane content = new GridPane();
		content.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		int y=0;
		// For "other" - ask name
		if (toMove.getId().equals("other")) {
			logger.debug("  'other' item - ask user for name");
			Label lbName = new Label(UI.getString("gear.select.name"));
			lbName.getStyleClass().add("base");
			TextField tfName = new TextField();
			tfName.setPrefColumnCount(20);
			tfName.textProperty().addListener( (ov,o,n) -> {
				for (Selection tmp : choices_l) {
					if (tmp.option==SelectOption.NAME) { choices_l.remove(tmp); break; }
				}
				if (tfName.getText()!=null)
					choices_l.add(new Selection(SelectOption.NAME, tfName.getText()));
			});			
			content.add(lbName, 0, y);
			content.add(tfName, 1, y++);
		}
		// Tech tiers
		if (toMove.getTier().size()>1) {
			logger.debug("  ask user to select technology tier");
			ChoiceBox<TechTier> cbTier = new ChoiceBox<TechTier>(FXCollections.observableArrayList(toMove.getTier()));
			cbTier.getSelectionModel().select(0);
			choices_l.add(new Selection(SelectOption.TIER, cbTier.getValue()));
			
			Label lbTier = new Label(UI.getString("gear.select.tier"));
			lbTier.getStyleClass().add("base");
			content.add(lbTier, 0, y);
			content.add(cbTier, 1, y++);
			
			cbTier.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
				for (Selection tmp : choices_l) {
					if (tmp.option==SelectOption.TIER) { choices_l.remove(tmp); break; }
				}
				choices_l.add(new Selection(SelectOption.TIER, cbTier.getValue()));
				logger.debug("  user chose tech tier "+cbTier.getValue());
			});
		}
		// Weight
		if (toMove.getWeight().isFlexibel()) {
			logger.debug("  ask user to select weight");
			ChoiceBox<Weight> cbWeight = new ChoiceBox<Weight>(FXCollections.observableArrayList(toMove.getWeight().getWeights()));
			cbWeight.getSelectionModel().select(0);
			choices_l.add(new Selection(SelectOption.WEIGHT, cbWeight.getValue()));
			
			Label label = new Label(UI.getString("gear.select.weight"));
			label.getStyleClass().add("base");
			content.add(label, 0, y);
			content.add(cbWeight, 1, y++);
			
			cbWeight.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
				for (Selection tmp : choices_l) {
					if (tmp.option==SelectOption.WEIGHT) { choices_l.remove(tmp); break; }
				}
				choices_l.add(new Selection(SelectOption.WEIGHT, cbWeight.getValue()));
				logger.debug("  user chose weight "+cbWeight.getValue());
			});
		}
		// Cost / Birr
		if (toMove.getCost().isFlexibel()) {
			logger.debug("  ask user to select cost/birr");
			TextField tfCost = new TextField();
			tfCost.setPromptText(toMove.getCost().getMin()+" - "+toMove.getCost().getMax());
			tfCost.setPrefColumnCount(10);
			tfCost.textProperty().addListener( (ov,o,n) -> {
				for (Selection tmp : choices_l) {
					if (tmp.option==SelectOption.COST) { choices_l.remove(tmp); break; }
				}
				try {
					if (tfCost.getText()!=null)
						choices_l.add(new Selection(SelectOption.COST, Integer.parseInt(tfCost.getText())));
				} catch (NumberFormatException e) {
					tfCost.textProperty().set(null);
				}
			});			
			
			Label label = new Label(UI.getString("gear.select.cost"));
			label.getStyleClass().add("base");
			content.add(label, 0, y);
			content.add(tfCost, 1, y++);
		}
		
		if (!choices_l.isEmpty())
			getManagerProvider().getScreenManager().showAlertAndCall(AlertType.QUESTION, UI.getString("dialog.choose.title"), content);
		
		
		Selection[] choices = new Selection[choices_l.size()];
		choices = choices_l.toArray(choices);
		return choices;
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped "+enhanceID);
        	// Get reference for ID
        	if (enhanceID.startsWith("carried:")) {
        		StringTokenizer tok = new StringTokenizer(enhanceID.substring(8),"|");
        		String talID = tok.nextToken();
        		String uniqID = tok.nextToken();

        		CoriolisCarriedItem toMove2 = control.getCharacter().getItem(UUID.fromString(uniqID));
        		logger.warn("MOVE "+toMove2);
        		toMove2.setLocation(location);
        		control.runProcessors();
        		
//        		CoriolisItemTemplate toMove = control.getRuleset().getByType(CoriolisItemTemplate.class, talID);
//        		logger.debug("Select item "+toMove);
//        		Platform.runLater(() -> {
//            		// Some items require some refinement choices 
//            		Selection[] choices = askNecessaryChoices(toMove);
//            		logger.debug("  with choices "+Arrays.asList(choices));
//            		// Now select
//             		CarriedItem<CoriolisItemTemplate> selected = ((CoriolisGearController)control.getGearController()).select(toMove, location, choices);
//            		if (selected!=null) {
//            			logger.info("Selected item "+toMove);
//            		} else {
//            			logger.warn("Failed selecting item: "+toMove);
//            		}        			
//        		});
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	        if (event.getDragboard().getString().startsWith("carried:"))
            /* allow for both copying and moving, whatever user chooses */
	        	event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

}
