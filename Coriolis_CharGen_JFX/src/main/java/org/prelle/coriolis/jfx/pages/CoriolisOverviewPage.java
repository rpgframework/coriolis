package org.prelle.coriolis.jfx.pages;

import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.sections.AttributeSection;
import org.prelle.coriolis.jfx.sections.BasicDataSection;
import org.prelle.coriolis.jfx.sections.HealthSection;
import org.prelle.coriolis.jfx.sections.SkillSection;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.BasePluginData;
import org.prelle.yearzeroengine.SkillCategory;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class CoriolisOverviewPage extends CoriolisManagedScreenPage {

	private ScreenManagerProvider provider;

	private BasicDataSection basic;
	private AttributeSection attrib;
	private HealthSection health;
	private SkillSection skillGen;
	private SkillSection skillAdv;

	private Section secLine2;
	private Section secLine3;

	//-------------------------------------------------------------------
	public CoriolisOverviewPage(CoriolisCharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("coriolis-overview");
		this.setTitle(control.getCharacter().getName());
		this.provider = provider;
		this.handle   = handle;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initBasicData() {
		basic = new BasicDataSection(UI.getString("section.basedata"), charGen, handle, provider);
		getSectionList().add(basic);

		// Interactivity
		basic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initAttributes() {
		attrib = new AttributeSection(UI.getString("section.attributes"), charGen, provider);
		health  = new HealthSection(UI.getString("section.health"), charGen, provider);

		secLine2 = new DoubleSection(attrib, health);
		getSectionList().add(secLine2);

		// Interactivity
//		attrPri.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initLine3() {
		SkillCategory GENERAL = null;
		SkillCategory ADVANCED = null;
		try {
			GENERAL = charGen.getRuleset().getSkillCategoryConverter().read("GENERAL");
			ADVANCED = charGen.getRuleset().getSkillCategoryConverter().read("ADVANCED");
		} catch (Exception e) {
			logger.error(e);
			return;
		}
		skillGen = new SkillSection(UI.getString("section.skills.general"), charGen, provider, GENERAL);
		skillAdv  = new SkillSection(UI.getString("section.skills.advanced"), charGen, provider, ADVANCED);

		secLine3 = new DoubleSection(skillGen, skillAdv);
		getSectionList().add(secLine3);

		// Interactivity
		skillGen.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		skillAdv.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initBasicData();
		initAttributes();
		initLine3();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//-------------------------------------------------------------------
	private void updateHelp(Attribute data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
