/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.AttributeCoriolis;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.AttributeValue;
import org.prelle.yearzeroengine.YearZeroEngineCharacter;
import org.prelle.yearzeroengine.charctrl.AttributeController;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class AttributeSection extends SingleSection {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private Map<Attribute, Label> fields;

	private AttributeController attrGen;
	private YearZeroEngineCharacter model;

	//-------------------------------------------------------------------
	public AttributeSection(String title, CharacterController<? extends YearZeroEngineCharacter> gen, ScreenManagerProvider provider) {
		super(provider, title, null);
		if (gen==null)
			throw new NullPointerException();
		this.attrGen = gen.getAttributeController();
		model = gen.getCharacter();

		initCompontents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
//		getStyleClass().add("priority-table");

		Attribute[] attributes = new Attribute[]{
				AttributeCoriolis.STRENGTH,
				AttributeCoriolis.AGILITY,
				AttributeCoriolis.EMPATHY,
				AttributeCoriolis.WITS,
				AttributeCoriolis.HIT_POINTS,
				AttributeCoriolis.MIND_POINTS
		};
		fields = new HashMap<Attribute, Label>();
		for (Attribute key : attributes) {
			Label boxes = new Label();
			fields.put(key, boxes);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdStrength = new Label(AttributeCoriolis.STRENGTH.getName());
		hdStrength.getStyleClass().add("base");
		Label hdAgility = new Label(AttributeCoriolis.AGILITY.getName());
		hdAgility.getStyleClass().add("base");
		Label hdHit = new Label(AttributeCoriolis.HIT_POINTS.getName());
		hdHit.getStyleClass().add("base");

		Label hdEmpathy = new Label(AttributeCoriolis.EMPATHY.getName());
		hdEmpathy.getStyleClass().add("base");
		Label hdWits = new Label(AttributeCoriolis.WITS.getName());
		hdWits.getStyleClass().add("base");
		Label hdMind = new Label(AttributeCoriolis.MIND_POINTS.getName());
		hdMind.getStyleClass().add("base");

		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em; -fx-padding: 0.5em");
		grid.add(hdStrength, 0, 0);
		grid.add(fields.get(AttributeCoriolis.STRENGTH), 1, 0);
		grid.add(hdAgility, 2, 0);
		grid.add(fields.get(AttributeCoriolis.AGILITY), 3, 0);
		grid.add(hdHit, 4, 0);
		grid.add(fields.get(AttributeCoriolis.HIT_POINTS), 5, 0);

		grid.add(hdEmpathy, 0, 1);
		grid.add(fields.get(AttributeCoriolis.EMPATHY), 1, 1);
		grid.add(hdWits, 2, 1);
		grid.add(fields.get(AttributeCoriolis.WITS), 3, 1);
		grid.add(hdMind, 4, 1);
		grid.add(fields.get(AttributeCoriolis.MIND_POINTS), 5, 1);
		
		grid.getColumnConstraints().add(new ColumnConstraints(60,80,100));
		grid.getColumnConstraints().add(new ColumnConstraints(20,30,40));
		grid.getColumnConstraints().add(new ColumnConstraints(80,120,140));
		grid.getColumnConstraints().add(new ColumnConstraints(20,30,40));
		grid.getColumnConstraints().add(new ColumnConstraints(100,140,160));
		grid.getColumnConstraints().add(new ColumnConstraints(20,30,40));
		
		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	private void setAttributeTo(Attribute attrib, int val) {
		int current = model.getAttribute(attrib).getPoints();
		logger.debug("Set "+attrib+" to "+val);
		// Loop until requested value reached
		while (current!=val) {
			if (current>val) {
				// Must be decreased
				if (attrGen.canBeDecreased(model.getAttribute(attrib))) {
					attrGen.decrease(model.getAttribute(attrib));
				} else {
					logger.warn("Cannot decrease from "+current+" to "+val);
					refresh();
					break;
				}
			} else {
				// Must be increased
				if (attrGen.canBeIncreased(model.getAttribute(attrib))) {
					attrGen.increase(model.getAttribute(attrib));
				} else {
					logger.warn("Cannot increase from "+current+" to "+val);
					refresh();
					break;
				}
			}
			current = model.getAttribute(attrib).getPoints();
		}
		logger.debug("Done");
	}

	//-------------------------------------------------------------------
	public void refresh() {
		for (Attribute key : fields.keySet()) {
			AttributeValue val = model.getAttribute(key);
			fields.get(key).setText(String.valueOf(val.getModifiedValue()));
		}
	}

}
