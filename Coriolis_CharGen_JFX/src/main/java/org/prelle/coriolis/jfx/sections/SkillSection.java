/**
 *
 */
package org.prelle.coriolis.jfx.sections;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.tables.SkillValueTableView;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.SkillCategory;
import org.prelle.yearzeroengine.charctrl.CharacterController;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author prelle
 *
 */
public class SkillSection extends SingleSection {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private CoriolisCharacterController control;

	private SkillValueTableView table;

	private ObjectProperty<Skill> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public SkillSection(String title, CharacterController<CoriolisCharacter> control, ScreenManagerProvider provider, SkillCategory category) {
		super(provider, title, null);
		this.control = (CoriolisCharacterController) control;
		initComponents(category, provider);
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents(SkillCategory type, ScreenManagerProvider provider) {
		table = new SkillValueTableView(control, type);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(table);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.debug("Selection changed to "+n);
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		logger.debug("refresh");
		table.setData(control.getCharacter());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<Skill> showHelpForProperty() {
		return showHelpFor;
	}

}
