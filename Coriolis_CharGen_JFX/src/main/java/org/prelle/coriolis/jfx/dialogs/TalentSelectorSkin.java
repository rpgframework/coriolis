/**
 * 
 */
package org.prelle.coriolis.jfx.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCore;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.listcells.TalentListCell;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.YearZeroEngineRuleset;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class TalentSelectorSkin extends SkinBase<TalentSelector> implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(CoriolisCharGenJFXConstants.LOGGER);

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;

	private YearZeroEngineRuleset rules;
	private CoriolisCharacterController controller;
	private VBox content;
	private TextField tfName;
	private ChoiceBox<Talent.Type> cbTypes;
	private ListView<Talent> lvItems;
	
	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public TalentSelectorSkin(TalentSelector control, CoriolisCharacterController controller) {
		super(control);
		this.rules = controller.getRuleset();
		this.controller = controller;
		
		initComponents(control.getAllowedTypes());
		initLayout();
		initInteractivity();
		cbTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(Talent.Type[] allowedTypes) {
		tfName = new TextField();
		tfName.setPromptText(UI.getString("talentselector.name.prompt"));

		cbTypes = new ChoiceBox<Talent.Type>();
		cbTypes.setMaxWidth(Double.MAX_VALUE);
		cbTypes.getItems().addAll(allowedTypes);
		cbTypes.setConverter(new StringConverter<Talent.Type>() {
			public String toString(Talent.Type data) {return (data==null)?"":CoriolisCore.getI18nResources().getString("talenttype."+data.name().toLowerCase()); }
			public Talent.Type fromString(String data) {return null;}
		});

		lvItems = new ListView<Talent>();
		lvItems.setCellFactory(new Callback<ListView<Talent>, ListCell<Talent>>() {
			public ListCell<Talent> call(ListView<Talent> param) {
				return new TalentListCell(controller);
			}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new VBox();
		content.setStyle("-fx-spacing: 0.5em");
		content.getChildren().addAll(tfName, cbTypes, lvItems);
		VBox.setVgrow(lvItems, Priority.ALWAYS);

		lvItems.setStyle("-fx-pref-width: 30em");
		lvItems.setMaxHeight(Double.MAX_VALUE);

		getSkinnable().impl_getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				lvItems.getItems().clear();
				lvItems.getItems().addAll(
						rules.getListByType(Talent.class).stream().filter(item -> item.getType()==cbTypes.getValue()).collect(Collectors.toList()));
			}
		});

		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			this.getSkinnable().setSelectedItem(n);
		});

		tfName.setOnAction(event -> {
			String filter = tfName.getText();
			List<Talent> poss = rules.getListByType(Talent.class);
			List<Talent> filtered = new ArrayList<>();
			if (filter.length()==0) {
				filtered = poss;
			} else {
				// Filter those names that match search string
				for (Talent tmp : poss) {
					if (tmp.getName().toLowerCase().contains(filter.toLowerCase())) {
						filtered.add(tmp);
					}
				}
			}
			lvItems.getItems().clear();
			lvItems.getItems().addAll(filtered);
		});
	}

	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			lvItems.getItems().clear();
			lvItems.getItems().addAll(rules.getListByType(Talent.class));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	SelectionModel<Talent> getSelectionModel() {
		return lvItems.getSelectionModel();
	}

}
