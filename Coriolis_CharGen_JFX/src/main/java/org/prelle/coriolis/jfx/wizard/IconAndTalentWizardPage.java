/**
 *
 */
package org.prelle.coriolis.jfx.wizard;

import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.Icon;
import org.prelle.coriolis.chargen.CoriolisCharacterGenerator;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.listcells.TalentListCell;
import org.prelle.coriolis.jfx.listcells.TalentValueListCell;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.yearzeroengine.Talent;
import org.prelle.yearzeroengine.TalentValue;
import org.prelle.yearzeroengine.YearZeroModule;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventListener;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class IconAndTalentWizardPage extends WizardPage implements
GenerationEventListener {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;
	private static PropertyResourceBundle RES = CoriolisCharGenJFXConstants.RES;

	private CoriolisCharacterGenerator ctrl;

	private Button btIcon;
	private ChoiceBox<Icon> cbIcon;

	private ListView<Talent> lvAvailable;
	private ListView<TalentValue> lvSelected;

	private Label lbSelectName;
	private Label lbSelectRef;
	private Label lbSelectDesc;

	//--------------------------------------------------------------------
	public IconAndTalentWizardPage(Wizard wizard, CoriolisCharacterGenerator ctrl) {
		super(wizard);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		setTitle(RES.getString("wizard.iconAndTalent.title"));
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		cbIcon  = new ChoiceBox<>();
		cbIcon.getItems().addAll(ctrl.getRuleset().getListByType(Icon.class));
		cbIcon.getSelectionModel().select(ctrl.getCharacter().getIcon());
		cbIcon.setConverter(new StringConverter<Icon>() {
			public String toString(Icon val) {return val!=null?val.getName():"";}
			public Icon fromString(String arg0) {return null;}
		});
		btIcon = new Button(RES.getString("button.random"));
		lvAvailable = new ListView<Talent>();
		lvAvailable.setCellFactory(lv -> new TalentListCell(ctrl));
		Label placeholder = new Label(RES.getString("wizard.iconAndTalent.talents.avprompt"));
		placeholder.setWrapText(true);
		lvAvailable.setPlaceholder(placeholder);
		lvSelected  = new ListView<TalentValue>();
		lvSelected.getItems().addAll(ctrl.getCharacter().getTalents());
		lvSelected.setCellFactory(lv -> new TalentValueListCell(ctrl));

		/*
		 * Explain
		 */
		lbSelectName = new Label();
		lbSelectName.getStyleClass().add("title");
		lbSelectRef = new Label();
		lbSelectRef.getStyleClass().add("subtitle");
		lbSelectDesc = new Label();
		lbSelectDesc.setWrapText(true);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Icon
		 */
		Label hdIcon = new Label(RES.getString("wizard.iconAndTalent.icon.heading"));
		hdIcon.getStyleClass().add("subtitle");
		Label lbIntroIcon = new Label(RES.getString("wizard.iconAndTalent.icon.intro"));
		lbIntroIcon.setWrapText(true);

		HBox iconLine = new HBox();
		iconLine.getChildren().addAll(btIcon, cbIcon);
		iconLine.setStyle("-fx-spacing: 1em;");

		/*
		 * Talents
		 */
		Label hdTalents = new Label(RES.getString("wizard.iconAndTalent.talents.heading"));
		hdTalents.getStyleClass().add("subtitle");
		Label lbIntroTal = new Label(RES.getString("wizard.iconAndTalent.talents.intro"));
		lbIntroTal.setWrapText(true);

		GridPane bxTalents = new GridPane();
		Label lbAvailable = new Label(RES.getString("label.available"));
		Label lbSelected  = new Label(RES.getString("label.selected"));
		lbAvailable.getStyleClass().add("base");
		lbSelected.getStyleClass().add("base");
		bxTalents.setStyle("-fx-hgap: 3em; -fx-vgap: 0.5em");
		bxTalents.add(lbAvailable, 0, 0);
		bxTalents.add(lbSelected , 1, 0);
		bxTalents.add(lvAvailable, 0, 1);
		bxTalents.add(lvSelected , 1, 1);

		VBox content = new VBox();
		content.setStyle("-fx-spacing: 0.5em");
		content.getChildren().addAll(hdIcon, lbIntroIcon, iconLine);
		content.getChildren().addAll(hdTalents, lbIntroTal, bxTalents);
		VBox.setMargin(hdTalents, new Insets(20,0,0,0));

		/*
		 * Explain
		 */
		VBox explain = new VBox(20);
		explain.getChildren().addAll(lbSelectName, lbSelectRef, lbSelectDesc);

		Separator line = new Separator(Orientation.VERTICAL);
		line.setStyle("-fx-padding: 0px 2em 0px 2em");

		HBox allContent = new HBox(content, line, explain);
		allContent.setStyle("-fx-spacing: 2em");
//		content.setStyle("-fx-pref-width: 40em; -fx-min-width:35em");
//		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:15em");
		content.setStyle("-fx-pref-width: 45em; -fx-min-width:25em");
		explain.setStyle("-fx-pref-width: 20em; -fx-min-width:10em");
		setContent(allContent);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		btIcon.setOnAction(ev -> ctrl.getIconController().rollIcon());
		cbIcon.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info("Selected "+n);
			ctrl.getIconController().setIcon(n);
			if (n!=null) {
				updateHelp(n);
			}
		});

		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)
				-> updateHelp(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)
				-> {if (n!=null) updateHelp(n.getTalent());});

		lvSelected.setOnDragOver(ev -> dragOverSelected(ev));
		lvSelected.setOnDragDropped(ev -> dragDroppedSelected(ev));
		lvAvailable.setOnDragOver(ev -> dragOverAvailable(ev));
		lvAvailable.setOnDragDropped(ev -> dragDroppedAvailable(ev));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.yearzeroengine.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.yearzeroengine.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			cbIcon.getSelectionModel().select(ctrl.getCharacter().getIcon());
			lvAvailable.getItems().clear();
			lvAvailable.getItems().addAll(ctrl.getTalentController().getAvailableTalents());
			lvSelected.getItems().clear();
			lvSelected.getItems().addAll(ctrl.getCharacter().getTalents());
			break;
		}
	}

	//--------------------------------------------------------------------
	private void updateHelp(YearZeroModule n) {
		if (n!=null) {
			lbSelectName.setText(n.getName());
			lbSelectRef.setText(n.getProductName()+" "+n.getPage());
			lbSelectDesc.setText(n.getHelpText());
		}
	}

	//-------------------------------------------------------------------
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	        if (event.getDragboard().getString().startsWith("talent:"))
            /* allow for both copying and moving, whatever user chooses */
	        	event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped "+enhanceID);
        	// Get reference for ID
        	if (enhanceID.startsWith("talent:")) {
        		StringTokenizer tok = new StringTokenizer(enhanceID.substring(7),"|");
        		String talID = tok.nextToken();
//        		String descr = tok.nextToken();
//        		String idref = tok.nextToken();
//        		if (descr!=null) descr=descr.substring(5);
//        		if (idref!=null) idref=idref.substring(6);
//        		if ("null".equals(descr)) descr=null;
//        		if ("null".equals(idref)) idref=null;

        		Talent toMove = ctrl.getRuleset().getByType(Talent.class, talID);
        		logger.debug("Select talent "+toMove);
        		if (ctrl.getTalentController().select(toMove)) {
        			logger.info("Selected talent "+toMove);
        		} else {
        			logger.warn("Failed selecting talent: "+toMove);
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	        if (event.getDragboard().getString().startsWith("talentref:"))
            /* allow for both copying and moving, whatever user chooses */
	        	event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDroppedAvailable(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped "+enhanceID);
        	// Get reference for ID
        	if (enhanceID.startsWith("talentref:")) {
        		StringTokenizer tok = new StringTokenizer(enhanceID.substring(10),"|");
        		String talID = tok.nextToken();

        		Talent talent = ctrl.getRuleset().getByType(Talent.class, talID);
        		TalentValue toMove = ctrl.getCharacter().getTalentValue(talent);
        		logger.debug("Deselect talent "+toMove);
        		if (ctrl.getTalentController().deselect(toMove)) {
        			logger.info("Deselected talent "+toMove);
        		} else {
        			logger.warn("Failed deselecting talent: "+toMove);
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

}
