/**
 * 
 */
package org.prelle.coriolis.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.Logger;
import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.CoriolisTools;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.coriolis.jfx.CoriolisCharGenJFXConstants;
import org.prelle.coriolis.jfx.ExpLine;
import org.prelle.coriolis.jfx.RewardBox;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DevelopmentPage;
import org.prelle.yearzeroengine.HistoryElementImpl;
import org.prelle.yearzeroengine.RewardImpl;
import org.prelle.yearzeroengine.chargen.event.GenerationEvent;
import org.prelle.yearzeroengine.chargen.event.GenerationEventDispatcher;
import org.prelle.yearzeroengine.chargen.event.GenerationEventType;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;
import de.rpgframework.products.ProductServiceLoader;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class CoriolisDevelopmentPage extends DevelopmentPage {

	private final static Logger logger = CoriolisCharGenJFXConstants.LOGGER;

	private static PropertyResourceBundle UI2 = CoriolisCharGenJFXConstants.RES;

	private ScreenManagerProvider provider;
	private CoriolisCharacterController control;
	private CoriolisCharacter model;

	private ExpLine expLine;

	//-------------------------------------------------------------------
	/**
	 */
	public CoriolisDevelopmentPage(CoriolisCharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(UI2,  RoleplayingSystem.CORIOLIS);
		this.control = control;
		this.model = control.getCharacter();
		this.setId("coriolis-development");
		this.setTitle(control.getCharacter().getName());
		this.provider = provider;
		
		expLine = new ExpLine();
		getCommandBar().setContent(expLine);
		setConverter(new StringConverter<Modification>() {
			public String toString(Modification value) {return value!=null?CoriolisTools.toString(value):"";}
			public Modification fromString(String arg0) { return null;}
		});
		setPointsNameProperty(UI2.getString("label.experience.short"));
		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		logger.info("refresh");
		history.setData(CoriolisTools.convertToHistoryElementList(model, super.shallBeAggregated()));
		expLine.setData(control.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(control.getCharacter().getExpFree());
	}

	//-------------------------------------------------------------------
	public void setData(CoriolisCharacter model) {
		this.model = model;
		setTitle(model.getName()+" / "+UI2.getString("label.development"));
		
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openAdd()
	 */
	@Override
	public HistoryElement openAdd() {
		RewardBox content = new RewardBox();
		logger.warn("TODO: openAdd");
		
		ManagedDialog dialog = new ManagedDialog(UI2.getString("dialog.reward.title"), content, CloseType.OK, CloseType.CANCEL);
		
		CloseType closed = (CloseType)provider.getScreenManager().showAndWait(dialog);
		
		if (closed==CloseType.OK) {
			RewardImpl reward = content.getDataAsReward();
			logger.debug("Add reward "+reward);
			CoriolisTools.reward(model, reward);
			HistoryElementImpl elem = new HistoryElementImpl();
			elem.setName(reward.getTitle());
			elem.addGained(reward);
			if (reward.getId()!=null)
				elem.setAdventure(ProductServiceLoader.getInstance().getAdventure(RoleplayingSystem.CORIOLIS, reward.getId()));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getExpFree(), model.getExpInvested()}));
//			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MONEY_CHANGED, null));
			return elem;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openEdit(de.rpgframework.genericrpg.HistoryElement)
	 */
	@Override
	public boolean openEdit(HistoryElement elem) {
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getScreenManager().showAlertAndCall(
					AlertType.ERROR, 
					UI2.getString("error.not-possible"), 
					UI2.getString("error.only-single-rewards-editable"));
			return false;
		}
		
		logger.warn("TODO: openEdit");
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getScreenManager().showAlertAndCall(
					AlertType.ERROR, 
					UI2.getString("error.not-possible"), 
					UI2.getString("error.only-single-rewards-editable"));
			return false;
		}
		
		Reward toEdit = elem.getGained().get(0);
		RewardBox content = new RewardBox(toEdit);
		
		ManagedDialog dialog = new ManagedDialog(UI2.getString("dialog.reward.title"), content, CloseType.OK, CloseType.CANCEL);
		
		CloseType closed = (CloseType)provider.getScreenManager().showAndWait(dialog);
		
		if (closed==CloseType.OK) {
			RewardImpl reward = content.getDataAsReward();
			logger.debug("Copy edited data: ORIG = "+toEdit);
			toEdit.setTitle(reward.getTitle());
			toEdit.setId(reward.getId());
			toEdit.setDate(reward.getDate());
			// Was single reward. Changes in reward title, change element title
			((HistoryElementImpl)elem).setName(reward.getTitle());
			ProductService sessServ = ProductServiceLoader.getInstance();
			if (reward.getId()!=null)  {
				Adventure adv = sessServ.getAdventure(RoleplayingSystem.CORIOLIS, reward.getId());
				if (adv==null) {
					logger.warn("Reference to an unknown adventure: "+reward.getId());
				} else
					((HistoryElementImpl)elem).setAdventure(adv);

			}
//			((HistoryElementImpl)elem).set(reward.getTitle());
			logger.debug("Copy edited data: NEW  = "+toEdit);
			logger.debug("Element now   = "+elem);
			return true;
		}
		return false;
	}

}