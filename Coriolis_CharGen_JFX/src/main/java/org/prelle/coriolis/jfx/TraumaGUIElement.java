/**
 * 
 */
package org.prelle.coriolis.jfx;

import java.util.PropertyResourceBundle;

import org.prelle.coriolis.AttributeCoriolis;

import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class TraumaGUIElement extends VBox {

	private static PropertyResourceBundle UI = CoriolisCharGenJFXConstants.RES;
	
	private TextField[] tfText;
	
	private Label header;
	private Label[] lbPC;

	//-------------------------------------------------------------------
	/**
	 */
	public TraumaGUIElement() {
		initComponents();
		initLayoutNormal();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		header = new Label(UI.getString("label.element.trauma"));
		header.getStyleClass().add("element-head");
		header.setMaxWidth(Double.MAX_VALUE);
		
		tfText = new TextField[AttributeCoriolis.primaryValues().length];
		lbPC   = new Label[AttributeCoriolis.primaryValues().length];
		for (int i=0; i<tfText.length; i++) {
			tfText[i] = new TextField();
			tfText[i].setMaxWidth(Double.MAX_VALUE);
			lbPC[i] = new Label(AttributeCoriolis.primaryValues()[i].getName()+":");
		}
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		GridPane grid = new GridPane();
		grid.setStyle("-fx-background-color: lightgrey; -fx-vgap: 0.5em; -fx-hgap: 0.5em; -fx-padding: 0.3em");
		for (int i=0; i<tfText.length; i++) {
			grid.add(lbPC[i]       , 0, i);
			grid.add(tfText[i]     , 1, i);
			GridPane.setValignment(lbPC[i], VPos.TOP);
			GridPane.setHgrow(tfText[i], Priority.ALWAYS);
		}
		getChildren().clear();
		getChildren().addAll(header, grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

}
