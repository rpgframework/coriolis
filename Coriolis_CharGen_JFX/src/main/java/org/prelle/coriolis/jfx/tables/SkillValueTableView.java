package org.prelle.coriolis.jfx.tables;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.coriolis.CoriolisCharacter;
import org.prelle.coriolis.charctrl.CoriolisCharacterController;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.rpgframework.jfx.NumericalValueField;
import org.prelle.rpgframework.jfx.NumericalValueTableCell;
import org.prelle.yearzeroengine.Attribute;
import org.prelle.yearzeroengine.Skill;
import org.prelle.yearzeroengine.SkillCategory;
import org.prelle.yearzeroengine.SkillValue;
import org.prelle.yearzeroengine.modifications.KeySkillModification;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * @author Stefan Prelle
 *
 */
public class SkillValueTableView extends TableView<SkillValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillValueTableView.class.getName());

	private CoriolisCharacterController control;
	private SkillCategory type;
	
	private TableColumn<SkillValue, Boolean> colConcept;
	private TableColumn<SkillValue, String> colName;
	private TableColumn<SkillValue, Attribute> colAttrib1;
	private TableColumn<SkillValue, SkillValue> colPoints;

	//-------------------------------------------------------------------
	public SkillValueTableView(CoriolisCharacterController control, SkillCategory type) {
		this.control = control;
		this.type    = type;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		setData(control.getCharacter());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initColumns() {		
		colConcept  = new TableColumn<SkillValue, Boolean>();
		colName     = new TableColumn<SkillValue, String>(RES.getString("label.name"));
		colAttrib1  = new TableColumn<SkillValue, Attribute>(RES.getString("label.attrib"));
		colPoints   = new TableColumn<SkillValue, SkillValue>(RES.getString("label.points"));

		colConcept.setMinWidth(30);
		colConcept.setPrefWidth(40);
		colConcept.setMaxWidth(40);
		colName.setMinWidth(130);
		colName.setPrefWidth(160);
		colAttrib1.setMinWidth(70);
		colPoints.setMinWidth(120);

		colName.setStyle( "-fx-alignment: top-left;");
		colAttrib1.setStyle( "-fx-alignment: top-left;");
		colPoints.setStyle( "-fx-alignment: top-center;");
		
		getColumns().addAll(colConcept, colName, colAttrib1, colPoints);
		setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
		
		colConcept.setCellValueFactory(cdf -> new SimpleBooleanProperty(control.getSkillController().isConceptSkill(cdf.getValue().getModifyable())));
		colName.setCellValueFactory( param-> new SimpleStringProperty(param.getValue().getSkill().getName()));
		colAttrib1.setCellValueFactory( param-> new SimpleObjectProperty(param.getValue().getSkill().getAttribute()));
		colPoints.setCellValueFactory( param-> new SimpleObjectProperty<SkillValue>(param.getValue()));

		colConcept.setCellFactory(col -> new TableCell<SkillValue,Boolean>(){
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					if (item) {
						Label lb = new Label("\uE735");
						lb.setStyle("-fx-text-fill: recommendation; -fx-font-family: 'Segoe MDL2 Assets';");
						Tooltip tip = new Tooltip(RES.getString("tooltip.conceptskill"));
						Tooltip.install(lb, tip);
						setGraphic(lb);
					} else
						setGraphic(null);
				}
			}
		});
//		colConcept.setCellFactory( (col) -> new TableCell<SkillValue,Boolean>() {
//			public void updateItem(Boolean item, boolean empty) {
//				super.updateItem(item, empty);
//				if (item==null || item==Boolean.FALSE) { 
//					setGraphic(null); 
//				} else { 
//					Canvas signLayer = new Canvas(20,17);
//					StackPane.setAlignment(signLayer, Pos.BOTTOM_LEFT);
//					GraphicsContext gc = signLayer.getGraphicsContext2D();
//					gc.setFont(new Font("Segoe UI Symbol", 12));
//					gc.setFill(Color.GREEN);
//					gc.fillText("\uE218", 2, 13);
//					gc.setFill(Color.WHITE);
//					gc.fillText("\uE171", 2, 13);
//					Tooltip tip = new Tooltip(RES.getString("tooltip.conceptskill"));
//					Tooltip.install(signLayer, tip);
//					setGraphic(signLayer);
//				}
//			}
//		});
		colAttrib1.setCellFactory( (col) -> new TableCell<SkillValue,Attribute>() {
			public void updateItem(Attribute item, boolean empty) {
				super.updateItem(item, empty);
				if (item==null) { setText(null); } else { 
					setText(item.getShortName()+" ("+control.getCharacter().getAttribute(item).getModifiedValue()+")"); 
				}
			}
		});
		colPoints.setCellFactory( (col) -> new NumericalValueTableCell<Skill,SkillValue,SkillValue>(control.getSkillController()));
//		colPoints.setCellFactory( (col) -> new TableCell<SkillValue,SkillValue>() {
//			public void updateItem(SkillValue item, boolean empty) {
//				super.updateItem(item, empty);
//				if (item==null) { setGraphic(null); } else { 
//					NumericalValueField<Skill, SkillValue> field = new NumericalValueField<>(item, control.getSkillController());
//					setGraphic(field); 
//				}
//			}
//		});
	}

	//-------------------------------------------------------------------
	public void setData(CoriolisCharacter model ) {
		getItems().clear();
		
		List<Skill> request = control.getRuleset().getSkillsByCategory(type);
		getItems().addAll(model.getSkills(request));
	}

}
