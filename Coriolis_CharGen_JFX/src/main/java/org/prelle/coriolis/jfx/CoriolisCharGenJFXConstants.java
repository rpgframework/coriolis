/**
 * 
 */
package org.prelle.coriolis.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Stefan Prelle
 *
 */
public interface CoriolisCharGenJFXConstants {

	public final static String PREFIX = "/org/prelle/coriolis/jfx";

	public final static Logger LOGGER = LogManager.getLogger("coriolis.jfx");

	public static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(CoriolisCharGenJFXConstants.class.getName());
	public static PropertyResourceBundle HELP = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/coriolis/jfx/i18n/coriolis/chargenui-help");

}
