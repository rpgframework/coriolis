module coriolis.chargen.jfx {
	exports org.prelle.coriolis.jfx.wizard;
	exports org.prelle.coriolis.jfx;
	
	provides de.rpgframework.character.RulePlugin with org.prelle.coriolis.jfx.CoriolisBasePlugin;

	requires transitive coriolis.chargen;
	requires transitive coriolis.core;
	requires java.prefs;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive yearzeroengine.chargen;
	requires transitive yearzeroengine.core;
	requires simple.persist;
	requires de.rpgframework.javafx;
}